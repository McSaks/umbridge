[constants]

AC:width  = 120 cm
det:width = 100 cm


AC,ToF:nlayers = 2
AC,ToF:gaps    = 1 mm
AC,ToF:strips = 12
AC,ToF:offset = -2 2 mm
AC,ToF:p-bins = 20
AC,ToF:t-bins = 50
AC,ToF:t-min  = 0 ns
AC,ToF:t-max  = 20 ns
speed-of-light  = 0.5 c

AC:pSty-thickness = 2 cm
AC:Al-thickness = 30 mm
AC:Al-type = 48
AC:CFRP-thickness = 0.8 mm

ToF:pSty-thickness = 2 cm
ToF:Al-thickness = 30 mm
ToF:Al-type = 16
ToF:CFRP-thickness = 0.8 mm

S3,S4:pSty-thickness = 2 cm
S3,S4:Al-type      = 16
S3,S4:Al-thickness = 28.4 mm
S3,S4:CFRP-thickness = 0.8 mm

strip-thickness = 0.3 mm
strip-spreading = -1 um  ; gaussian sigma
C:strip-pitch   = 100 um
CC1:strip-pitch = 100 um

C:Al-thickness   = 28.4 mm
C:Al-type        = 16
C:CFRP-thickness = 0.8 mm


[world]

supports   = on
semiheight = 4 m
y-width    = 4 m
z-width    = 4 m


[gaps]
# and distances

AC--C      = 5 mm
C--S1      = 5 mm
S1--S2     = 50 cm ; not actual gap but a distance between centers of scintilators
S2--CC1    = 5 mm
S2--S3     = 40 cm ; once again, between centers
S3--CC2    = 5 mm
CC2--S4    = 5 mm
S4--ND     = 1 cm
ND--bottom-stuff = 1 cm

topAC--sideAC = -2 cm ; between bottom of topAC and top of sideAC scintilators
topAC<>sideAC = 0 ! ; between adjacent lateral sides of topAC and sideAC scintilators

S2--TD = 0 !
S2<>TD = 2 cm

# <fallback>
CC2--plate = 5 mm
plate--S4 = 5 mm
# </fallback>



#################
##  Detectors  ##
#################

[topAC]

supports     = on

n-layers = $AC,ToF:nlayers
gaps = $AC,ToF:gaps all ; [n-layers - 1]

n-strips       = $AC,ToF:strips all ; [n-layers]
strip-offset   = $AC,ToF:offset ; [n-layers]
p-bins         = $AC,ToF:p-bins all ; [n-layers]
t-bins         = $AC,ToF:t-bins all ; [n-layers]
t-min          = $AC,ToF:t-min all ; [n-layers]
t-max          = $AC,ToF:t-max all ; [n-layers]
speed-of-light = $speed-of-light

pSty-thickness = $AC:pSty-thickness all ; [n-layers]
pSty-y-width   = $AC:width
pSty-z-width   = $AC:width

Al-thickness = $AC:Al-thickness
Al-y-width   = $AC:width
Al-z-width   = $AC:width
Al-type      = $AC:Al-type

CFRP-thickness = $AC:CFRP-thickness
CFRP-y-width   = $AC:width
CFRP-z-width   = $AC:width


[sideAC]

n-layers = 2

length    = 110 cm ; X-extension
thickness = 12 mm all
strips-horizintal = no
n-layers = $AC,ToF:nlayers
gaps = $AC,ToF:gaps all ; [n-layers - 1]
side-gaps = 5 mm all ; [n-layers]


n-strips       = $AC,ToF:strips all ; [n-layers]
strip-offset   = $AC,ToF:offset ; [n-layers]
p-bins         = $AC,ToF:p-bins all ; [n-layers]
t-bins         = $AC,ToF:t-bins all ; [n-layers]
t-min          = $AC,ToF:t-min all ; [n-layers]
t-max          = $AC,ToF:t-max all ; [n-layers]
speed-of-light = $speed-of-light


[C]

supports  = on

n-layers  = 10
spreading = $strip-spreading
gaps      = 5 mm  all         ; between assemblies

y-pitch   = $C:strip-pitch
z-pitch   = $C:strip-pitch
Si-thickness = $strip-thickness  all ; [n-layers]
Si-y-width   = $det:width
Si-z-width   = $det:width

W-thickness  = 0.1  0.1  0.1  0.1  0.1  0.1  0.1  0.1  0  0  Xo ; [n-layers]
W-y-width    = $det:width
W-z-width    = $det:width

Al-thickness = $C:Al-thickness  all ; [n-layers + 1]
Al-type      = $C:Al-type  all ; [n-layers + 1]
Al-y-width   = $det:width
Al-z-width   = $det:width

CFRP-thickness  = $C:CFRP-thickness  all ; [n-layers + 1]
CFRP-y-width    = $det:width
CFRP-z-width    = $det:width

interleave-layers = no


[S1]

supports = on

n-layers = $AC,ToF:nlayers
gaps = $AC,ToF:gaps all ; [n-layers - 1]

n-strips       = $AC,ToF:strips all ; [n-layers]
strip-offset   = $AC,ToF:offset ; [n-layers]
p-bins         = $AC,ToF:p-bins all ; [n-layers]
t-bins         = $AC,ToF:t-bins all ; [n-layers]
t-min          = $AC,ToF:t-min all ; [n-layers]
t-max          = $AC,ToF:t-max all ; [n-layers]
speed-of-light = $speed-of-light

pSty-thickness = $ToF:pSty-thickness all ; [n-layers]
pSty-y-width   = $det:width
pSty-z-width   = $det:width

Al-thickness = $ToF:Al-thickness
Al-y-width   = $det:width
Al-z-width   = $det:width
Al-type      = $ToF:Al-type

CFRP-thickness = $ToF:CFRP-thickness
CFRP-y-width   = $det:width
CFRP-z-width   = $det:width


[S2]

supports = on

n-layers = $AC,ToF:nlayers
gaps = $AC,ToF:gaps all ; [n-layers - 1]

n-strips       = $AC,ToF:strips all ; [n-layers]
strip-offset   = $AC,ToF:offset ; [n-layers]
p-bins         = $AC,ToF:p-bins all ; [n-layers]
t-bins         = $AC,ToF:t-bins all ; [n-layers]
t-min          = $AC,ToF:t-min all ; [n-layers]
t-max          = $AC,ToF:t-max all ; [n-layers]
speed-of-light = $speed-of-light

pSty-thickness = $ToF:pSty-thickness all ; [n-layers]
pSty-y-width   = $det:width
pSty-z-width   = $det:width

Al-thickness = $ToF:Al-thickness
Al-y-width   = $det:width
Al-z-width   = $det:width
Al-type      = $ToF:Al-type

CFRP-thickness = $ToF:CFRP-thickness
CFRP-y-width   = $det:width
CFRP-z-width   = $det:width


[CC1]

supports  = on

n-layers  = 2
spreading = $strip-spreading

gaps(CsI--Si) = 1 cm  all ; [n-layers]
gaps(Si--CsI) = 1 cm  all ; [n-layers - 1]

y-pitch   = $CC1:strip-pitch
z-pitch   = $CC1:strip-pitch
Si-thickness = $strip-thickness  all ; [n-layers]
Si-y-width   = $det:width
Si-z-width   = $det:width

CsI-thickness  = 1 Xo  all ; [n-layers]
CsI-y-width    = $det:width
CsI-z-width    = $det:width

Al(CsI)-thickness = 27 mm  all ; [n-layers]
Al(CsI)-type      = 48  all ; [n-layers]
Al(CsI)-y-width   = $det:width
Al(CsI)-z-width   = $det:width
Al(Si)-thickness  = 28.4 mm  all ; [n-layers]
Al(Si)-type       = 16  all ; [n-layers]
Al(Si)-y-width    = $det:width
Al(Si)-z-width    = $det:width

CFRP(CsI)-thickness  = 1.5 mm  all ; [n-layers + 1]
CFRP(CsI)-y-width    = $det:width
CFRP(CsI)-z-width    = $det:width
CFRP(Si)-thickness  = 1.5 mm  all ; [n-layers + 1]
CFRP(Si)-y-width    = $det:width
CFRP(Si)-z-width    = $det:width

# <fallback>
Al-thickness = 27  27  28.4 mm ; [n-layers + 1]
Al-type      = 16  all ; [n-layers + 1]
Al-y-width   = $det:width
Al-z-width   = $det:width
CFRP-thickness  = 1.5  1.5  0.8 mm ; [n-layers + 1]
CFRP-y-width    = $det:width
CFRP-z-width    = $det:width
gaps      = 1 cm  all         ; between assemblies
# </fallback>


[S3]

supports = on

n-layers = $AC,ToF:nlayers
gaps = $AC,ToF:gaps all ; [n-layers - 1]

n-strips       = $AC,ToF:strips all ; [n-layers]
strip-offset   = $AC,ToF:offset ; [n-layers]
p-bins         = $AC,ToF:p-bins all ; [n-layers]
t-bins         = $AC,ToF:t-bins all ; [n-layers]
t-min          = $AC,ToF:t-min all ; [n-layers]
t-max          = $AC,ToF:t-max all ; [n-layers]
speed-of-light = $speed-of-light

pSty-thickness = $ToF:pSty-thickness all ; [n-layers]
pSty-y-width   = $det:width
pSty-z-width   = $det:width

Al-thickness = $S3,S4:Al-thickness
Al-y-width   = $det:width
Al-z-width   = $det:width
Al-type      = $S3,S4:Al-type

CFRP-thickness = $S3,S4:CFRP-thickness
CFRP-y-width   = $det:width
CFRP-z-width   = $det:width


[CC2]

supports  = on
segmented = yes

n-layers = 1
CFRP-between-thickness = 0 mm ; support between layers
mirrorfilm-thickness   = 50 um ; currently, just vacuum
item-heights          = 430 mm  all ; [n-layers]  heights of scintilator cuboids

n-items[Y]  = 28  all ; [n-layers]
n-items[Z]  = 28  all ; [n-layers]
scint-width = 36 mm  all ; [n-layers]  currently, y = z
CFRP-between-items-thickness[Y] = 500 um ; [n-layers]
CFRP-between-items-thickness[Z] = 500 um ; [n-layers]


[plate]

upper-outer-thickness = 1 cm
upper-inner-thickness = 7 cm
lower-inner-thickness = 15 cm
lower-outer-thickness = 1.5 cm
y-width   = $det:width
z-width   = $det:width

# <fallback>
thickness = 2 cm
# </fallback>


[S4]

supports = on

n-layers = $AC,ToF:nlayers
gaps = $AC,ToF:gaps all ; [n-layers - 1]

n-strips       = $AC,ToF:strips all ; [n-layers]
strip-offset   = $AC,ToF:offset ; [n-layers]
p-bins         = $AC,ToF:p-bins all ; [n-layers]
t-bins         = $AC,ToF:t-bins all ; [n-layers]
t-min          = $AC,ToF:t-min all ; [n-layers]
t-max          = $AC,ToF:t-max all ; [n-layers]
speed-of-light = $speed-of-light

pSty-thickness = $ToF:pSty-thickness all ; [n-layers]

pSty-y-width   = $det:width
pSty-z-width   = $det:width

Al-thickness = $S3,S4:Al-thickness
Al-y-width   = $det:width
Al-z-width   = $det:width
Al-type      = $S3,S4:Al-type

CFRP-thickness = $S3,S4:CFRP-thickness
CFRP-y-width   = $det:width
CFRP-z-width   = $det:width


[ND]

supports = on

pEth1-thickness = 14 mm
PMMA1-thickness = 15 mm
LiFZnS1-thickness = 0.45 mm
PMMA2-thickness = 15 mm
pEth2-thickness = 10 mm
PMMA3-thickness = 15 mm
LiFZnS2-thickness = 0.45 mm
PMMA4-thickness = 15 mm
pEth3-thickness = 14 mm

y-width   = $det:width
z-width   = $det:width

Al-thickness = $S3,S4:Al-thickness
Al-y-width   = $det:width
Al-z-width   = $det:width
Al-type      = $S3,S4:Al-type

CFRP-thickness = $S3,S4:CFRP-thickness
CFRP-y-width   = $det:width
CFRP-z-width   = $det:width



[Bottom Stuff]

supports = on

x-size = 0 m
y-size = 1.5 m
z-size = 1.5 m

[TD]

length = 70 cm
thickness = 1 cm
