#!/usr/bin/env python3

"""

"""

import sys
import scipy
# from scipy import *
import math
import random as rand

import argparse
parser = argparse.ArgumentParser(
  description = """Create a macro file with random exponential distribution
  %(prog)s [OPTS...] N > path/to/FILE.mac
place the generated file FILE.mac via
  /control/execute path/to/FILE.mac
in place of
  /run/beanOn N""",
  formatter_class=argparse.RawTextHelpFormatter,
)
parser.add_argument("nevents",
                    metavar = "N",
                    type = int,
                    help = "beam on N events")
parser.add_argument("-w",
                    "--half-width",
                    metavar = "DYZ",
                    type = float,
                    default = 1,
                    help = "place gun at (2DYZ)×(2DYZ) mm² square;\ndefault: %(default)s×%(default)s mm²")
parser.add_argument("-x",
                    "--x-start",
                    metavar = "X",
                    type = float,
                    default = 86.60006,
                    help = "use minimal x-coordinate X mm;\ndefault: %(default)s mm")
parser.add_argument("-y",
                    "--y-center",
                    metavar = "Y",
                    type = float,
                    default = 0,
                    help = "center gun at Y mm; default: %(default)s mm")
parser.add_argument("-z",
                    "--z-center",
                    metavar = "Z",
                    type = float,
                    default = 0,
                    help = "center gun at Z mm; default: %(default)s mm")
parser.add_argument("-r",
                    "--radiation-length",
                    metavar = "X0",
                    type = float,
                    default = 3.513,
                    help = "assuming radiation length be X0 mm;\ndefault is tungsten: %(default)s mm")
parser.add_argument("-d",
                    "--thickness",
                    metavar = "L",
                    type = float,
                    default = 0.1,
                    help = "foil thickness is L Xo; default: %(default)s Xo")
args = parser.parse_args()
print('#', args)
locals().update(args.__dict__)


def lin_rand(a, l, gamma):
  a2 = a - 2
  return l/a * (
    1 + a2 * l * math.sqrt( (1 + a*a2*gamma) / (a2 * l)**2 )
  )

def exp_rand(l, Xo):
  def expXo(x):
    return math.exp(-x/Xo)
  a = 1 - expXo(l)
  while True:
    x = lin_rand(a, l, rand.random())
    y = 1 - a*x/l
    y *= rand.random()
    #print("\nx = ", x, "\ny = ", y, "\nexp = ", expXo(x))
    if y < expXo(x):
      break
    #break
  return x


Xo = radiation_length
l = thickness * Xo

for i in range(nevents):
  print(
    '/gun/random/position',
    x_start + exp_rand(l, Xo),
    y_center,
    z_center,
    'mm 0',
    half_width, half_width,
    'mm'
  )
  print(
    '/run/beamOn'
  )
