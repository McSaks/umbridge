/**
 * @file Delacour.cc
 * @author  Maxim Kheymits (McSaks) <mcsaksik\@gmail.com>
 * @version 16
 *
 * @section DESCRIPTION
 *
 *   The main file of the Myrtle program.
 * Program simulates Gamma-400 instrument with a
 * construction called Myrtle
 *
 *   You can see the usage by executing
 * <tt>Delacour --help</tt>
 * or in "man/usage.txt".
 *
 */

        /*/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\\
       //   __    __          ____         __             \\
      //   //|   //|         //  \\        ||              \\
     //   //||  //||   ___   \\___     ___ || __  ___       \\
    //   // || // ||  // \\      \\   //|| ||//  //__'       )>
   //   //  ||//  || ((   _  _    )) // || ||/\     \\      //
  //   //   |//   ||  \\_//  \\__// ||_//\ || \\ \\_//     //
 //                                                       //
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\*/

/*       //                                          \\
//      /  \                                        /  \
//     /o  o\             Main program             /o  o\
//    /  ..  \                                    /  ..  \
//   /__\__/__\                                  /__\__/__\
*/

#if defined(G4VIS_USE_OPENGLQT) || defined(G4VIS_USE_OPENGLX) || defined(G4VIS_USE_OPENGLXM)
  #define G4VIS_USE 1
#else
  #undef G4VIS_USE
#endif

#if defined(G4UI_USE_QT) || defined(G4UI_USE_TCSH)
  #define G4UI_USE 1
#else
  #undef G4UI_USE
#endif

#include "DetectorConstruction.hh"

//#include "PhysicsList/SSPhysicsList.hh"
#include "QGSP_BERT_HP.hh"

//#include "PhysicsList.hh"
#include "PrimaryGeneratorAction.hh"
#include "RunAction.hh"
#include "EventAction.hh"
#include "SteppingAction.hh"
#include "SteppingVerbose.hh"
#include "DetectorMessenger.hh"
#include "EventData.hh"
#include "Trigger.hh"
#include "FOR.hh"
#include "QGSP_BIC_HP.hh"

#include "G4RunManager.hh"
#include "G4UImanager.hh"

#include <fstream>


#ifdef G4VIS_USE
  #include "G4VisExecutive.hh"
#endif

#if   defined(G4UI_USE_QT)
  #include "G4UIExecutive.hh"
#elif defined(G4UI_USE_TCSH)
  #include "G4UIterminal.hh"
  #include "G4UItcsh.hh"
#endif


const G4float version = 17.0;
const G4String version_string = "GCD-McS v17.0 (Umbridge)";

#include <string.h>
#include "Alternative.hh"


void usage(G4String docfilename = "man/usage.txt")
{
  //Disp(outfile, "outfile: "); return;
  std::ifstream docfile(docfilename);
  IF (docfile)
    char c;
    G4cout << "\n";
    while(!docfile.eof()) {
      docfile.get(c);
      G4cout << c;
    }
    G4cout << G4endl;
  ELSE
    G4cout << "\n"
    << version_string << " program.\nCannot find 'man/usage.txt' help file."
    << G4endl;
  END_IF
}


#include "ProcessParameters.hh"

#include <unistd.h>

int main(int argc, char** argv)
{
  if(!ProcessParameters(argc, argv)) return 1;

  // Print head
  const G4int version_string_length = version_string.length();
  G4cout << "\n  ┌";
  FOR(_, -17, version_string_length) G4cout << "─";
  G4cout << "┐\n  │ Program "
         << version_string
         << " started ├─┐\n  └─┬";
  FOR(_, -15, version_string_length) G4cout << "─";
  G4cout << "┘ │\n    └";
  FOR(_, -17, version_string_length) G4cout << "─";
  G4cout << "┘"
         << G4endl;
  G4cout << "\n    $";
  FOR(_, 0, argc - 1)
    if( (size_t)G4String(argv[_]).first(' ') == std::string::npos )
      G4cout << " " << argv[_];
    else
      G4cout << " '" << argv[_] << "'";
  G4cout << G4endl;


  /* randomize: */
  if (seed == -1) seed = time(0)+getpid();
  G4cout << "\nRandom seed = " << seed << G4endl;
  CLHEP::HepRandom::setTheSeed(seed);

  // User Verbose output class
  //
  G4VSteppingVerbose* verbosity = new SteppingVerbose;
  G4VSteppingVerbose::SetInstance(verbosity);

  // Run manager
  //
  G4RunManager* runManager = new G4RunManager;

  // User Initialization classes (mandatory)
  //

  PositionSystem* posSystem = new PositionSystem;
  EnergySystem* caloSystem = new EnergySystem;
  Calo3DSystem* cc2System = new Calo3DSystem;
  ACSystem* AC = new ACSystem;
  ToFSystem* ToF = new ToFSystem;
  //

  DetectorConstruction* detector = NULL;
  G4UserRunAction* run_action = NULL;
  EventData* eventData = NULL;
  G4UImanager* UImanager = NULL;
  G4UserSteppingAction* stepping_action = NULL;
  G4VUserPrimaryGeneratorAction* gen_action = NULL;
  EventAction* event_action = NULL;
  DetectorMessenger* messenger = NULL;
  Trigger* trigger = NULL;
  #ifdef G4VIS_USE
    G4VisManager* visManager = NULL;
  #endif

  TRY
    detector = new DetectorConstruction(geofile);
  CATCH (std::exception& e)
    G4cerr << "!!! Exception caught: " << e.what() << "!!!" << G4endl; goto clean;
  CATCH (...)
    G4cerr << "!!! Exception caught" << "!!!" << G4endl; goto clean;
  END_TRY

  if (ini_allow_default) detector->SetCompatibilityMode();
  detector->SetSystem(posSystem);
  detector->SetSystem(caloSystem);
  detector->SetSystem(cc2System);
  detector->SetSystem(AC);
  detector->SetSystem(ToF);
  detector->SetOutputFile(outfile);
  messenger = new DetectorMessenger;//messenger = detector->GetMessenger();
  posSystem->SetDetectorConstruction(detector);
  posSystem->SetMessenger(messenger);
  //tof->SetDetectorConstruction(detector);
  //tof->SetMessenger(messenger);
  caloSystem->SetPositionSystem(posSystem);
  //
  trigger = new Trigger(detector, posSystem, ToF, AC, use_trigger, ToFTimeSelection);
  runManager->SetUserInitialization(detector);
  //
  G4VUserPhysicsList* physics;
  SWITCH(physics_list)
    //CASE kPhysicsList_simple:
    //  physics = new PhysicsList;
    CASE kPhysicsList_QGSP_BERT_HP:
      physics = new QGSP_BERT_HP; // SSPhysicsList;
    CASE kPhysicsList_QGSP_BIC_HP:
      physics = new QGSP_BIC_HP;
    DEFAULT:
      G4cerr << "Error: unknown PhysicsList" << G4endl;
      return 1;
  END_SWITCH
  runManager->SetUserInitialization(physics);

  // User Action classes
  //
  gen_action = new PrimaryGeneratorAction(detector);
  runManager->SetUserAction(gen_action);
  messenger->SetPrimaryGeneratorAction((PrimaryGeneratorAction*)gen_action);
  //
  //G4UserEventAction
  event_action = new EventAction(trigger);
  event_action->SetSystem(posSystem);
  event_action->SetSystem(caloSystem);
  event_action->SetSystem(cc2System);
  event_action->SetSystem(ToF);
  event_action->SetSystem(AC);
  runManager->SetUserAction(event_action);
  posSystem->SetEventAction((EventAction*)event_action);
  //tof->SetEventAction((EventAction*)event_action);
  //
  run_action = new RunAction((EventAction*)event_action, rundatafile);
  runManager->SetUserAction(run_action);
  event_action->SetRunAction((RunAction*)run_action);
  ((RunAction*)run_action)->SetPrimaryGeneratorAction((PrimaryGeneratorAction*)gen_action);
  //
  stepping_action = new SteppingAction;
  runManager->SetUserAction(stepping_action);
  ((PrimaryGeneratorAction*)gen_action)->SetRunAction((RunAction*)run_action);

  // Initialize G4 kernel
  //
  runManager->Initialize();

  // EventData
  eventData = new EventData(detector, outfile);
  eventData->SetOption(scintstripoutput);
  eventData->SetDetectorConstruction(detector);
  posSystem->SetEventData(eventData);
  caloSystem->SetEventData(eventData);
  cc2System->SetEventData(eventData);
  ToF->SetEventData(eventData);
  AC->SetEventData(eventData);
  event_action->SetEventData(eventData);
  #ifdef G4VIS_USE
    visManager = new G4VisExecutive;
    visManager->Initialize();
  #endif

  // Get the pointer to the User Interface manager
  //
  UImanager = G4UImanager::GetUIpointer();
  TRY
    IF (ui_use) // interactive mode : define UI session
        #if   defined(G4UI_USE_QT)
          G4UIExecutive* ui = new G4UIExecutive(argc, argv);
        #elif defined(G4UI_USE_TCSH)
          G4UIterminal* ui = new G4UIterminal(new G4UItcsh);
        #endif

        #ifdef G4UI_USE
          #ifdef G4VIS_USE
            UImanager->ApplyCommand(G4String("/control/execute ") + macrofile);
            ExecuteCommands(UImanager);
            UImanager->ApplyCommand(G4String("/run/beamOn ") + nevents_str);
          #endif
          ui->SessionStart();
          delete ui;
        #endif
    ELSE // batch mode
        UImanager->ApplyCommand(G4String("/control/execute ") + macrofile);
        ExecuteCommands(UImanager);
        if(nevents) UImanager->ApplyCommand(G4String("/run/beamOn ") + nevents_str);
    END (IF ui_use)
  CATCH (std::exception& e)
    G4cerr << "!!! Exception caught: " << e.what() << "!!!" << G4endl;
  CATCH (...)
    G4cerr << "!!! Exception caught" << "!!!" << G4endl;
  END_TRY

  clean:

  #ifdef G4VIS_USE
    delete visManager;
  #endif

  // Free the store: user actions, physics_list and detector_description are
  //                 owned and deleted by the run manager, so they should not
  //                 be deleted in the main() program !
  G4cout << runManager->GetVersionString() << G4endl;
  delete runManager;
  delete verbosity;
  delete ToF;
  delete AC;
  delete posSystem;
  delete caloSystem;
  delete cc2System;
  delete messenger;
  delete trigger;
  delete eventData;

  return 0;
}
