/*        //                                          \\ 
//       /  \              Run Action                /  \ 
//      /o  o\                                      /o  o\ 
//     /  ..  \        begin and end of run        /  ..  \ 
//    /__\__/__\                                  /__\__/__\ 
*/

#ifndef RunAction_h
#define RunAction_h 1

#include "G4UserRunAction.hh"
#include "globals.hh"
#include <fstream>

class G4Run;
class EventAction;
class PrimaryGeneratorAction;

class RunAction : public G4UserRunAction
{
  public:
    RunAction(EventAction*, G4String runfile = "/dev/null");
   ~RunAction();
  
  public:
    void BeginOfRunAction(const G4Run*);
    void EndOfRunAction(const G4Run*);
    inline void SetPrimaryGeneratorAction(PrimaryGeneratorAction* gen) {gun = gen;};
    inline size_t operator++() { return ++goodCounter; };
    inline void IncreaseGoodCounter() { ++goodCounter; };
    inline void IncreaseAllCounter() { ++allCounter; /*++effectiveCounter;*/ };
    inline void IncreaseEffectiveCounter() { ++effectiveCounter; };
  
  protected:
    EventAction* myEventAction;
    PrimaryGeneratorAction* gun;
    G4double angres;
    size_t allCounter, goodCounter, effectiveCounter;
    std::ofstream rundatafile;
};

#endif
