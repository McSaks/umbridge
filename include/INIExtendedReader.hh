/*        //                                          \\
//       /  \                                        /  \
//      /o  o\        Reading data from file        /o  o\
//     /  ..  \                                    /  ..  \
//    /__\__/__\                                  /__\__/__\
*/

#ifndef INIExtenedReader_h
#define INIExtenedReader_h 1

#include "globals.hh"
#include "INIReader.h"
#include <vector>
#include <cmath>
#include <ostream>
#include "TFormat.hh"
#include "types.hh"

#ifndef bufsize
#define bufsize 0xFFFF
#endif

//using std::string;

static double Unit(const std::string& u, double radlength = 0.)
{
  if(false) { }
  else if( u ==  "m" ) return  m;
  else if( u == "cm" ) return cm;
  else if( u == "mm" ) return mm;
  else if( u == "um" ) return 1e-3 * mm;
  else if( u == "µm" ) return 1e-3 * mm;
  else if( u == "μm" ) return 1e-3 * mm;
  else if( u ==  "s" ) return  s;
  else if( u == "ms" ) return ms;
  else if( u == "us" ) return 1e-6 * s;
  else if( u == "µs" ) return 1e-6 * s;
  else if( u == "μs" ) return 1e-6 * s;
  else if( u == "ns" ) return ns;
  else if( u == "ps" ) return picosecond;
  else if( u == "!"  ) return 1;
//else if( u == ""   ) return 1;
  else if( u == "Xo" ) return radlength;
  else if( u == "c" )  return 2.99792458e+8 * m/s;
  else G4Exception(
    "Unit",
    "Error",
    FatalException,
    ("Unknown unit ‘" + u + "’. Possible values are:\n"
    "  m, cm, mm, um|µm, s, ms, us|µs, ns, ps,"
    "  c, Xo, !.").c_str());
  return 1;
}

static double atof(const std::string& a) { std::stringstream str(a); double d; str >> d; return d; }


typedef std::string ValueS;
typedef long ValueI;
typedef double ValueD;

struct ValueB
{
  ValueB(bool b = false)
    : val(b) { };
  ValueB(const std::string& str) {
    if (  str == "yes" || str == "on"   || str == "true"  || str == "yep"         || str == "1"
       || str == "y"   || str == "yeah" || str == "+"     || str == "affirmative" || str == "aye"
       || str == "t"   || str == "T"    || str == "allow" || str == "accept"      || str == "ok" )
      val = true;
    else
    if (  str == "no"  || str == "off"  || str == "false" || str == "nope"        || str == "0"
       || str == "n"   || str == "none" || str == "-"     || str == "negative"    || str == "skip"
       || str == "f"   || str == "F"    || str == "deny"  || str == "reject"      || str == "refuse" )
      val = false;
    else G4Exception("ValueB::ValueB", "Warning", JustWarning, "Wrong boolean. Assuming false"), val = false;
  };
  operator bool() const { return val; };
  friend std::ostream& operator<< (std::ostream& os, const ValueB& b) { return os << (b.val ? "true" : "false"); };

  bool val;
};

struct ValueU
{
  ValueU(double d = 0., const std::string& str = "!")
    : val(d), u(str), valu(val*Unit(u)) { };
  ValueU(double d, std::string str, double radlength)
    : val(d), u(str), valu(val*Unit(u, radlength)) { };
  ValueU(std::string d, std::string str)
    : val(atof(d)), u(str), valu(val*Unit(u)) { };
  ValueU(std::string d, std::string str, double radlength)
    : val(atof(d)), u(str), valu(val*Unit(u, radlength)) { };
  operator double() const { return valu; };
  friend std::ostream& operator<< (std::ostream& os, const ValueU& u) { return os << u.valu; };

  double val;
  std::string u;
  double valu;
};

typedef std::vector<ValueS> ValueSv;
typedef std::vector<ValueD> ValueDv;
struct ValueIv: public std::vector<ValueI>
{
  ValueIv() { }
  ValueIv(
      size_type n, const value_type& val = value_type(),
      const allocator_type& alloc = allocator_type())
    : std::vector<ValueI>(n, val, alloc)
  { }
  operator std::vector<long>() const;
  operator std::vector<int>() const;
};

struct ValueBv: public std::vector<ValueB>
{
  ValueBv() { }
  ValueBv(
      size_type n, const value_type& val = value_type(),
      const allocator_type& alloc = allocator_type())
    : std::vector<ValueB>(n, val, alloc)
  { }
  operator std::vector<bool>() const;
};
struct ValueUv: public std::vector<ValueU>
{
  ValueUv() { }
  ValueUv(
      size_type n, const value_type& val = value_type(),
      const allocator_type& alloc = allocator_type())
    : std::vector<ValueU>(n, val, alloc)
  { }
  operator std::vector<double>() const;
};


/* * * * * * * * *\
|*  FileReader   *|
\* * * * * * * * */

class INIExtendedReader : public INIReader
{

  /* CONSTRUCTORS / DESTRUCTORS */

  public:
    INIExtendedReader(const std::string& ifile = "in/default.in");
   ~INIExtendedReader();


  /* METHODS */

  public: // get / set

  public: // main
    //virtual bool Read() = 0;

  public: // subroutines of Read()
    ValueS   ReadS  (const std::string& section, const std::string& name, const ValueS& dflt = "????") { return Get(section, name, dflt); };
    ValueI   ReadI  (const std::string& section, const std::string& name, const ValueI& dflt = 0     ) { return GetInteger(section, name, dflt); };
    ValueB   ReadB  (const std::string& section, const std::string& name, const ValueB& dflt = false ) { std::string o = Get(section, name, "\04"); return (o == "\04") ? dflt : o; };
    ValueD   ReadD  (const std::string& section, const std::string& name, const ValueD& dflt = NAN   ) { return GetReal(section, name, dflt); };
    ValueU   ReadU  (const std::string& section, const std::string& name, const ValueU& dflt = ValueU(), double radlength = 1.);
    ValueSv  ReadSv (const std::string& section, const std::string& name, int n, const ValueS& dflt = "????");
    ValueIv  ReadIv (const std::string& section, const std::string& name, int n, const ValueI& dflt = 0     );
    ValueBv  ReadBv (const std::string& section, const std::string& name, int n, const ValueB& dflt = false );
    ValueDv  ReadDv (const std::string& section, const std::string& name, int n, const ValueD& dflt = NAN   );
    ValueUv  ReadUv (const std::string& section, const std::string& name, int n, const ValueU& dflt = ValueU(), double radlength = 1);
    ValueUv  AddUnit(const ValueDv&, const std::string& u, double radlength);
    ValueUv  AddUnit(const ValueDv&, const std::string& u, const std::vector<double>& radlength);
    inline std::string ReadItem(const std::string& section, const std::string& name, const std::string& dflt) { return Get(section, name, dflt); };
    static std::vector<std::string> UnValue(const ValueSv&);
    static std::vector<int>         UnValue_i(const ValueIv&);
    static std::vector<long>        UnValue_l(const ValueIv&);
    static std::vector<bool>        UnValue(const ValueBv&);
    static std::vector<double>      UnValue(const ValueDv&);
    static std::vector<double>      UnValue(const ValueUv&);

    virtual std::string Get(const std::string& section, const std::string& name,
                    const std::string& default_value, int depth = 0);
    //string ShowItems(int n = 1);

  /* FIELDS */

  protected: // pointers


  protected: // files
    std::string infilename;
    //std::ifstream infile;
    bool ok;
    std::string kw_all, kw_same;

  public:
    inline bool Ok() const { return ok; };
    inline operator bool() { return ok; };
    inline bool operator!() { return !ok; };
};

#endif
