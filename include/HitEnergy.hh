/*        //                                          \\ 
//       /  \                                        /  \ 
//      /o  o\    Hit in the E-Sensitive Detector   /o  o\ 
//     /  ..  \                                    /  ..  \ 
//    /__\__/__\                                  /__\__/__\ 
*/

#ifndef HitEnergy_h
#define HitEnergy_h 1

#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4Allocator.hh"
#include "G4ThreeVector.hh"
#include "types.hh"

class HitEnergy : public G4VHit
{
  public:

      HitEnergy();
     ~HitEnergy();
      HitEnergy(const HitEnergy&);
      const HitEnergy& operator=(const HitEnergy&);
      G4int operator==(const HitEnergy&) const;

      inline void* operator new(size_t);
      inline void  operator delete(void*);

      void Draw();
      void Print();

  public:
  
      void SetTrackID    (G4int track)       { trackID = track; };
      void SetEdep       (G4double de)       { edep = de; };
      void SetPos        (G4ThreeVector xyz) { pos = xyz; };
      
      G4int GetTrackID()      { return trackID; };
      G4double GetEdep()      { return edep; };      
      G4ThreeVector GetPos()  { return pos; };
      
  private:
  
      G4int         trackID;
      G4double      edep;
      G4ThreeVector pos;
};

typedef G4THitsCollection<HitEnergy> HitsCollectionEnergy;

extern G4Allocator<HitEnergy> hitAllocatorEnergy;

inline void* HitEnergy::operator new(size_t)
{
  void* aHit;
  aHit = (void*) hitAllocatorEnergy.MallocSingle();
  return aHit;
}

inline void HitEnergy::operator delete(void* aHit)
{
  hitAllocatorEnergy.FreeSingle((HitEnergy*) aHit);
}

#endif
