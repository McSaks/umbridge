/** @file types
 *
 *       //                                          \\ 
 *      /  \     Some types used in other files     /  \ 
 *     /o  o\        Also useful functions         /o  o\ 
 *    /  ..  \           and constants            /  ..  \ 
 *   /__\__/__\                                  /__\__/__\ 
 */

#ifndef types_h
#define types_h 1

#include "globals.hh"
#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"
#include "TFormat.hh"
#include <algorithm>
#include <vector>

class EnumNames
{
  protected:
    G4String* array;
    size_t size;
    G4int first;
  public:
    EnumNames(G4String* array_, size_t size_, G4int first_ = 0)
      : array(array_), size(size_), first(first_) { };
    G4String operator[](G4int i)
    {
      if(i < first || i >= first+(G4int)size) return G4String("<EnumNames:OutOfRange> ("+TFormat::itoa(i)+")");
      return array[i];
    };
};

enum EKeyValues {kOutOfVolume = -1, kUnsensitive = -2,
                 kErrorInComputing = -3, kUninitialized = -4};
static G4String __EKeyValues[] = {"kUninitialized", "kErrorInComputing",
                                   "kUnsensitive", "kOutOfVolume"};
//static G4String* _EKeyValues = __EKeyValues + 4;
static EnumNames _EKeyValues(__EKeyValues, 4, -4);

struct StripIndex
{
      G4int x;
      G4int y;
      G4int z;
      StripIndex(G4int X, G4int Y, G4int Z):x(X),y(Y),z(Z) {};
      StripIndex(G4int N):x(N),y(N),z(N) {};
      StripIndex():x(kUninitialized),y(kUninitialized),z(kUninitialized) {};
      StripIndex operator=(const StripIndex right) {x=right.x; y=right.y; z=right.z; return *this;};
};

enum EStripProj {kNoStripProj = 0, kYStripProj = 1<<0, kZStripProj = 1<<1, kXStripProj = 1<<2,
                 kBothStripProj = kYStripProj | kZStripProj, kYZStripProj = kYStripProj | kZStripProj,
                 kXYStripProj = kXStripProj | kYStripProj, kXZStripProj = kXStripProj | kZStripProj,
                 kXYZStripProj = kXStripProj | kYStripProj | kZStripProj,
                 kAllStripProj = kXStripProj | kYStripProj | kZStripProj
};
static G4String __EStripProj[] = {
  "kNoStripProj", "kYStripProj", "kZStripProj", "kYZStripProj",
  "kXStripProj", "kXYStripProj", "kXZStripProj", "kXYZStripProj"
};
static EnumNames _EStripProj(__EStripProj, 8);
static G4String __EStripProjSimple[] = {
  "-", "Y", "Z", "YZ", "X", "XY", "XZ", "XYZ"
};
static EnumNames _EStripProjSimple(__EStripProjSimple, 8);
template <class T> T SelectByStripProj(EStripProj proj, T x, T y, T z, T no) {
  if (proj == kXStripProj) return x;
  if (proj == kYStripProj) return y;
  if (proj == kZStripProj) return z;
  else return no;
}
template <class T> static T SelectByStripProj(EStripProj proj, T x, T y, T z) {
  if (proj == kXStripProj) return x;
  if (proj == kYStripProj) return y;
  if (proj == kZStripProj) return z;
  else G4Exception(
          "EStripProj::Wrong",
          ("Wrong EStripProj: " + __EStripProj[proj]).c_str(),
          FatalException,
         "");
}
static int IndexOfStripProj(EStripProj proj) {
  return SelectByStripProj<int>(proj, 0, 1, 2, -1);
}

enum EDetectorType {
    kOtherDetector,   kConverter,   kCalorimeter,   kMidi,   kToF,   kAC};
static G4String __EDetectorType[] = {
   "kOtherDetector", "kConverter", "kCalorimeter", "kMidi", "kToF", "kAC"};
static EnumNames _EDetectorType(__EDetectorType, 8);

enum E_use_trigger {kTrigger_none = 0,
                    kTrigger_pos = 1<<0, kTrigger_tof = 1<<1, kTrigger_ac = 1<<2,
                    kTrigger_all = kTrigger_pos|kTrigger_tof};
static G4String __E_use_trigger[] = {"kTrigger_none", "kTrigger_pos", "kTrigger_tof", "kTrigger_all", "kTrigger_ac"};
static EnumNames _E_use_trigger(__E_use_trigger, 5);

enum EScintStripOutput {
    kScintStrip_array, kScintStrip_tree, kScintStrip_PMSignals};
static G4String __EScintStripOutput[] = {"kScintStrip_array", "kScintStrip_tree", "kScintStrip_PMSignals"};
static EnumNames _EScintStripOutput(__EScintStripOutput, 3);

static G4String __bool[] = {"false", "true"};
static EnumNames _bool(__bool, 2);


static const G4double V = volt, mV = 1e-3*V, uV = 1e-6*V, /*ps = 1e-3*ns,*/ microgap = 0.01*um;



__attribute__ ((unused)) static G4double bincenter(G4double bmin, G4double bin, G4int num)
{
  return bmin  +  bin * (num+.5);
}

__attribute__ ((unused)) static G4double bincenter(G4double bmin, G4double bmax, G4int bins, G4int num)
{
  return bincenter(bmin, (bmax-bmin)/bins, num);
}

__attribute__ ((unused)) static G4double binmax(G4double bmin, G4double bin, G4int num)
{
  return bmin  +  bin * (num+1);
}

__attribute__ ((unused)) static G4double binmax(G4double bmin, G4double bmax, G4int bins, G4int num)
{
  return binmax(bmin, (bmax-bmin)/bins, num);
}

__attribute__ ((unused)) static G4double binmin(G4double bmin, G4double bin, G4int num)
{
  return bmin  +  bin * (num);
}

__attribute__ ((unused)) static G4double binmin(G4double bmin, G4double bmax, G4int bins, G4int num)
{
  return binmin(bmin, (bmax-bmin)/bins, num);
}

__attribute__ ((unused)) static G4int findbin(G4double bmin, G4double bin, G4double val)
{
  return (G4int)floor( (val-bmin) / bin );
}

__attribute__ ((unused)) static G4int findbin(G4double bmin, G4double bmax, G4int bins, G4double val)
{
  return findbin(bmin, (bmax-bmin)/bins, val);
}

__attribute__ ((unused)) static G4double binsize(G4double bmin, G4double bmax, G4int nbins)
{
  return (bmax - bmin) / nbins;
}

__attribute__ ((unused)) static G4int bincnt(G4double bmin, G4double bmax, G4double bin)
{
  return (G4int)round((bmax - bmin) / bin);
}


/*
#include <functional>
template <typename T>
struct identityfcn : std::unary_function<T,T> {
  T operator()(const T& x) const {return x;};
};

template <typename T, class Unop>
 T Disp(const T foo, const char* pre = "", const char* post = "",
        const G4bool red = true, const Unop op = identityfcn<T>())
    { (red?G4cerr:G4cout) << pre << op(foo) << post << G4endl; return foo; }
*/
template <typename T>
 T Disp(const T foo, const char* pre = "", const char* post = "",
        const G4bool print = true, const G4bool red = true)
    { if(print) (red?G4cerr:G4cout) << pre << foo << post << G4endl; return foo; }
template <typename T>
 T DispUnit(const T foo, const G4double u, const char* pre = "", const char* post = "",
        const G4bool print = true, const G4bool red = true)
    { if(print) (red?G4cerr:G4cout) << pre << foo/u << post << G4endl; return foo; }
template <typename T, typename Fcn>
 T DispFcn(const T foo, const Fcn fcn, const char* pre = "", const char* post = "",
        const G4bool print = true, const G4bool red = true)
    { if(print) (red?G4cerr:G4cout) << pre << fcn(foo) << post << G4endl; return foo; }
template <typename T, typename Enum>
 T DispEnum(const T foo, const Enum e, const char* pre = "", const char* post = "",
        const G4bool print = true, const G4bool red = true)
    { if(print) (red?G4cerr:G4cout) << pre << e[G4int(foo)] << post << G4endl; return foo; }

#define DispOnce(tag, msg, pre) \
  static bool tag = false; \
  if(!tag) Disp(msg, pre), tag=true;

#define DispNTimes(tag, n, msg, pre) \
  static unsigned int tag = 0; \
  if(++tag<=n) Disp(msg, pre);

template<typename T>
void sort(std::vector<T>& v) { std::sort( v.begin(), v.end() ); }
//template<typename T, class Compare>
//void sort(std::vector<T> v) { sort( v.begin(), v.end(), Compare ); }

__attribute__ ((unused))
static std::ostream& backspace(std::ostream& stream)
{ stream.seekp(long(stream.tellp())-1); return stream; }
__attribute__ ((unused))
static std::istream& backspace(std::istream& stream)
{ stream.seekg(long(stream.tellg())-1); return stream; }

#define delete0(p) do { if(p){delete p; p = NULL; } } while(0)

#define METHOD(obj, method) if(obj) (obj)->method

#define ensure(test) if(!(test)) return false;
#define ensure_and_do(test, doit) if(!(test)) { doit; return false;}
#define voidensure(test) if(!(test)) return;
#define voidensure_and_do(test, doit) if(!(test)) { doit; return;}

#include "flow-control-macros.hh"

__attribute__ ((unused))
static G4double chop(G4double val, G4double prec = 1e-12) {
  return val > +prec ? val : val < -prec ? val : 0.;
}

// system
#include <stdlib.h>

/** Prepend path with a directory specified and append an extension, if needed.
 *  \n Directory is to be prepended if the basename do not start with one of <tt>./~</tt>.
 *  \n Extension is to be appended if it is not already present and equal to \p ext.
 *  @param dir directory to be prepended
 *  @param ext extentsion to be appended
 *  @param base basename
 */
__attribute__ ((unused))
static G4String GetFileName(const G4String& dir, const G4String& ext, const G4String& base) {
  G4String realdir, file = base, dest;
  // Get position of the last dot:
  str_size last_dot = base.last('.');
  G4bool is_cmd = false;
  
  IF_ file.c_str()[0] == '!' THEN
    is_cmd = true;
    file = file(1, -1);
  END_IF
  
  SWITCH ( base.c_str()[0] )
    CASE '.' OR '/' OR '~':
      // If the file name starts with one of the listed here characters,
      // use an absolute path or one relative to the base directory:
      RELAX
    DEFAULT:
      // else use a path relative to the _dir_ specified
      // (mainly 'in' or 'out'):
      realdir = dir + '/';
  END_SWITCH
  
  str_size first_space = file.first(' ');
  G4String params = (first_space == std::string::npos) ? G4String() : file(first_space, -1);
  file = file(0, first_space);
  
  // If the extension needed is not already specified,
  // append it to the file name:
  IF_ last_dot != str_size(-1) && ext == (G4String) file(last_dot + 1, -1) THEN
    dest = realdir + file;
  ELSE
    dest = realdir + file + '.' + ext;
  END_IF
  
  IF_ is_cmd THEN
    G4String out = dest + ".generated";
    G4String cmd = dest + params + " >! " + out;
    G4int exitcode = system(cmd.c_str());
    //G4cerr << "dest = " << dest << "; ext = " << ext << "; file = " << file << "; base = " << base << "; last_dot = " << last_dot << ";\ncmd = " << cmd << "; exitcode = " << exitcode << G4endl;
    IF_ exitcode THEN
      G4cerr << "Error: executing `" << cmd << "` returned with non-zero status." << G4endl;
      return "ERROR";
    END_IF
    dest = out;
  END_IF
  
  return dest;
}


#include <typeinfo>
//! Overload stream-put operator for vectors
template<typename T> std::ostream& operator<< (std::ostream& os, const std::vector<T>& v)
{
  os << "vector<" << typeid(T).name() << ">[" << v.size() << "] (";
  IF_ v.size() THEN
    os << v.front();
    IF_ v.size() > 1 THEN
      size_t n = v.size();
      for(size_t i = 1; i < n; ++i)
        os << ", " << v.at(i);
    END_IF
  END_IF
  os << ")";
  return os;
}

__attribute__ ((unused)) static G4String& _gdb_gs (const char* cstr) { return *(new G4String(cstr)); }
__attribute__ ((unused)) static std::string& _gdb_s (const char* cstr) { return *(new std::string(cstr)); }

#endif
