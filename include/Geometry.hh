/*        //                                          \\ 
//       /  \                                        /  \ 
//      /o  o\      Reading geometry from file      /o  o\ 
//     /  ..  \                                    /  ..  \ 
//    /__\__/__\                                  /__\__/__\ 
*/

#ifndef Geometry_h
#define Geometry_h 1

#include "INIExtendedReader.hh"

class DetectorConstruction;
class G4Material;


/* * * * * * * * *\
|*   Geometry    *|
\* * * * * * * * */

class Geometry : public INIExtendedReader
{
  
  /* CONSTRUCTORS / DESTRUCTORS */
  
  public:
    Geometry(DetectorConstruction* = NULL, G4String ifile = "in/default.geometry");
   ~Geometry();
  
  
  /* METHODS */
  
  public: // get / set
    inline DetectorConstruction* GetDetectorConstruction() { return det; };
    inline void SetDetectorConstruction(DetectorConstruction* d) { det = d; };
  
  public: // main
    virtual G4bool Read();
  
  private: // materials
    G4Material* AlType(const G4int&);
    G4Material* MatType(const G4String&);
    std::vector<G4Material*> AlType(const std::vector<G4int>&);
    std::vector<G4Material*> MatType(std::vector<G4String>&);
  
  /* FIELDS */
    
  protected: // pointers
    DetectorConstruction* det;
  
  protected:
    //const size_t bufsize;
  
  protected: // files
  
  public:
};

#endif
