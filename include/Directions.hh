/*        //                                          \\ 
//       /  \         Various representations        /  \ 
//      /o  o\              of direction            /o  o\ 
//     /  ..  \                                    /  ..  \ 
//    /__\__/__\                                  /__\__/__\ 
*/

#ifndef Directions_h
#define Directions_h 1

#include "globals.hh"
#include "G4SystemOfUnits.hh"
#include "G4ThreeVector.hh"

#define DIRECTIONS_DEFINE_TO_STRING(type, arg, body)                                        \
  friend std::ostream& operator<<(std::ostream& os, const type& arg) { return os << body; }; \
  operator G4String() { std::ostringstream buf; buf << *this; return buf.str(); };            \
  operator const char*() { return (operator G4String()).c_str(); };


class Angle3D;

class Direction3D // direction (1, vy, vz)
{
  public:
    
    // CONSRUCTORS:
    Direction3D(const Direction3D& right) : vy(right.vy), vz(right.vz) { };
    Direction3D(G4double vy_ = 0, G4double vz_ = 0) : vy(vy_), vz(vz_) { };
    Direction3D(G4ThreeVector);
    Direction3D(const Angle3D&);
    DIRECTIONS_DEFINE_TO_STRING(Direction3D, dir, "Direction3D(" << dir.vy << ", " << dir.vz << ")")
    
    // TYPE CONVERTIONS:
    operator G4ThreeVector() const { return G4ThreeVector(1, vy, vz).unit(); };
  
  public:
    G4double vy;
    G4double vz;
  
};


class Angle3D // theta, phi
{
  public:
    
    // CONSRUCTORS:
    Angle3D(const Angle3D& right) : theta(right.theta), phi(right.phi) { };
    Angle3D(G4double theta_ = 0, G4double phi_ = 0) : theta(theta_), phi(phi_) { };
    Angle3D(G4ThreeVector);
    Angle3D(const Direction3D&);
    DIRECTIONS_DEFINE_TO_STRING(Angle3D, ang, "Angle3D(" << ang.theta/deg << "deg, " << ang.phi/deg << "deg)")
    
    // TYPE CONVERTIONS:
    operator G4ThreeVector() const;
  
  public:
    G4double theta;
    G4double phi;
  
};

#endif
