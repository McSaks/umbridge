/*        //                                          \\ 
//       /  \                                        /  \ 
//      /o  o\     Sensitive Strip-Type Detector    /o  o\ 
//     /  ..  \                                    /  ..  \ 
//    /__\__/__\                                  /__\__/__\ 
*/

#ifndef SensitiveStrip_h
#define SensitiveStrip_h 1

#include "SensitiveDetector.hh"
#include "G4ThreeVector.hh"
#include "HitStrip.hh"
#include "DetectorConstruction.hh"
#include "PositionSystem.hh"
#include "geomdefs.hh"
#include <vector>
#include "types.hh"

class G4Step;
class G4HCofThisEvent;
class DetectorConstruction;

class SensitiveStrip : public SensitiveDetector
{
  public:
       SensitiveStrip(G4String, DetectorConstruction*, G4VPhysicalVolume*,
               PositionSystem*, G4int, G4int, G4int, G4int,
               EDetectorType = kOtherDetector, EStripProj = kNoStripProj, EStripProj = kNoStripProj, G4double spreading = -1., G4bool inherited = false);
/*       SensitiveStrip(G4String, DetectorConstruction*, G4int,
                                             G4ThreeVector, G4ThreeVector, EAxis = kYAxis);*/
      ~SensitiveStrip();
 
       void Initialize(G4HCofThisEvent*);
       G4bool ProcessHits(G4Step*, G4TouchableHistory*);
       void EndOfEvent(G4HCofThisEvent*);
       G4int ComputeStripX(const G4double&);
       G4int ComputeStripX(const G4ThreeVector&);
       G4int ComputeStripY(const G4double&);
       G4int ComputeStripY(const G4ThreeVector&);
       G4int ComputeStripZ(const G4double&);
       G4int ComputeStripZ(const G4ThreeVector&);
       StripIndex ComputeStrip(G4ThreeVector&);
       inline EStripProj GetStripProj() { return acrossaxis; };
       inline EStripProj GetAlongProj() { return alongaxis; };
       G4bool IsXStripProj() { return acrossaxis == kXStripProj; };
       G4bool IsYStripProj() { return acrossaxis == kYStripProj; };
       G4bool IsZStripProj() { return acrossaxis == kZStripProj; };
       G4double GetStripPosY(const G4int&);
       G4double GetStripPosZ(const G4int&);
       G4ThreeVector GetStripPos(StripIndex);
       G4ThreeVector GetStripPos(G4int, G4int);
       G4ThreeVector GetStripPos(G4int, EStripProj = kBothStripProj);
       G4ThreeVector GetStripDims(EStripProj = kBothStripProj);
       G4int GetDetectorPart() { return detPart; };
       G4double GetFullWidth() { return 2*SelectByStripProj(acrossaxis, fdims.x(), fdims.y(), fdims.z(), 0.0); };
       G4double GetStripPitch() { return GetFullWidth()/SelectByStripProj(acrossaxis, fNstripsX, fNstripsY, fNstripsZ, -1); };
       G4double GetStripOffset() { return stripoffset; };
       inline void SetStripSpreading(const G4double& spr) {stripspreading = spr;};
       inline void SetStripOffset(const G4double& o) { stripoffset = o; };
       
  protected:
       G4double ComputeWeight(const TrackPoint& pt, G4double edep);
       G4double ComputeChargeDensity(G4double hit, G4double pos);
       G4double ComputeChargeBetween(G4double hit, G4double a, G4double b); //!< int(ComputeChargeDensity(hit,pos), pos=a..b)
       inline G4bool UseSpreading() { return stripspreading > 0 ; };
  
  protected:
       PositionSystem* posSystem;
       HitsCollectionStrip* hitsCollection;
       EStripProj acrossaxis;
       EStripProj alongaxis;
       G4int fNstripsX, fNstripsY, fNstripsZ;
       std::vector<G4double> etotX, etotY, etotZ;
       G4double stripspreading;
       G4double stripoffset;
};

#endif

