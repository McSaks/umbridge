/*        //                                          \\ 
//       /  \                                        /  \ 
//      /o  o\      Data to be written to file      /o  o\ 
//     /  ..  \                                    /  ..  \ 
//    /__\__/__\                                  /__\__/__\ 
*/

#ifndef EventData_h
#define EventData_h 1

#include "globals.hh"
#include "PositionSystem.hh"
#include "EnergySystem.hh"
#include "Calo3DSystem.hh"
#include "ScintillationSystem.hh"
#include <vector>
#include <fstream>

//class TrackPoints;

/* * * * * * * * *\ 
|* PositionData  *|
\* * * * * * * * */

struct TrackUnit
{
  public:
    TrackUnit(const TrackPoint&);
    TrackUnit& operator=(const TrackPoint&);
    TrackUnit& operator=(const TrackUnit&);
    G4int stripNo;
    G4double edep;
};

struct LayerUnit
{
  std::vector<TrackUnit> Y, Z;
};
typedef std::vector< LayerUnit > TrackUnits;
/*class TrackUnits : public std::vector<TrackUnit>
{
  public:
    TrackUnits() { };
};*/

class PositionData
{
  public: // constructors / destructors
    PositionData(EventData*);
  
  public: // other
    void Clear();
  
  protected: // fields
    EventData* evd;
  public:
    TrackUnits C, CDmidi, CC1;
    TrackPoints trackPoints;
};


/* * * * * * * * *\ 
|*   TimeData    *|
\* * * * * * * * */

struct TimeUnit
{
  public:
    TimeUnit() /*: actuationtime(kNotActuated), edep(0)*/ { };
    //TimeUnit(const TimePoint&);
    //TimeUnit& operator=(const TimePoint&);
    //TimeUnit& operator=(const TimeUnit&);
    //TimeUnit& operator+=(const TimeUnit&);
    static G4double kNotActuated;
    //G4bool operator<(const TimeUnit&);
    //G4bool operator==(const TimeUnit&);
    
    //G4double actuationtime;
    //G4double edep;
    ScintStripLayers* data;
};
// typedef std::vector<TimeUnit> TimeUnits;

class TimeData
{
  public: // constructors / destructors
    TimeData(EventData*);
    //TimeData(const TimePoint&);
  
  public: // other
    inline void Clear() EMPTY
  
  protected: // fields
    EventData* evd;
  public:
    TimeUnit topAC;
    TimeUnit  sideAC_left, sideAC_front, sideAC_right, sideAC_back;
    TimeUnit S1, S2, S3, S4;
};


/* * * * * * * * *\ 
|*  EnergyData   *|
\* * * * * * * * */

struct EnergyUnit
{
  public:
    EnergyUnit() : edep(0) { };
    EnergyUnit(const TrackPoint&);
    EnergyUnit(const EnergyPoint&);
    EnergyUnit& operator=(const EnergyPoint&);
    EnergyUnit& operator=(const EnergyUnit&);
    G4double edep;
};

typedef std::vector<EnergyUnit> EnergyUnits;

class EnergyData
{
  public: // constructors / destructors
    EnergyData(EventData*);
  
  public: // other
    inline void Clear() { };
  
  protected: // fields
    EventData* evd;
  public:
    EnergyUnits CC1;
    EnergyUnit /*topAC, S1, S2, S3, S4,*/ CC2;
};


/* * * * * * * * *\ 
|*  Calo3DData   *|
\* * * * * * * * */

struct Calo3DUnit
{
  public:
    Calo3DUnit() : edep(0) { };
    G4double edep;
    Edep3D* array;
};

class Calo3DData
{
  public: // constructors / destructors
    Calo3DData(EventData*);
  
  public: // other
    inline void Clear() { };
  
  protected: // fields
    EventData* evd;
  public:
    Calo3DUnit CC2;
    G4bool active;
};



/* * * * * * * * *\ 
|*   EventData   *|
\* * * * * * * * */

class EventData
{
  
  
  /* CONSTRUCTORS / DESTRUCTORS */
  
  public:
    EventData(DetectorConstruction*, G4String ofile = "out/default.eventdata");
   ~EventData();
  
  
  /* METHODS */
  
  public: // get / set
    inline G4bool ok() { return _ok; };
    inline void Reject() { _ok = false; };
    inline DetectorConstruction* GetDetectorConstruction() { return det; };
    inline void SetDetectorConstruction(DetectorConstruction* d) { det = d; };
    inline void SetOption(EScintStripOutput o) { scintstripoutput = o; };
  
  public: // other
    void Clear();
    void Write(); // write one event to file
  
  private: // subroutines of Write()
    void WriteRecSep() { outfile << "\n"; };
    void WriteEvtSep() { outfile << "\n" << G4endl; };
    void WriteString(const char* str) { outfile << str << std::flush; };
    void WritePD(const TrackUnits&, const char* name);
    void AfterPD() { };
    void WriteTD(const TimeUnit&, const char* name);
    void AfterTD() { outfile << backspace; };
    void WriteED(const EnergyUnit&, const char* name = NULL);
    void AfterED() { outfile << backspace << "\n"; };
    void Write3D(const Calo3DUnit&, const char* name);
    void After3D() { /*outfile << backspace << "\n";*/ };
  
  
  /* FIELDS */
    
  protected: // pointers
    DetectorConstruction* det;
  
  public: // subdata
    PositionData  positionData;
        TimeData      timeData;
      EnergyData    energyData;
      Calo3DData    calo3dData;
  
  private: // flags
    G4bool _ok;
  public: // flags
    G4bool use;
    G4bool write_structured;
    G4bool write_trackPoints;
    EScintStripOutput scintstripoutput;
    //G4bool write_position_energy;
  
  protected: // files
    G4String outfilename;
    std::ofstream outfile;
  
};

#endif
