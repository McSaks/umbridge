#ifndef Trigger_h
#define Trigger_h 1

#include "globals.hh"
#include "types.hh"

class DetectorConstruction;
class PositionSystem;
class ACSystem;
class ToFSystem;

class Trigger
{
  public:
    Trigger(DetectorConstruction* det = NULL, PositionSystem* rec = NULL,
            ToFSystem* tof = NULL, ACSystem* ac = NULL, E_use_trigger use = kTrigger_all, G4bool time_selection = true)
      : detector(det), posSystem(rec), ToF(tof), AC(ac), use_trigger(use), ToFTimeSelection(time_selection) { };
    void SetDetectorConstruction(DetectorConstruction* det) {detector = det;};
    void SetPositionSystem(PositionSystem* rec) {posSystem = rec;};
    void SetToFSystem(ToFSystem* tof) {ToF = tof;};
    void SetACSystem(ACSystem* ac) {AC = ac;};
    
    G4bool IsGoodEvent() const;
    operator G4bool() const { return IsGoodEvent(); };
  
  protected:
    DetectorConstruction* detector;
    PositionSystem* posSystem;
    ToFSystem* ToF;
    ACSystem* AC;
    E_use_trigger use_trigger;
    G4bool ToFTimeSelection;
};

#endif
