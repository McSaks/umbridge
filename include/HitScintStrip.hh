/*        //                                          \\ 
//       /  \                                        /  \ 
//      /o  o\       Hit in the Strip Detector      /o  o\ 
//     /  ..  \                                    /  ..  \ 
//    /__\__/__\                                  /__\__/__\ 
*/

#ifndef HitScintStrip_h
#define HitScintStrip_h 1

#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4Allocator.hh"
#include "G4ThreeVector.hh"
#include "types.hh"

class HitScintStrip : public G4VHit
{
  public:

      HitScintStrip();
     ~HitScintStrip();
      HitScintStrip(const HitScintStrip&);
      const HitScintStrip& operator=(const HitScintStrip&);
      G4int operator==(const HitScintStrip&) const;

      inline void* operator new(size_t);
      inline void  operator delete(void*);

      void Print();

  public:
  
      void SetTrackID    (G4int track)       { trackID = track; };
      void SetStripNum   (StripIndex strip)  { stripNum = strip; };
      void SetEdep       (G4double de)       { edep = de; };
      void SetPos        (G4ThreeVector xyz) { pos = xyz; };
      
      G4int GetTrackID()      { return trackID; };
      StripIndex GetStripNum() { return stripNum; };
      G4double GetEdep()      { return edep; };
      G4ThreeVector GetPos()  { return pos; };
      
  private:
  
      G4int         trackID;
      StripIndex    stripNum;
      G4double      edep;
      G4ThreeVector pos;
};

typedef G4THitsCollection<HitScintStrip> HitsCollectionScintStrip;

extern G4Allocator<HitScintStrip> hitAllocatorScintStrip;

inline void* HitScintStrip::operator new(size_t)
{
  void* aHit;
  aHit = (void*) hitAllocatorScintStrip.MallocSingle();
  return aHit;
}

inline void HitScintStrip::operator delete(void* aHit)
{
  hitAllocatorScintStrip.FreeSingle((HitScintStrip*) aHit);
}






#endif
