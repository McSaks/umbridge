/*        //                                          \\ 
//       /  \             Event Action               /  \ 
//      /o  o\                                      /o  o\ 
//     /  ..  \       begin and end of event       /  ..  \ 
//    /__\__/__\                                  /__\__/__\ 
*/

#ifndef EventAction_h
#define EventAction_h 1

#include "globals.hh"
#include "G4UserEventAction.hh"

class G4Event;
class PositionSystem;
class ToFSystem;
class ACSystem;
class EnergySystem;
class Calo3DSystem;
class Trigger;
class RunAction;
class EventData;

class EventAction : public G4UserEventAction
{
  public:
    EventAction(Trigger*);
   ~EventAction();

  public:
    void BeginOfEventAction(const G4Event*);
    void EndOfEventAction(const G4Event*);
    
    size_t ResetGoodCounter()
               { size_t old = good_event_id; good_event_id = 0; return old; };
    operator G4bool() const;
    void SetRunAction(RunAction* r) { runaction = r; };
    void SetEventData(EventData* d) { data = d; };
    void SetSystem(PositionSystem* sys) { posSystem = sys; };
    void SetSystem(EnergySystem* sys)   { egySystem = sys; };
    void SetSystem(ToFSystem* sys)      { ToF = sys; };
    void SetSystem(ACSystem* sys)       { AC = sys; };
    void SetSystem(Calo3DSystem* sys)   { calo3dSystem = sys; };
  
  private:
    PositionSystem*      posSystem;
    EnergySystem*        egySystem;
    Calo3DSystem*        calo3dSystem;
    ToFSystem* ToF;
    ACSystem*  AC;
    Trigger* trigger;
    size_t good_event_id;
    RunAction* runaction;
    EventData* data;
};

#endif

    
