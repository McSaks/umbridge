/*        //                                          \\ 
//       /  \         Volume Parameterisation        /  \ 
//      /o  o\                                      /o  o\ 
//     /  ..  \     parameterize layered systems   /  ..  \ 
//    /__\__/__\                                  /__\__/__\ 
*/

#ifndef Parameterisation_H
#define Parameterisation_H 1

#include "globals.hh"
#include "G4VPVParameterisation.hh"
#include "G4ThreeVector.hh"
#include <vector>

class G4VPhysicalVolume;
class G4Box;

// Dummy declarations to get rid of warnings ...
class G4Trd;
class G4Trap;
class G4Cons;
class G4Orb;
class G4Sphere;
class G4Torus;
class G4Para;
class G4Hype;
class G4Tubs;
class G4Polycone;
class G4Polyhedra;

class Parameterisation : public G4VPVParameterisation
{ 
  public:
  
    Parameterisation(G4int nlayers, const std::vector<G4ThreeVector>& pos, const std::vector<G4ThreeVector>& dims);

    virtual				 
   ~Parameterisation();
    
    std::vector<G4ThreeVector> GetLocalPosition() {return fpos;};
    std::vector<G4ThreeVector> GetDimentions() {return fdims;};
    
    void ComputeTransformation (const G4int copyNo,
                                G4VPhysicalVolume* physVol) const;
    
    void ComputeDimensions (G4Box & trackerLayer, const G4int copyNo,
                            const G4VPhysicalVolume* physVol) const;
    
    G4ThreeVector GetPosition  (G4int layerNo) {return fpos[layerNo];};
    G4ThreeVector GetDimentions(G4int layerNo) {return fdims[layerNo];};
    
  private:  // Dummy declarations to get rid of warnings ...

    void ComputeDimensions (G4Trd&,const G4int,const G4VPhysicalVolume*) const { };
    void ComputeDimensions (G4Trap&,const G4int,const G4VPhysicalVolume*) const { };
    void ComputeDimensions (G4Cons&,const G4int,const G4VPhysicalVolume*) const { };
    void ComputeDimensions (G4Sphere&,const G4int,const G4VPhysicalVolume*) const { };
    void ComputeDimensions (G4Orb&,const G4int,const G4VPhysicalVolume*) const { };
    void ComputeDimensions (G4Torus&,const G4int,const G4VPhysicalVolume*) const { };
    void ComputeDimensions (G4Para&,const G4int,const G4VPhysicalVolume*) const { };
    void ComputeDimensions (G4Hype&,const G4int,const G4VPhysicalVolume*) const { };
    void ComputeDimensions (G4Tubs&,const G4int,const G4VPhysicalVolume*) const { };
    void ComputeDimensions (G4Polycone&,const G4int,const G4VPhysicalVolume*) const { };
    void ComputeDimensions (G4Polyhedra&,const G4int,const G4VPhysicalVolume*) const { };
  private:

    G4int fnlayers;   
    std::vector<G4ThreeVector> fpos;  // [fnlayers]
    std::vector<G4ThreeVector> fdims; // [fnlayers]
};

#endif


