/*        //                                          \\ 
//       /  \                                        /  \ 
//      /o  o\       Hit in the Strip Detector      /o  o\ 
//     /  ..  \                                    /  ..  \ 
//    /__\__/__\                                  /__\__/__\ 
*/

#ifndef HitStrip_h
#define HitStrip_h 1

#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4Allocator.hh"
#include "G4ThreeVector.hh"
#include "types.hh"

class HitStrip : public G4VHit
{
  public:

      HitStrip();
     ~HitStrip();
      HitStrip(const HitStrip&);
      const HitStrip& operator=(const HitStrip&);
      G4int operator==(const HitStrip&) const;

      inline void* operator new(size_t);
      inline void  operator delete(void*);

      void Draw();
      void Print();

  public:
  
      void SetTrackID    (G4int track)       { trackID = track; };
      void SetStripNum   (StripIndex strip)   { stripNum = strip; };
      void SetEdep       (G4double de)       { edep = de; };
      void SetPos        (G4ThreeVector xyz) { pos = xyz; };
      
      G4int GetTrackID()      { return trackID; };
      StripIndex GetStripNum(){ return stripNum; };
      G4double GetEdep()      { return edep; };
      G4ThreeVector GetPos()  { return pos; };
      
  private:
  
      G4int         trackID;
      StripIndex    stripNum;
      G4double      edep;
      G4ThreeVector pos;
};

typedef G4THitsCollection<HitStrip> HitsCollectionStrip;

extern G4Allocator<HitStrip> hitAllocatorStrip;

inline void* HitStrip::operator new(size_t)
{
  void* aHit;
  aHit = (void*) hitAllocatorStrip.MallocSingle();
  return aHit;
}

inline void HitStrip::operator delete(void* aHit)
{
  hitAllocatorStrip.FreeSingle((HitStrip*) aHit);
}

#endif
