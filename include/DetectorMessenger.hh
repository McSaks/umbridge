
#ifndef DetectorMessenger_h
#define DetectorMessenger_h 1

#include "globals.hh"
#include "G4UImessenger.hh"

class DetectorConstruction;
class PositionSystem;
class PrimaryGeneratorAction;
class G4UIdirectory;
class G4UIparameter;
class G4UIcmdWithAString;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWithAnInteger;
class G4UIcmdWithoutParameter;

class DetectorMessenger: public G4UImessenger
{
  public:
    DetectorMessenger(DetectorConstruction* = NULL, PositionSystem* = NULL);
   ~DetectorMessenger();
    inline void SetPrimaryGeneratorAction(PrimaryGeneratorAction* gun_) { gun=gun_; };
    inline void SetDetectorConstruction(DetectorConstruction* det_) { myDetector=det_; };
    inline void SetPositionSystem(PositionSystem* pos_) { myReconstruction=pos_; };
    
    void SetNewValue(G4UIcommand*, G4String);
    G4String GetCurrentValue(G4UIcommand*);
    
  private:
    DetectorConstruction* myDetector;
    PositionSystem* myReconstruction;
    PrimaryGeneratorAction* gun;
    
    G4UIdirectory*             projDir;
    G4UIdirectory*             detDir;
    G4UIdirectory*             drawDir;
    G4UIdirectory*             gunRandom;
    G4UIdirectory*             gunRandomTrigger;
    G4UIcmdWithoutParameter*   DoNothing_cmd;
    G4UIcmdWithAString*        SetMaterial_world_cmd;
    G4UIcmdWithADoubleAndUnit* StepMax_cmd;
    G4UIcmdWithoutParameter*   Scale_cmd;
    G4UIcmdWithoutParameter*   Descale_cmd;
    G4UIcmdWithAnInteger*      Verbose_cmd;
    G4UIcommand*               SetAngle_cmd;
          G4UIparameter*           SetAngle_theta;
          G4UIparameter*           SetAngle_phi;
    G4UIcommand*               SetRandomPosition_cmd;
          G4UIparameter*           SetRandomPosition_X;
          G4UIparameter*           SetRandomPosition_Y;
          G4UIparameter*           SetRandomPosition_Z;
          G4UIparameter*           SetRandomPosition_unit;
          G4UIparameter*           SetRandomPosition_dX;
          G4UIparameter*           SetRandomPosition_dY;
          G4UIparameter*           SetRandomPosition_dZ;
          G4UIparameter*           SetRandomPosition_dunit;
    G4UIcommand*               SetRandomDirection_cmd;
          G4UIparameter*           SetRandomDirection_type;
          G4UIparameter*           SetRandomDirection_par1;
    G4UIcommand*               SetRandomDirectionMaxAngle_cmd;
          G4UIparameter*           SetRandomDirectionMaxAngle_angle;
    G4UIcommand*               SetRandomDirectionMeanDir_cmd;
          G4UIparameter*           SetRandomDirectionMeanDir_theta;
          G4UIparameter*           SetRandomDirectionMeanDir_phi;
//    G4UIcmdWithoutParameter*   DrawTrack_cmd;
    G4UIcmdWithoutParameter*   DrawHitted_cmd;
    G4UIcommand*               SetRandomTriggerAddCuboid_cmd;
          G4UIparameter*           SetRandomTriggerAddCuboid_X;
          G4UIparameter*           SetRandomTriggerAddCuboid_Y;
          G4UIparameter*           SetRandomTriggerAddCuboid_Z;
          G4UIparameter*           SetRandomTriggerAddCuboid_unit;
          G4UIparameter*           SetRandomTriggerAddCuboid_dX;
          G4UIparameter*           SetRandomTriggerAddCuboid_dY;
          G4UIparameter*           SetRandomTriggerAddCuboid_dZ;
          G4UIparameter*           SetRandomTriggerAddCuboid_dunit;
    G4UIcmdWithAString*        SetRandomTriggerInvert_cmd;
    G4UIcmdWithoutParameter*   SetRandomTriggerClear_cmd;
    G4UIcmdWithoutParameter*   GunShow_cmd;
    G4UIcmdWithAString*        ControlExecute_cmd;
};

#endif

