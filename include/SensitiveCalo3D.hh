/*        //                                          \\ 
//       /  \                                        /  \ 
//      /o  o\             3D Sensitive             /o  o\ 
//     /  ..  \        Calorimeter Detector        /  ..  \ 
//    /__\__/__\                                  /__\__/__\ 
*/

#ifndef SensitiveCalo3D_h
#define SensitiveCalo3D_h 1

#include "SensitiveDetector.hh"
#include "G4ThreeVector.hh"
#include "DetectorConstruction.hh"
#include "HitCalo3D.hh"
#include "Calo3DSystem.hh"
#include "geomdefs.hh"
#include <vector>
#include "types.hh"

class G4Step;
class G4HCofThisEvent;
class DetectorConstruction;


struct Index3D
{
  Index3D(G4int xx = 0, G4int yy = 0, G4int zz = 0) : x(xx), y(yy), z(zz) { };
  G4bool IsOutOfVolume() const { return x == kOutOfVolume; };
  G4int x, y, z;
  friend std::ostream& operator<<(std::ostream& os, const Index3D& i) {
    return os << "Index3D(" << i.x << ", " << i.y << ", " << i.z << ")";
  };
  operator G4String() { std::ostringstream buf; buf << *this; return buf.str(); };
  operator const char*() { return (operator G4String()).c_str(); };
};

class Edep3D : public std::vector <std::vector <std::vector <G4double> > >
{
 public:
  Edep3D() : nitemsX(0) EMPTY;
  //inline void Reserve(const Index3D& i) { Reserve(i.x, i.y, i.z); };
  //void Reserve(G4int ix, G4int iy, G4int iz);
  void Reserve(G4int nx, const vector<G4int>& ny, const vector<G4int>& nz);
  inline void Add(const G4int& i, G4double dE) { operator[](i) += dE; };
  void Reset();
  
  /* Get: */
  //inline Index3D GetItemCount() const { return Index3D(nitemsX, nitemsY, nitemsZ); };
  inline G4int GetItemCountX() const { return nitemsX; };
  inline G4int GetItemCountY(G4int x) const { return nitemsY.at(x); };
  inline G4int GetItemCountZ(G4int x) const { return nitemsZ.at(x); };
  inline G4int GetTotalItemCount() const;
  
  /* Indexations: */
  inline G4double operator()(G4int ix, G4int iy, G4int iz) const { return at(ix).at(iy).at(iz); };
  inline G4double operator[](const Index3D& i) const { return at(i.x).at(i.y).at(i.z); };
  inline G4double& operator()(G4int ix, G4int iy, G4int iz) { return at(ix).at(iy).at(iz); };
  inline G4double& operator[](const Index3D& i) { return at(i.x).at(i.y).at(i.z); };
  inline G4double operator[](G4int j) const { return operator[](ThreedeeizeIndex(j)); };
  inline G4double& operator[](G4int j) { return operator[](ThreedeeizeIndex(j)); };
  G4int   FlattenIndex(const Index3D& i) const;
  Index3D ThreedeeizeIndex(G4int j) const;
 public:
  vector<G4double> z_default;
  vector< vector<G4double> > yz_default;
  G4int nitemsX;
  vector<G4int> nitemsY, nitemsZ;
};

class SensitiveCalo3D : public SensitiveDetector
{
  public:
       SensitiveCalo3D(G4String, DetectorConstruction*, G4VPhysicalVolume*,
               Calo3DSystem*, G4int xrows, const vector<G4int>& yrows, const vector<G4int>& zrows, const vector<G4double>& ywidths, const vector<G4double>& zwidths, EDetectorType type = kOtherDetector);
/*       SensitiveStrip(G4String, DetectorConstruction*, G4int,
                                             G4ThreeVector, G4ThreeVector, EAxis = kYAxis);*/
      ~SensitiveCalo3D();
 
       enum EItemDir { kXDir, kYDir, kZDir };
       
       void Initialize(G4HCofThisEvent*);
       G4bool ProcessHits(G4Step*, G4TouchableHistory*);
       void EndOfEvent(G4HCofThisEvent*);
       void clear();
       //inline G4int ComputeItemX(const G4double& a) const { return ComputeItem(a, kXDir); };
       inline G4int ComputeItemX(const G4ThreeVector& r) const { return ComputeItem(r).x; };
       //inline G4int ComputeItemY(const G4double& a) const { return ComputeItem(a, kYDir); };
       inline G4int ComputeItemY(const G4ThreeVector& r) const { return ComputeItem(r).y; };
       //inline G4int ComputeItemZ(const G4double& a) const { return ComputeItem(a, kZDir); };
       inline G4int ComputeItemZ(const G4ThreeVector& r) const { return ComputeItem(r).z; };
       //G4int   ComputeItem(const G4double&, const EItemDir&) const;
       Index3D ComputeItem(const G4ThreeVector&) const;
       //G4double      GetItemCenter(const G4int&, const EItemDir&) const;
       G4ThreeVector GetItemCenter(const Index3D&) const;
       G4double      GetItemDims(G4int layer, const EItemDir&) const;
       G4ThreeVector GetItemDims(G4int layer) const;
       //G4double GetHalfWidthY(G4int layer) const;
       //G4double GetHalfWidthZ(G4int layer) const;
       
  protected:
  
  protected:
       Calo3DSystem* system;
       HitsCollectionCalo3D* hitsCollection;
       G4int nitemsX;
       vector<G4int> nitemsY, nitemsZ;
       vector<G4double> widthY, widthZ;
       vector<G4double> offsetY, offsetZ;
       G4double etot;
       Edep3D dEArray;
};

#endif

