#ifndef BoxUnion_h
#define BoxUnion_h 1


#include "BoxVolume.hh"

using std::vector;


#if 0
class BoxUnion
{
  public:
    BoxUnion(const G4String& name_ = "unnamed");
    BoxUnion(G4VSolid&);
    BoxUnion(G4VSolid*);
   ~BoxUnion();
    
    BoxUnion& operator|=(G4VSolid* that);
    BoxUnion& operator&=(G4VSolid* that);
    BoxUnion& operator-=(G4VSolid* that);
    operator G4bool() { return true; };
    operator G4VSolid*() { return solid; };
    operator G4VSolid&() { return *solid; };
    
    inline void SetTranslation(const G4ThreeVector& v = G4ThreeVector()) { translation = v; };
    inline void AddTranslation(const G4ThreeVector& v) { translation += v; };
    inline void SetName(const G4String& n) { name = n; };
    inline G4String GetName() { return name; };
    inline G4VSolid* GetSolid() { return solid; };
    
    void Reserve(size_t n) { solids.reserve(n); };
    
    
  private:
    BoxUnion(const BoxUnion&); // No copy constructor
    BoxUnion& operator=(const BoxUnion&); // nither copy operator
    G4String UniquifyName();
    
  protected:
    G4String name;
    vector<G4VSolid*> solids;
    size_t copyNo;
    G4VSolid* solid;
    G4ThreeVector translation;
};
#endif

class BoxUnionVolume// : public BoxVolume
{
  public:
    struct PosDims {
      G4ThreeVector pos, dims;
      PosDims(const G4ThreeVector& pos_, const G4ThreeVector& dims_) : pos(pos_), dims(dims_) { };
    };
  
  public:
    BoxUnionVolume();
   ~BoxUnionVolume();
    
    void CreateVolume(G4bool multiple = false);
    void CreateVolume(G4double& currentpos, const G4double& gap_after = microgap, G4bool multiple = false);
    void SetPos(G4double& currentpos, const G4double& gap_after = microgap);
    void SetSensitiveDetector() { if(sensitive) if(logic) logic->SetSensitiveDetector(sensitive); };
    operator G4bool() { return dims.x() && dims.y() && dims.z(); /*return solid;*/ };
    inline G4bool operator!() { return !operator G4bool(); };
    inline void SetName(const G4String& n) { name = n; };
    inline G4String GetName() { return name; };
    inline void SetMother(BoxVolume& mommy) { mother = &mommy; };
    inline void SetMater(G4Material* stuff) { mater = stuff; };
    void push(const G4ThreeVector& pos, const G4ThreeVector& dims);
  
  public:
    G4ThreeVector pos, dims;
    G4String name;
    BoxVolume* mother;
    Attr attr;
  
  public:
    G4VSolid* solid;
    G4LogicalVolume* logic;
    vector<G4VPhysicalVolume*> physics;
    G4Material* mater;
    SensitiveDetector* sensitive;
    vector<PosDims> posdims;
    vector<BoxVolume> volumes;
  
  private:
    G4String UniquifyName();
    size_t copyNo;
    //template<class T> inline static void DELETE(T* ptr) { if(ptr) delete ptr; };
};

#endif
