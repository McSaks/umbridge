/*        //                                          \\ 
//       /  \         Detector Construction          /  \ 
//      /o  o\                                      /o  o\ 
//     /  ..  \   dimentions, volumes, materials   /  ..  \ 
//    /__\__/__\                                  /__\__/__\ 
*/

#ifndef DetectorConstruction_h
#define DetectorConstruction_h 1

#include "globals.hh"
#include "G4VUserDetectorConstruction.hh"
#include "G4PVPlacement.hh"
#include "G4ThreeVector.hh"
#include "Parameterisation.hh"

#include "PositionSystem.hh"
//#include "TimeOfFlightSystem.hh"
#include "EnergySystem.hh"
#include "Calo3DSystem.hh"
#include "ScintillationSystem.hh"
#include "DetectorMessenger.hh"

#include "BoxVolume.hh"
#include "BoxUnion.hh"

#include <vector>
using std::vector;

class G4Box;
class G4LogicalVolume;
class G4VPhysicalVolume;
class G4Material;
class G4VPVParameterisation;
class G4UserLimits;
class G4VSensitiveDetector;
class Geometry;

class DetectorConstruction : public G4VUserDetectorConstruction
{
  public:
  
     DetectorConstruction(G4String geofile);
    ~DetectorConstruction();

  public:
  
     G4VPhysicalVolume* Construct();
     void PrintAllPosAndDims();
     void PrintThickness();
     
     inline void SetSystem(PositionSystem* sys) { posSystem = sys; };
     inline void SetSystem(EnergySystem* sys) { caloSystem = sys; };
     inline void SetSystem(Calo3DSystem* sys) { cc2System = sys; };
     inline void SetSystem(ACSystem* sys) { AC = sys; };
     inline void SetSystem(ToFSystem* sys) { ToF = sys; };
     
     G4double GetTotalHight()             const  {return 2*world.dims.x();};
     BoxVolume* GetWorld()                       {return &world;};
     
     G4ThreeVector GetDimentions_world()  const  {return world.dims;};
     DetectorMessenger* GetMessenger()    const  {return detectorMessenger;};
     void SetMessenger(DetectorMessenger* mes)  {detectorMessenger = mes;};
     void SetMaterial_world(G4String);
     void SetMaxStep(G4double);
     inline void SetOutputFile(const G4String& str) {outfile = str;};
     void SetCompatibilityMode();
     
  public:
     
     G4String outfile;
     G4double microgap_;
     
     PositionSystem*  posSystem;
     EnergySystem*   caloSystem;
     Calo3DSystem*   cc2System;
     ACSystem*   AC;
     ToFSystem*   ToF;
     G4UserLimits* stepLimit;
     DetectorMessenger* detectorMessenger;
  private:
     G4PVPlacement* PseudoParameterised(G4String,
            G4LogicalVolume*, Parameterisation*, G4int);
  
  protected:
     Geometry* geo;
  
  public:
  
       G4Material* materW     ,*  materSi    ,*  materVacuum  ,*  materBGO   ,*
                   materPEth  ,*  materPSty  ,*  materAl16    ,*  materAl48  ,*
                   materCFRP  ,*  materCsI   ,*  materMirror  ,*  materAl    ,*
                   materAl108 ,*  materPMMA  ,*  materLiFZnS  ,*  materBottomStuff ;
  
       ///////////////////////////////////////////////////////////////////////////////
      //------- Declare solids, logical and physical volumes and materials --------//
     ///////////////////////////////////////////////////////////////////////////////
       enum ESide {kY0, kY1, kZ0, kZ1};

       BoxVolume  world;
       BoxVolumes topAC;
       std::vector<BoxVolumes> sideAC;
       //BoxVolume  C;
       BoxVolumes CLayerW;
       BoxVolumes CLayerSiY;
       BoxVolumes CLayerSiZ;
       BoxVolumes S1;
       //BoxVolume  SiArray1;
       //BoxVolume  SiArray2;
       BoxVolumes TD;
       BoxVolumes S2;
       //BoxVolume  CC1;
       BoxVolumes CC1LayerCsI;
       BoxVolumes CC1LayerSiY;
       BoxVolumes CC1LayerSiZ;
       BoxVolumes S3;
       BoxVolume  CC2_single;
       BoxVolumes S4;
       
       BoxVolume  ND_pEth1, ND_pEth2, ND_pEth3;
       BoxVolume  ND_PMMA1, ND_PMMA2, ND_PMMA3, ND_PMMA4;
       BoxVolume  ND_LiFZnS1, ND_LiFZnS2;
       
       BoxVolume  topACSupportTopCFRP;
       BoxVolume  topACSupportAl;
       BoxVolume  topACSupportBotCFRP;
       BoxVolumes CSupportAl;
       BoxVolumes CSupportTopCFRP;
       BoxVolumes CSupportBotCFRP;
       BoxVolume  S1SupportTopCFRP;
       BoxVolume  S1SupportAl;
       BoxVolume  S1SupportBotCFRP;
/*     BoxVolume  SiArray1SupportTopCFRP;
       BoxVolume  SiArray1SupportAl;
       BoxVolume  SiArray1SupportBotCFRP;   */
/*     BoxVolume  SiArray2SupportTopCFRP;
       BoxVolume  SiArray2SupportAl;
       BoxVolume  SiArray2SupportBotCFRP;   */
       BoxVolume  S2SupportTopCFRP;
       BoxVolume  S2SupportAl;
       BoxVolume  S2SupportBotCFRP;
       BoxVolumes CC1SupportAl_CsI;
       BoxVolumes CC1SupportTopCFRP_CsI;
       BoxVolumes CC1SupportBotCFRP_CsI;
       BoxVolumes CC1SupportAl_Si;
       BoxVolumes CC1SupportTopCFRP_Si;
       BoxVolumes CC1SupportBotCFRP_Si;
       BoxVolume  S3SupportTopCFRP;
       BoxVolume  S3SupportAl;
       BoxVolume  S3SupportBotCFRP;
       BoxVolume  CC2SupportTopCFRP;
       BoxVolume  CC2SupportBotCFRP;
       //BoxVolume  AlPlate;
       BoxVolume  S4SupportTopCFRP;
       BoxVolume  S4SupportAl;
       BoxVolume  S4SupportBotCFRP;
       
       BoxVolume  NDSupportTopCFRP;
       BoxVolume  NDSupportAl;
       BoxVolume  NDSupportBotCFRP;
       
       BoxVolume  BottomStuff;
       
       G4bool CC2_is_segmented;
       G4bool sideAC_horizontal;
       BoxUnionVolume CC2;
       BoxUnionVolume CC2_CFRP_x, CC2_CFRP_y, CC2_CFRP_z;
       BoxUnionVolume CC2_mirrorfilm;
       BoxVolume  CC2SupportTopAl_o;
       BoxVolume  CC2SupportTopAl_i;
       BoxVolume  CC2SupportBotAl_i;
       BoxVolume  CC2SupportBotAl_o;
       
       enum EDetSide {kLeft/* -z */, kFront/* -y */, kRight/* +z */, kBack/* +y */};
       inline static std::string DetSideName(EDetSide side) {
	 switch (side) { case kLeft:  return "left";    case kFront: return "front";
                         case kRight: return "right";   case kBack:  return "back";
	                 default: return "OUT OF RANGE"; }
       }
       
       ///////////////////////////////////////////////////////////////////////////////
      //------------------------- Geometrical parameters --------------------------//
     ///////////////////////////////////////////////////////////////////////////////

     
   public:
       ///////////////////////////////////////////////////////////////////////////////
      //------------------------- Position and dimentions -------------------------//
     ///////////////////////////////////////////////////////////////////////////////
};

#endif
