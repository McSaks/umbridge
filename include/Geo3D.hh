#ifndef Geo3D_h
#define Geo3D_h 1

#include "G4ThreeVector.hh"
#include "G4SystemOfUnits.hh"

enum Eaxis_ {kXaxis = 1<<0, kYaxis = 1<<1, kZaxis = 1<<2};
typedef short Eaxis;

class Geo3D_fail
{
  public:
    Geo3D_fail() { };
    operator G4ThreeVector() { return G4ThreeVector(-2.565102*pc,4.999545555*cm,89.110084*um); };
    operator G4int() { return -99988; };
    operator G4double() {return -2.565102*pc; };
};

class Line3D
{
  public:
    Line3D(const G4ThreeVector& a_ = G4ThreeVector(), const G4ThreeVector& v_ = G4ThreeVector()) : a(a_), v(v_) { };
    inline void set(const G4ThreeVector& a_, const G4ThreeVector& v_) { a=a_; v=v_; };
    G4ThreeVector FindPoint(G4double, const Eaxis&) const;
  
  public:
    G4ThreeVector a, v;
};

class Cuboid3D
{
  public:
    Cuboid3D(const G4ThreeVector& r_ = G4ThreeVector(), const G4ThreeVector& d_ = G4ThreeVector()) : r(r_), d(d_) { };
    inline void set(const G4ThreeVector& r_, const G4ThreeVector& d_) { r=r_; d=d_; };
    enum EType_ {kBox = 0,
                kXPlane = 1<<0, kYPlane = 1<<1, kZPlane = 1<<2,
                kXLine = kYPlane|kZPlane, kYLine = kZPlane|kXPlane, kZLine = kXPlane|kYPlane,
                kPoint = kXPlane|kYPlane|kZPlane};
    typedef short EType;
    EType GetType() const;
  
  public:
    G4ThreeVector r, d;
};

// intersections:
G4bool operator&(const Line3D&, const Cuboid3D&);
inline G4bool operator&(const Cuboid3D& b, const Line3D& l) { return operator&(l, b); }


#endif
