/*        //                                          \\ 
//       /  \                                        /  \ 
//      /o  o\        Time-of-flight System         /o  o\ 
//     /  ..  \                                    /  ..  \ 
//    /__\__/__\                                  /__\__/__\ 
*/

#ifndef ScintillationSystem_h
#define ScintillationSystem_h 1

#include "globals.hh"
#include "G4ThreeVector.hh"
#include "HitScintStrip.hh"
#include <vector>
#include "types.hh"

class DetectorConstruction;
class DetectorMessenger;
class EventAction;
class EventData;


struct ScintStripPoint {
  G4double edep;
  ScintStripPoint(G4double e = 0) : edep(e) EMPTY;
  operator G4double() const { return edep; };
  friend std::ostream& operator<<
            (std::ostream& os, const ScintStripPoint& p)
             { return os << G4double(p); };
  ScintStripPoint& operator+=(const ScintStripPoint& p) { edep += p.edep; return *this; };
  G4bool operator>(const ScintStripPoint& p) { return edep > p.edep; };
  G4bool operator>(G4double e) { return edep > e; };
};

struct ScintStripIndex
{
  G4int s, p, t;
  ScintStripIndex(G4int S, G4int P, G4int T) : s(S), p(P), t(T) EMPTY;
  ScintStripIndex() : s(-1), p(-1), t(-1) { };
};

#include <vector>
using std::vector;

class ScintStripLayer;

class ScintStripSignal : public vector< vector < ScintStripPoint > >
{
  public:
    //ScintStripLayer* layer;
    G4double t_bin, t_min;
    G4double s_min, s_max; G4int s_nbins; G4double s_bin() const { return (s_max-s_min) / s_nbins; };
    
    G4double GetActuationTime() const;
    G4double GetActuationTime(G4int strip) const;
};

struct ScintStripSignals
{
  ScintStripSignal one, two;
  G4double GetActuationTime() const
      { return one.GetActuationTime() < two.GetActuationTime() ?
               one.GetActuationTime() : two.GetActuationTime(); };
};

class SensitiveScintStrip;

class ScintStripLayer : public vector< vector< vector< ScintStripPoint > > >
{
  public:
    
    SensitiveScintStrip* sensitive;
    EStripProj acrossproj;
    EStripProj alongproj;
    G4double s_min, s_max; G4int s_nbins; G4double s_bin() const { return (s_max-s_min) / s_nbins; };
    G4double p_min, p_max; G4int p_nbins; G4double p_bin() const { return (p_max-p_min) / p_nbins; };
    G4double t_min, t_max; G4int t_nbins; G4double t_bin() const { return (t_max-t_min) / t_nbins; };
    G4double speedoflight;
    
    ScintStripIndex FindIndex(const G4ThreeVector& hitpos, G4double time) const;
    const ScintStripPoint& operator()(G4int ss, G4int pp, G4int tt) const { return at(ss).at(pp).at(tt); };
    ScintStripPoint&       operator()(G4int ss, G4int pp, G4int tt)       { return at(ss).at(pp).at(tt); };
    const ScintStripPoint& operator[](const ScintStripIndex& i) const { return operator()(i.s, i.p, i.t); };
    ScintStripPoint&       operator[](const ScintStripIndex& i)       { return operator()(i.s, i.p, i.t); };
    inline const ScintStripPoint& operator()(const ScintStripIndex& i) const { return operator[](i); };
    inline ScintStripPoint&       operator()(const ScintStripIndex& i)       { return operator[](i); };
    
    ScintStripSignals GetSignal();
    ScintStripIndex Size() const { return ScintStripIndex(s_nbins,p_nbins,t_nbins); };
  
    void Initialize();
};
typedef vector<ScintStripLayer*> ScintStripLayers;

#define ScintStripLayerFOR(layer, it)                      \
  vFOR(vector< vector< ScintStripPoint > >, layer, _iter_1) \
    vFOR(vector< ScintStripPoint >, *_iter_1, _iter_2)       \
      vFOR(ScintStripPoint, *_iter_2, it)

#define ScintStripLayerFORc(layer, it)                      \
  vFORc(vector< vector< ScintStripPoint > >, layer, _iter_1) \
    vFORc(vector< ScintStripPoint >, *_iter_1, _iter_2)       \
      vFORc(ScintStripPoint, *_iter_2, it)


class ScintillationSystem
{
  public:
    ScintillationSystem() : data(0) EMPTY;
    ScintStripLayers layers;
    G4bool Trigger() const { /* _PLACEHOLDER_ */ return true; };
    G4bool Write() const;
    void Clear();
    EventData* data;
    void SetEventData(EventData* ev) { data = ev; };
};
    
#if 0
{
  public:
       
       // constructors / destructors :
       ScintillationSystem()
           : timePoints() { };
       ScintillationSystem(const TimePoints& pts)
           : timePoints(pts) { };
      ~ScintillationSystem() { /*Clear();*/ };
       
       // set / get :
       TimePoint& GetPoint(size_t i) { return timePoints[i]; };
       void SetPoint(size_t i, TimePoint pt) { timePoints[i] = pt; };
       TimePoint& operator[](size_t i) { return timePoints[i]; };
       const TimePoint& operator[](size_t i) const { return timePoints[i]; };
       void SetDetectorConstruction(DetectorConstruction* det) { detector = det; };
       void SetMessenger(DetectorMessenger* mess) { messenger = mess; };
       void SetEventAction(EventAction* ev) { currentEvent = ev; };
       void SetEventData(EventData* ev) { data = ev; };
       
       // others :
       size_t NPoints() const { return timePoints.size(); };
       void Append(const TimePoint&);
       ScintillationSystem& operator+=(const TimePoint& pt)
           { Append(pt); return *this; };
       void Join(const TimeOfFlightSystem*);
       ScintillationSystem& operator+=(const TimeOfFlightSystem& tr)
           { Join(&tr); return *this; };
       void Clear() { timePoints.clear(); };
       G4bool Trigger() const;
       G4bool Write() const;
       
       // // // // //
       
  protected:
       // input:
       TimePoints timePoints;
       // output:
       
       // pointers:
       DetectorConstruction* detector;
       DetectorMessenger* messenger;
       EventAction* currentEvent;
       EventData* data;
};
#endif


class ToFSystem : public ScintillationSystem
{ public:
  //ToFSystem() : S1(0), S2(0), S3(0), S4(0) EMPTY;
  ScintStripLayers S1, S2, S3, S4;
  G4bool Set();
  G4bool Write();
  void SetEventData(EventData* ev) { data = ev; Set(); };
  G4bool Trigger(G4bool timeSelection = true) const;
};

class ACSystem : public ScintillationSystem
{ public:
  //ACSystem() : topAC(0), sideAC_left(0), sideAC_front(0), sideAC_right(0), sideAC_back(0) EMPTY;
  ScintStripLayers topAC;
  ScintStripLayers sideAC_left, sideAC_front, sideAC_right, sideAC_back ;
  G4bool Set();
  G4bool Write();
  void SetEventData(EventData* ev) { data = ev; Set(); };
  G4bool Trigger() const;
};


#endif
