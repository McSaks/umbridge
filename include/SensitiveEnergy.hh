/*        //                                          \\ 
//       /  \                                        /  \ 
//      /o  o\       Energy Sensitive Detector      /o  o\ 
//     /  ..  \                                    /  ..  \ 
//    /__\__/__\                                  /__\__/__\ 
*/

#ifndef SensitiveEnergy_h
#define SensitiveEnergy_h 1

#include "SensitiveDetector.hh"
#include "G4ThreeVector.hh"
#include "HitEnergy.hh"
#include "DetectorConstruction.hh"
#include "EnergySystem.hh"
#include "geomdefs.hh"
#include "types.hh"

class G4Step;
class G4HCofThisEvent;
class DetectorConstruction;

class SensitiveEnergy : public SensitiveDetector
{
  public:
       SensitiveEnergy(G4String, DetectorConstruction*, G4VPhysicalVolume*,
               EnergySystem*, G4int detPart_, G4int detLayer,
               EDetectorType = kOtherDetector);
      ~SensitiveEnergy();
 
       void Initialize(G4HCofThisEvent*);
       G4bool ProcessHits(G4Step*, G4TouchableHistory*);
       void EndOfEvent(G4HCofThisEvent*);
       G4int GetDetectorPart() { return detPart; };
inline G4double GetEnergyDeposit() { return etot; };
inline G4double GetAmplitude() { return gain*etot; };
       
  private:
       G4double ComputeWeight(const TrackPoint& pt, G4double edep);
  
  private:
       EnergySystem* egySystem;
       HitsCollectionEnergy* hitsCollection;
       G4double etot;
       G4double gain; // = Signal / Etot
       G4int detPart;

};

#endif

