/*        //                                          \\ 
//       /  \                                        /  \ 
//      /o  o\      Primary Particle Generator      /o  o\ 
//     /  ..  \                                    /  ..  \ 
//    /__\__/__\                                  /__\__/__\ 
*/

#ifndef PrimaryGeneratorAction_h
#define PrimaryGeneratorAction_h 1

#include "G4VUserPrimaryGeneratorAction.hh"
#include "G4ThreeVector.hh"
#include "Geo3D.hh"
#include <vector>

class DetectorConstruction;
class G4ParticleGun;
class G4Event;
class RunAction;

class PrimaryGeneratorAction : public G4VUserPrimaryGeneratorAction
{
  public:
    PrimaryGeneratorAction(DetectorConstruction*);
   ~PrimaryGeneratorAction();
    inline void SetRunAction(RunAction* r) { run_action = r; };

  public:
    void GeneratePrimaries(G4Event*);
    G4double GetParticleEnergy();
    G4String GetParticleName();
    inline G4ParticleGun* GetParticleGun() const { return particleGun; };
  
  public:
    enum E_direction_random_type {kSphere,
                                  kSemisphere_up, kSemisphere_down,
                                  kPlaneUniformSource_down, kPlaneUniformSource_up, kRandomPhi};
    G4ThreeVector position_random_r, position_random_d;
    E_direction_random_type direction_random_type;
    G4double direction_random_theta;
    G4double direction_random_phi;
    G4double direction_random_max_angle;
    std::vector<Cuboid3D> line_trigger_volumes;
    G4bool position_random_use, direction_random_use, line_trigger_use, line_trigger_inverted;
  
  private:
    G4ParticleGun* particleGun;
    DetectorConstruction* myDetector;
    RunAction* run_action;
};

#endif


