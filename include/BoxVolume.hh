#ifndef BoxVolume_h
#define BoxVolume_h 1


#include <vector>
#include "globals.hh"
#include "G4ThreeVector.hh"
#include "SensitiveDetector.hh"
#include "types.hh"

class G4Box;
class G4LogicalVolume;
class G4VPhysicalVolume;
class G4Material;
class Parameterisation;


struct HasIs { G4bool is; HasIs(): is(false) {}; };
struct AttrStrip: HasIs
  { G4double spreading; G4int nstrips; };
struct AttrTime: HasIs
  { G4double gain, risetime, falltime; };
struct AttrEnergy: HasIs
  { };
struct AttrScint: HasIs
  { G4int nstrips; G4double s_offset; G4int t_nbins; G4int p_nbins;
    G4double t_min; G4double t_max; G4double speedoflight; };
struct AttrScints: HasIs
  { std::vector<G4int> nstrips; std::vector<G4double> s_offset; std::vector<G4int> t_nbins; std::vector<G4int> p_nbins;
    std::vector<G4double> t_min; std::vector<G4double> t_max; G4double speedoflight;
    AttrScint select(size_t n); };
struct AttrCalo3D: HasIs
  { G4int nx; std::vector<G4int> ny, nz; G4double wx; std::vector<G4double> wy, wz; std::vector<G4double> oy, oz; };
/*union*/ struct  Attr { AttrStrip strip; AttrTime time; AttrEnergy enegry; AttrScint scint; AttrScints scints; AttrCalo3D calo3d; G4bool is_BoxVolumes; };


class BoxVolume
{
  public:
    BoxVolume(const G4String& name, BoxVolume& mommy); // : sensitive(NULL)
    BoxVolume(); // : sensitive(NULL)
   ~BoxVolume();
    //BoxVolume(const BoxVolume& r): pos(r.pos),dims(r.dims),name(r.name),mother(r.mother),attr(r.attr),solid(r.solid),logic(r.logic),physic(r.physic),mater(r.mater),sensitive(r.sensitive) { };
    //BoxVolume& operator=(const BoxVolume& r){ pos=r.pos,dims=r.dims,name=r.name,mother=r.mother,attr=r.attr,solid=r.solid,logic=r.logic,physic=r.physic,mater=r.mater,sensitive=r.sensitive; return *this; };
    void CreateVolume();
    void CreateVolume(G4double& currentpos, const G4double& gap_after = microgap);
    inline void SetMother(BoxVolume& mommy) { mother = &mommy; };
    inline void SetMater(G4Material* stuff) { mater = stuff; };
    inline G4Material* operator=(G4Material* stuff) { return mater = stuff; };
    void SetPos(G4double& currentpos, const G4double& gap_after = microgap);
    void SetSensitiveDetector() { if(sensitive) if(logic) logic->SetSensitiveDetector(sensitive); };
    void Print(G4bool private_ = false) const;
    operator G4bool() { return dims.x() && dims.y() && dims.z(); };
    G4bool operator!() { return !operator G4bool(); };
    inline G4String GetName() const { return name; };
  
  public:
    G4ThreeVector pos, dims;
    G4String name;
    BoxVolume* mother;
    Attr attr;
  
  public:
    G4VSolid* solid;
    G4LogicalVolume* logic;
    G4VPhysicalVolume* physic;
    G4Material* mater;
    SensitiveDetector* sensitive;
  
  private:
    //template<class T> inline static void DELETE(T* ptr) { if(ptr) delete ptr; };
};


class BoxVolumes : public std::vector<BoxVolume>
{
  public:
    BoxVolumes() : current(0) { attr.is_BoxVolumes = true; };
   ~BoxVolumes() { };
    void Add(const BoxVolume&);
    BoxVolume DefaultChild(size_t n = 0); // copy 'mother', 'attr', 'mater'...
    void CreateVolume();
    inline void SetMother(BoxVolume& mommy) { mother = &mommy; };
    inline void SetMater(G4Material* stuff) { mater = stuff; };
    G4Material* operator=(G4Material* stuff);
    void Print() const;
    inline G4String GetName() const { return name; };
    //inline const BoxVolume& last() const { return at(current); };
    //inline       BoxVolume& last()       { return at(current); };
    //inline void reset() { current = 0; };
    //inline void pos_add(const G4ThreeVector& p) { at(++current).pos =  };
  
  public:
    G4String name;
    size_t nlayers;
    size_t current;
    BoxVolume* mother;
    Attr attr;
    G4Material* mater;
};

#endif
