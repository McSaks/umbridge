/*        //                                          \\ 
//       /  \                                        /  \ 
//      /o  o\        Various FOR Loop Macros       /o  o\ 
//     /  ..  \                                    /  ..  \ 
//    /__\__/__\                                  /__\__/__\ 
*/

#ifndef FOR_h
#define FOR_h

#include "globals.hh"

#define FOR(i, a, b)        for(G4int  i = G4int (a);  i <= G4int (b);  ++i)
#define dFOR(i, a, b)       for(      (i)=       (a); (i)<=       (b);  ++i)
#define uFOR(i, a, b)       for(size_t i = size_t(a);  i <= size_t(b);  ++i)
#define tFOR(type, i, a, b) for(type   i = type  (a);  i <= type  (b);  ++i)
//#define fFOR(type, i, a, b, d) for(type i = (a); i <= (b); i+=(d))
//#define fFOR(i, a, b, d) for(G4double i = (a); i <= (b); i+=(d))

#define FORs(i, a, b)        for(G4int  i = G4int (a);  i < G4int (b);  ++i)
#define dFORs(i, a, b)       for(      (i)=       (a); (i)<       (b);  ++i)
#define uFORs(i, a, b)       for(size_t i = size_t(a);  i < size_t(b);  ++i)
#define tFORs(type, i, a, b) for(type   i = type  (a);  i < type  (b);  ++i)

#define FORd(i, a, b)        for(G4int  i = G4int (a);  i >= G4int (b); --i)
#define dFORd(i, a, b)       for(      (i)=       (a); (i)>=       (b); --i)
#define uFORd(i, a, b)       for(size_t i = size_t(a);  i >= size_t(b); --i)
#define tFORd(type, i, a, b) for(type   i = type  (a);  i >= type  (b); --i)

#define vFOR(type, vec, i)   \
           tFORs(std::vector< type >::iterator, i, (vec).begin(), (vec).end())
#define vdFOR(vec, i)   \
           dFORs(i, (vec).begin(), (vec).end())
#define vFORr(type, vec, i, a, b)   \
           tFORs(std::vector< type >::iterator, i, ((a)>=0)?(vec).begin()+(a):(vec).end()+(a)-1, \
                                                 ((b)>=0)?(vec).begin()+(b)+1:(vec).end()+(b))
#define vdFORr(vec, i, a, b)   \
           dFORs(i, ((a)>=0)?(vec).begin()+(a):(vec).end()+(a)-1, \
                                                 ((b)>=0)?(vec).begin()+(b)+1:(vec).end()+(b))
#define vFORc(type, vec, i)   \
           tFORs(std::vector< type >::const_iterator, i, (vec).begin(), (vec).end())
#define vdFORc(vec, i)   \
           dFORs(i, (vec).begin(), (vec).end())
#define cFOR(container, type, vec, i)   \
           tFORs(container< type >::iterator, i, (vec).begin(), (vec).end())
#define cFORc(container, type, vec, i)   \
           tFORs(container< type >::const_iterator, i, (vec).begin(), (vec).end())

#define vFORi_BEGIN(type, vec, i, n)   \
           { size_t n = 0; for(std::vector< type >::iterator i = (vec).begin(); (i) < (vec).end(); ++i, ++n) {
#define vFORci_BEGIN(type, vec, i, n)   \
           { size_t n = 0; for(std::vector< type >::const_iterator i = (vec).begin(); (i) < (vec).end(); ++i, ++n) {
#define END_vFORi } }
#define END_vFORci } }

#define until(a) while(!(a))

#endif
