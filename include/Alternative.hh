#ifndef Alternative_h
#define Alternative_h

#include <vector>
#include <stdarg.h>

template<typename T>
class Alternative;
template<typename T>        bool operator==(const T&              left, const Alternative<T>& right);
template<typename T> inline bool operator==(const Alternative<T>& left, const T&              right) { return right == left; }
template<typename T>        bool operator==(const Alternative<T>& left, const Alternative<T>& right);
template<typename T> inline bool operator!=(const T&              left, const Alternative<T>& right) { return !(left == right); }
template<typename T> inline bool operator!=(const Alternative<T>& left, const T&              right) { return !(left == right); }
template<typename T> inline bool operator!=(const Alternative<T>& left, const Alternative<T>& right) { return !(left == right); }

template<typename T>
class Alternative : public std::vector<T>
{
  public:
        typedef T value_type;
        typedef std::vector<T> vector_type;
        typedef Alternative<T> alternative_type;
        Alternative(T val = T()) : std::vector<T>(1, val) { };
        Alternative(size_t, T*);
 inline void Add(T val) { push_back(val); }
        //friend bool operator==<T>(const T&              left, const Alternative<T>& right);
        //friend bool operator==<T>(const Alternative<T>& left, const T&              right);
        //friend bool operator==<T>(const Alternative<T>& left, const Alternative<T>& right);
        //friend bool operator!=<T>(const T&              left, const Alternative<T>& right);
        //friend bool operator!=<T>(const Alternative<T>& left, const T&              right);
        //friend bool operator!=<T>(const Alternative<T>& left, const Alternative<T>& right);
  
  protected:
  
};

#include "Alternative.icc"

#endif
