/*        //                                          \\ 
//       /  \           Position-sensitive           /  \ 
//      /o  o\                system                /o  o\ 
//     /  ..  \                                    /  ..  \ 
//    /__\__/__\                                  /__\__/__\ 
*/

#ifndef PositionSystem_h
#define PositionSystem_h 1

#include "globals.hh"
#include "G4ThreeVector.hh"
#include <vector>
#include "types.hh"

class DetectorConstruction;
class DetectorMessenger;
class EventAction;
class EventData;

class TrackPoint
{
  public:
       TrackPoint(G4ThreeVector r, G4double e = 0, G4double w = 0,
                  EStripProj p = kBothStripProj, G4int dP = 0, G4int dL = -1, G4bool b = true)
           : stripNo(-1), last(false), point(r), edep(e), weight(w), enabled(b), proj(p), SDtype(kOtherDetector), detPart(dP), detLayer(dL) { };
       TrackPoint()
           : stripNo(-1), last(false), point(G4ThreeVector(0,0,0))
           , edep(0), weight(0), enabled(false), proj(kNoStripProj)
           , SDtype(kOtherDetector), detPart(0), detLayer(-1) { };
/* operator= and copy constructor are created automatically:
       TrackPoint operator=(const TrackPoint& right)
           {stripNo = right.stripNo; last = right.last; point=right.point;
            edep = right.edep; weight=right.weight;
            enabled=right.enabled; proj=right.proj;
            SDname=right.SDname; SDtype=right.SDtype;
            detPart=right.detPart; detLayer=right.detLayer; return *this;};
       TrackPoint(const TrackPoint& right)
           : stripNo(right.stripNo), last(right.last), point(right.point)
           , edep(right.edep), weight(right.weight)
           , enabled(right.enabled), proj(right.proj)
           , SDname(right.SDname), SDtype(right.SDtype)
           , detPart(right.detPart), detLayer(right.detLayer) { }; */
       void On()     { enabled = true; };
       void Off()    { enabled = false; };
       void Switch() { enabled -= 1; };
       void Draw();
       inline G4bool First() { return !stripNo; };
       inline G4bool Last()  { return last; };
       
  public:
       G4int stripNo; G4bool last;
       G4ThreeVector point;
       G4double edep;
       G4double weight; // not normalized
       G4bool enabled;
       EStripProj proj;
       G4String SDname;
       EDetectorType SDtype;
       G4int detPart; // 1 <- C1, 2 <- C2
       G4int detLayer;
};

typedef std::vector<TrackPoint> TrackPoints;


class PositionSystem
{
  public:
       
       // constructors / destructors :
       PositionSystem();
       //PositionSystem(const TrackPoints& pts, const TrackLine& l = TrackLine())
       //    : trackPoints(pts), trackLine(l) { };
       //PositionSystem(const TrackLine& l)
       //    : trackPoints(), trackLine(l) { };
      ~PositionSystem();
       
       // set / get :
       TrackPoint& GetPoint(size_t i) { return trackPoints[i]; };
       TrackPoints& GetPoints() { return trackPoints; };
       void SetPoint(size_t i, TrackPoint pt) { trackPoints[i] = pt; };
       TrackPoint& operator[](size_t i) { return trackPoints[i]; };
       const TrackPoint& operator[](size_t i) const { return trackPoints[i]; };
       void SetDetectorConstruction(DetectorConstruction* det) { detector = det; };
       void SetMessenger(DetectorMessenger* mess); // two-way link
       void SetEventAction(EventAction* ev) { currentEvent = ev; };
       void SetEventData(EventData* ev) { data = ev; };
       
       // others :
       size_t NPoints() const { return trackPoints.size(); };
       size_t NOnPoints() const;
       void Append(TrackPoint);
       PositionSystem& operator+=(const TrackPoint& pt)
           { Append(pt); return *this; };
       void Join(const PositionSystem*);
       PositionSystem& operator+=(const PositionSystem& tr)
           { Join(&tr); return *this; };
       void Clear() { trackPoints.clear();};
       G4bool Trigger() const;
       G4bool Write() const;
   private:
//       G4bool Trigger1() const;
       
  protected:
       TrackPoints trackPoints;
       DetectorConstruction* detector;
       DetectorMessenger* messenger;
       EventAction* currentEvent;
       EventData* data;
};

#endif
