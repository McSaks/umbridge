#ifndef flow_control_macros_h
#define flow_control_macros_h 1

 

//-----------------------------------------------------//
//                                                    //
//Create syntactic sugar for control structures:
//
//  IF (test)               IF_ test THEN        SWITCH (test)
//    statements;             statements;          CASE a:
//    ...                     ...                    statements;
//  ELSE                    END_IF                 CASE b OR c OR d:  // ←— no fall-through
//    statements;                                    statements;
//    ...                                          FALL_THROUGH
//  END (if test -- just a comment)                CASE e OR f    // ←— fall-through here
//                                                   statements;
//                                                 DEFAULT:
//                                                 statements;
//                                               END_SWITCH
//
//  WHICH                      FOR (i, 0, n) DO  // see FOR.hh          IF_ test THEN RELAX
//    INCASE test THEN           statements[i];                         ELSE
//      statements;            END_FOR                                    statements;
//    INCASE test THEN                                                  END_IF
//      statements;            WHILE (test)        REPEAT
//    ELSE                       statements;         statements;          and so on
//      statements;            END_WHILE           REPEAT_UNTIL (test)
//  END_WHICH
//
#define BEGIN_SCOPE {
#define END_SCOPE }
#define EMPTY { }
#define RELAX { }
#define DO {
#define BEGIN {
#define BEGIN_FUNCTION {
#define BEGIN_CLASS {
#define END(block) }
#define END_IF }
  #define FI }
#define END_UNLESS }
  #define SSELNU }
#define END_SWITCH }
  #define HCTIWS }
#define END_WHILE }
  #define ELIHW }
#define END_FUNCTION }
  #define NOITCNUF }
#define END_FOR }
  #define ROF }
#define END_CLASS };
  #define SSALC };
#define IF(test) if(test) {
#define REPEAT do {
#define IF_ if(
#define THEN ) {
#define UNLESS(test) if(!(test)) {
#define ELSE } else {
#define ELSE_IF(test) } else if(test) {
#define WHILE(test) while(test) {
#define UNTIL(test) while(!(test)) {
#define REPEAT_WHILE(test) } while(test)
#define REPEAT_UNTIL(test) } while(!(test))
#define SWITCH(test) switch(test) {
#define SWITCH_ switch(
#define CASE break; case
#define FALL_THROUGH if(false)
//#define FALLTHROUGH_CASE case
#define OR : case
#define DEFAULT break; default

#define WHICH { if(false) {
#define INCASE } else if(
#define END_WHICH } }
  #define HCIHW } }

#define TRY try {
#define CATCH(ex) } catch(ex) {
#define END_TRY }
  #define YRT }

#define unless(test) if(!(test))
//#define until(test) while(!(test))

#endif
