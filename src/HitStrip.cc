#include "HitStrip.hh"
#include "G4UnitsTable.hh"
#include "G4VVisManager.hh"
#include "G4Circle.hh"
#include "G4Colour.hh"
#include "G4VisAttributes.hh"

G4Allocator<HitStrip> hitAllocatorStrip;


HitStrip::HitStrip()
  : trackID(-1), edep(0)
{}

HitStrip::~HitStrip() {}

HitStrip::HitStrip(const HitStrip& right)
  : G4VHit()
{
  trackID   = right.trackID;
  stripNum  = right.stripNum;
  edep      = right.edep;
  pos       = right.pos;
}

const HitStrip& HitStrip::operator=(const HitStrip& right)
{
  trackID   = right.trackID;
  stripNum  = right.stripNum;
  edep      = right.edep;
  pos       = right.pos;
  return *this;
}

G4int HitStrip::operator==(const HitStrip& right) const
{
  return (this==&right) ? 1 : 0;
}

void HitStrip::Draw()
{
  G4VVisManager* pVVisManager = G4VVisManager::GetConcreteInstance();
  if(pVVisManager)
  {
    G4Circle circle(pos);
    circle.SetScreenSize(2.);
    circle.SetFillStyle(G4Circle::filled);
    G4Color color(.3,.3,0.);
    G4VisAttributes attribs(color);
    circle.SetVisAttributes(attribs);
    pVVisManager->Draw(circle);
  }
}

void HitStrip::Print()
{
  G4cout << "  track ID:       " << trackID << '\n';
   if(stripNum.y-kUnsensitive)
  G4cout << "  stripY #:       " << stripNum.y << '\n';
   if(stripNum.z-kUnsensitive)
  G4cout << "  stripZ #:       " << stripNum.z << '\n';
  G4cout << "  energy deposit: " << G4BestUnit(edep,"Energy") << '\n';
  G4cout << "  position:       " << G4BestUnit(pos,"Length") << G4endl;
}
