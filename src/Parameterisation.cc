#include "Parameterisation.hh"

#include "G4VPhysicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4Box.hh"

#ifndef FOR
#define FOR(i,a,b) for(G4int i = a;  i <= b;  i++)
#endif

Parameterisation::Parameterisation(G4int nlayers, const std::vector<G4ThreeVector>& pos, const std::vector<G4ThreeVector>& dims)
:fnlayers(nlayers)
{
   fpos  = pos;
   fdims = dims;
}

Parameterisation::~Parameterisation()
{
}

void Parameterisation::ComputeTransformation
(const G4int copyNo, G4VPhysicalVolume* physVol) const
{
  physVol->SetTranslation(fpos[copyNo]);
  physVol->SetRotation(NULL);
}

void Parameterisation::ComputeDimensions(G4Box& solid, const G4int copyNo, const G4VPhysicalVolume*) const
{
  solid.SetXHalfLength(fdims[copyNo].x());
  solid.SetYHalfLength(fdims[copyNo].y());
  solid.SetZHalfLength(fdims[copyNo].z());
}
