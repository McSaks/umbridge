#include "Geometry.hh"
#include "DetectorConstruction.hh"
#include "FOR.hh"
#include "G4UnitsTable.hh"

#include "limits"
static const G4double NaN = std::numeric_limits<G4double>::signaling_NaN();

using namespace std;


template<typename T>
T front(const vector<T>& v)
{
  return v.size() ? v.front() : T();
}

Geometry::Geometry(DetectorConstruction* d, G4String ifile)
  : INIExtendedReader(ifile), det(d) { }

Geometry::~Geometry()
{ }

#include "G4Material.hh"
G4Material* Geometry::AlType(const G4int& type)
{
  switch(type) {
    case 16:
      return det->materAl16;
    case 48:
      return det->materAl48;
    case 108:
      return det->materAl108;
    default:
      return det->materVacuum;
  }
}

G4Material* Geometry::MatType(const G4String& type)
{
  WHICH
    INCASE type == "W" THEN
      return det->materW;
    INCASE type == "CsI" THEN
      return det->materCsI;
    INCASE type == "BGO" THEN
      return det->materBGO;
  END_WHICH
  return NULL;
}

vector<G4Material*> Geometry::AlType(const vector<G4int>& in)
{
  vector<G4Material*> out;
  vFORc(G4int, in, it)
    out.push_back(AlType(*it));
  return out;
}

vector<G4Material*> Geometry::MatType(vector<G4String>& in)
{
  vector<G4Material*> out;
  vFOR(G4String, in, it)
    out.push_back(MatType(*it));
  return out;
}


/* * * * * * * * * * * * * * * * * *\
|*  Read data from geometry file   *|
\* * * * * * * * * * * * * * * * * */

G4bool Geometry::Read()
BEGIN

  //ensure(infile);
  ValueUv tmpDuv;
  ValueIv tmpIv;
  ValueSv tmpSv;
  BoxVolume vol;
  G4double thickness, ywidth, zwidth, spreading;
  vector<G4double> thicknesses;
  G4int alType;
  vector<long> alTypes, numbers;
  vector<G4double> C_gaps, CC1_gaps_SC, CC1_gaps_CS;
  vector<G4double> topAC_gaps, sideAC_gaps, S1_gaps, S2_gaps, S3_gaps, S4_gaps;
  AttrScints attrscints;
  G4bool odd;
  register size_t ilayer;
  size_t nlayers;
  G4double pitchY, pitchZ;
  G4bool CC2_segmented;
  const G4double speed_of_light_in_vacuum = 2.99792458e+8 * m/s;


   G4bool global_supports = ReadB("world", "supports", true);

   ///////////////////////////////////
  //             world             //
 ///////////////////////////////////

   thickness = ReadU("world", "semiheight", 4*m);
   ywidth = ReadU("world", "y-width", 4*m);
   zwidth = ReadU("world", "z-width", 4*m);
   det->world.name = "World";
   det->world.pos.set(0, 0, 0);
   det->world.dims.set(thickness, ywidth/2, zwidth/2);
   det->world.SetMater(det->materVacuum);


   ///////////////////////////////////
  //             topAC             //
 ///////////////////////////////////

   G4bool topAC_supports = ReadB("topAC", "supports", true);

   // common:
   nlayers = ReadI("topAC", "n-layers", 1);
   det->topAC.assign(nlayers, BoxVolume());
   topAC_gaps = ReadUv("topAC", "gaps", nlayers, 0.);

   // scintillator:
   det->topAC.name = "topAC";
   thicknesses = ReadUv("topAC", "pSty-thickness", nlayers, 0.);
   attrscints.is = true;
   attrscints.nstrips      = ReadIv("topAC", "n-strips", nlayers, 1);
   attrscints.s_offset     = ReadUv("topAC", "strip-offset", nlayers, 0.);
   attrscints.p_nbins      = ReadIv("topAC", "p-bins", nlayers, 1);
   attrscints.t_nbins      = ReadIv("topAC", "t-bins", nlayers, 1);
   attrscints.t_min        = ReadUv("topAC", "t-min", nlayers, 0*ns);
   attrscints.t_max        = ReadUv("topAC", "t-max", nlayers, 1*s);
   attrscints.speedoflight = ReadU ("topAC", "speed-of-light", speed_of_light_in_vacuum);
   //vol->attr.time.gain        = ReadD("topAC", "gain", 1.);
   //vol->attr.time.risetime    = ReadU("topAC", "rise-time", 0.);
   //vol->attr.time.falltime    = ReadU("topAC", "fall-time", 0.);
   ywidth    = ReadU("topAC", "pSty-y-width", 0.);
   zwidth    = ReadU("topAC", "pSty-z-width", 0.);
   vFORi_BEGIN(BoxVolume, det->topAC, vol, layer)
     vol->name = det->topAC.name + "[" + TFormat::itoa(layer) + "]";
     vol->attr.scint.is = true;
     vol->attr.scint.nstrips      = attrscints.nstrips.at(layer);
     vol->attr.scint.s_offset     = attrscints.s_offset.at(layer);
     vol->attr.scint.p_nbins      = attrscints.p_nbins.at(layer);
     vol->attr.scint.t_nbins      = attrscints.t_nbins.at(layer);
     vol->attr.scint.t_min        = attrscints.t_min.at(layer);
     vol->attr.scint.t_max        = attrscints.t_max.at(layer);
     vol->attr.scint.speedoflight = attrscints.speedoflight;
     vol->dims.set(thicknesses[layer]/2, ywidth/2, zwidth/2);
     vol->SetMother(det->world);
     vol->SetMater(det->materPSty);
   END_vFORi

   // Al:
   det->topACSupportAl.name = "topACSupportAl";
   thickness = ReadU("topAC", "Al-thickness", 0.);
   ywidth    = ReadU("topAC", "Al-y-width", 0.);
   zwidth    = ReadU("topAC", "Al-z-width", 0.);
   alType    = ReadI("topAC", "Al-type", 0);
   det->topACSupportAl.dims.set(thickness/2, ywidth/2, zwidth/2);
   det->topACSupportAl.SetMother(det->world);
   det->topACSupportAl.SetMater(AlType(alType));

   // CFRP:
   det->topACSupportTopCFRP.name = "topACSupportTopCFRP";
   thickness = ReadU("topAC", "CFRP-thickness", 0.);
   ywidth    = ReadU("topAC", "CFRP-y-width", 0.);
   zwidth    = ReadU("topAC", "CFRP-z-width", 0.);
   det->topACSupportTopCFRP.dims.set(thickness/2, ywidth/2, zwidth/2);
   det->topACSupportTopCFRP.SetMother(det->world);
   det->topACSupportTopCFRP.SetMater(det->materCFRP);
   det->topACSupportBotCFRP = det->topACSupportTopCFRP;
   det->topACSupportBotCFRP.name = "topACSupportBotCFRP";

   // gap:
   G4double gap_topAC_C = ReadU("gaps", "AC--C", 0.);


   ///////////////////////////////////
  //               C               //
 ///////////////////////////////////

   G4bool C_supports = ReadB("C", "supports", true);

   // common:
   nlayers   = ReadI("C", "n-layers", 1);
   pitchY    = ReadU("C", "y-pitch", cm);
   pitchZ    = ReadU("C", "z-pitch", cm);
   spreading = ReadU("C", "spreading", -1.);
   C_gaps    = ReadUv("C", "gaps", nlayers, 0.);

   // Si (Y and Z):
   thicknesses = ReadUv("C", "Si-thickness", 2*nlayers, 0.);
   ywidth      = ReadU("C", "Si-y-width", 0.);
   zwidth      = ReadU("C", "Si-z-width", 0.);
   det->CLayerSiY.nlayers = nlayers;
   det->CLayerSiY.name = "CLayerSiY";
   det->CLayerSiY.mater = det->materSi;
   det->CLayerSiY.SetMother(det->world);
   det->CLayerSiY.attr.strip.spreading = spreading;
   det->CLayerSiY.attr.strip.is = true;
   det->CLayerSiY.attr.strip.nstrips = G4int(ywidth/pitchY);
   det->CLayerSiZ = det->CLayerSiY;
   det->CLayerSiZ.name = "CLayerSiZ";
   det->CLayerSiZ.attr.strip.is = true;
   det->CLayerSiZ.attr.strip.nstrips = G4int(zwidth/pitchZ);
   dFORs(ilayer, 0, det->CLayerSiY.nlayers) {
     vol = det->CLayerSiY.DefaultChild();
     vol = det->CLayerSiY.DefaultChild();
     vol.name = det->CLayerSiY.name + "[" + TFormat::itoa(ilayer) + "]";
     vol.dims.set(thicknesses[2*ilayer]/2, ywidth/2, zwidth/2);
     det->CLayerSiY.Add(vol);
     vol = det->CLayerSiZ.DefaultChild();
     vol.name = det->CLayerSiZ.name + "[" + TFormat::itoa(ilayer) + "]";
     vol.dims.set(thicknesses[2*ilayer+1]/2, ywidth/2, zwidth/2);
     det->CLayerSiZ.Add(vol);
   }

   // W:
   G4double XoW = det->materW->GetRadlen();
   thicknesses = ReadUv("C", "W-thickness", nlayers, 0., XoW);
   ywidth      = ReadU("C", "W-y-width", 0., XoW);
   zwidth      = ReadU("C", "W-z-width", 0., XoW);
   det->CLayerW.nlayers = nlayers;
   det->CLayerW.name = "CLayerW";
   det->CLayerW.mater = det->materW;
   det->CLayerW.SetMother(det->world);
   dFORs(ilayer, 0, det->CLayerW.nlayers) {
     vol = det->CLayerW.DefaultChild();
     vol.name = det->CLayerW.name + "[" + TFormat::itoa(ilayer) + "]";
     vol.dims.set(thicknesses[ilayer]/2, ywidth/2, zwidth/2);
     det->CLayerW.Add(vol);
   }

   // Al:
   thicknesses = ReadUv("C", "Al-thickness", nlayers + 1, 0.);
   alTypes     = ReadIv("C", "Al-type", nlayers + 1, 0);
   ywidth      = ReadU("C", "Al-y-width", 0.);
   zwidth      = ReadU("C", "Al-z-width", 0.);
   det->CSupportAl.nlayers = nlayers + 1;
   det->CSupportAl.name = "CSupportAl";
   det->CSupportAl.SetMother(det->world);
   dFORs(ilayer, 0, det->CSupportAl.nlayers) {
     vol = det->CSupportAl.DefaultChild();
     vol.name = det->CSupportAl.name + "[" + TFormat::itoa(ilayer) + "]";
     vol.dims.set(thicknesses[ilayer]/2, ywidth/2, zwidth/2);
     vol.SetMater(AlType(alTypes[ilayer]));
     det->CSupportAl.Add(vol);
   }

   // CFRP (Top and Bot):
   thicknesses = ReadUv("C", "CFRP-thickness", nlayers + 1, 0.);
   ywidth      = ReadU ("C", "CFRP-y-width", 0.);
   zwidth      = ReadU ("C", "CFRP-z-width", 0.);
   det->CSupportTopCFRP.nlayers = nlayers + 1;
   det->CSupportTopCFRP.name = "CSupportTopCFRP";
   det->CSupportTopCFRP.mater = det->materCFRP;
   det->CSupportTopCFRP.SetMother(det->world);
   det->CSupportBotCFRP = det->CSupportTopCFRP;
   det->CSupportBotCFRP.name = "CSupportBotCFRP";
   dFORs(ilayer, 0, det->CSupportTopCFRP.nlayers) {
     vol = det->CSupportTopCFRP.DefaultChild();
     vol.name = det->CSupportTopCFRP.name + "[" + TFormat::itoa(ilayer) + "]";
     vol.dims.set(thicknesses[ilayer]/2, ywidth/2, zwidth/2);
     det->CSupportTopCFRP.Add(vol);
     vol = det->CSupportBotCFRP.DefaultChild();
     vol.name = det->CSupportBotCFRP.name + "[" + TFormat::itoa(ilayer) + "]";
     vol.dims.set(thicknesses[ilayer]/2, ywidth/2, zwidth/2);
     det->CSupportBotCFRP.Add(vol);
   }

   // gap:
   G4double gap_C_S1 = ReadU("gaps", "C--S1", 0.);


   ///////////////////////////////////
  //              S1               //
 ///////////////////////////////////

   G4bool S1_supports = ReadB("S1", "supports", true);

   // common:
   nlayers = ReadI("S1", "n-layers", 1);
   det->S1.assign(nlayers, BoxVolume());
   S1_gaps = ReadUv("topAC", "gaps", nlayers, 0.);

   // scintillator:
   det->S1.name = "S1";
   thicknesses = ReadUv("S1", "pSty-thickness", nlayers, 0.);
   attrscints.is = true;
   attrscints.nstrips      = ReadIv("S1", "n-strips", nlayers, 1);
   attrscints.s_offset     = ReadUv("S1", "strip-offset", nlayers, 0.);
   attrscints.p_nbins      = ReadIv("S1", "p-bins", nlayers, 1);
   attrscints.t_nbins      = ReadIv("S1", "t-bins", nlayers, 1);
   attrscints.t_min        = ReadUv("S1", "t-min", nlayers, 0*ns);
   attrscints.t_max        = ReadUv("S1", "t-max", nlayers, 1*s);
   attrscints.speedoflight = ReadU ("S1", "speed-of-light", speed_of_light_in_vacuum);
   ywidth    = ReadU("S1", "pSty-y-width", 0.);
   zwidth    = ReadU("S1", "pSty-z-width", 0.);
   vFORi_BEGIN(BoxVolume, det->S1, vol, layer)
     vol->name = det->S1.name + "[" + TFormat::itoa(layer) + "]";
     vol->attr.scint.is = true;
     vol->attr.scint.nstrips      = attrscints.nstrips.at(layer);
     vol->attr.scint.s_offset     = attrscints.s_offset.at(layer);
     vol->attr.scint.p_nbins      = attrscints.p_nbins.at(layer);
     vol->attr.scint.t_nbins      = attrscints.t_nbins.at(layer);
     vol->attr.scint.t_min        = attrscints.t_min.at(layer);
     vol->attr.scint.t_max        = attrscints.t_max.at(layer);
     vol->attr.scint.speedoflight = attrscints.speedoflight;
     vol->dims.set(thicknesses[layer]/2, ywidth/2, zwidth/2);
     vol->SetMother(det->world);
     vol->SetMater(det->materPSty);
   END_vFORi

   // Al:
   det->S1SupportAl.name = "S1SupportAl";
   thickness = ReadU("S1", "Al-thickness", 0.);
   ywidth    = ReadU("S1", "Al-y-width", 0.);
   zwidth    = ReadU("S1", "Al-z-width", 0.);
   alType    = ReadI("S1", "Al-type", 0);
   det->S1SupportAl.dims.set(thickness/2, ywidth/2, zwidth/2);
   det->S1SupportAl.SetMother(det->world);
   det->S1SupportAl.SetMater(AlType(alType));

   // CFRP:
   det->S1SupportTopCFRP.name = "S1SupportTopCFRP";
   thickness = ReadU("S1", "CFRP-thickness", 0.);
   ywidth    = ReadU("S1", "CFRP-y-width", 0.);
   zwidth    = ReadU("S1", "CFRP-z-width", 0.);
   det->S1SupportTopCFRP.dims.set(thickness/2, ywidth/2, zwidth/2);
   det->S1SupportTopCFRP.SetMother(det->world);
   det->S1SupportTopCFRP.SetMater(det->materCFRP);
   det->S1SupportBotCFRP = det->S1SupportTopCFRP;
   det->S1SupportBotCFRP.name = "S1SupportBotCFRP";

   G4double dist_S1_S2 = ReadU("gaps", "S1--S2");

   ///////////////////////////////////
  //              S2               //
 ///////////////////////////////////

   G4bool S2_supports = ReadB("S2", "supports", true);

   // common:
   nlayers = ReadI("S2", "n-layers", 1);
   det->S2.assign(nlayers, BoxVolume());
   S2_gaps = ReadUv("topAC", "gaps", nlayers, 0.);

   // scintillator:
   det->S2.name = "S2";
   thicknesses = ReadUv("S2", "pSty-thickness", nlayers, 0.);
   attrscints.is = true;
   attrscints.nstrips      = ReadIv("S2", "n-strips", nlayers, 1);
   attrscints.s_offset     = ReadUv("S2", "strip-offset", nlayers, 0.);
   attrscints.p_nbins      = ReadIv("S2", "p-bins", nlayers, 1);
   attrscints.t_nbins      = ReadIv("S2", "t-bins", nlayers, 1);
   attrscints.t_min        = ReadUv("S2", "t-min", nlayers, 0*ns);
   attrscints.t_max        = ReadUv("S2", "t-max", nlayers, 1*s);
   attrscints.speedoflight = ReadU ("S2", "speed-of-light", speed_of_light_in_vacuum);
   ywidth    = ReadU("S2", "pSty-y-width", 0.);
   zwidth    = ReadU("S2", "pSty-z-width", 0.);
   vFORi_BEGIN(BoxVolume, det->S2, vol, layer)
     vol->name = det->S2.name + "[" + TFormat::itoa(layer) + "]";
     vol->attr.scint.is = true;
     vol->attr.scint.nstrips      = attrscints.nstrips.at(layer);
     vol->attr.scint.s_offset     = attrscints.s_offset.at(layer);
     vol->attr.scint.p_nbins      = attrscints.p_nbins.at(layer);
     vol->attr.scint.t_nbins      = attrscints.t_nbins.at(layer);
     vol->attr.scint.t_min        = attrscints.t_min.at(layer);
     vol->attr.scint.t_max        = attrscints.t_max.at(layer);
     vol->attr.scint.speedoflight = attrscints.speedoflight;
     vol->dims.set(thicknesses[layer]/2, ywidth/2, zwidth/2);
     vol->SetMother(det->world);
     vol->SetMater(det->materPSty);
   END_vFORi

   // Al:
   det->S2SupportAl.name = "S2SupportAl";
   thickness = ReadU("S2", "Al-thickness", 0.);
   ywidth    = ReadU("S2", "Al-y-width", 0.);
   zwidth    = ReadU("S2", "Al-z-width", 0.);
   alType    = ReadI("S2", "Al-type", 0);
   det->S2SupportAl.dims.set(thickness/2, ywidth/2, zwidth/2);
   det->S2SupportAl.SetMother(det->world);
   det->S2SupportAl.SetMater(AlType(alType));

   // CFRP:
   det->S2SupportTopCFRP.name = "S2SupportTopCFRP";
   thickness = ReadU("S2", "CFRP-thickness", 0.);
   ywidth    = ReadU("S2", "CFRP-y-width", 0.);
   zwidth    = ReadU("S2", "CFRP-z-width", 0.);
   det->S2SupportTopCFRP.dims.set(thickness/2, ywidth/2, zwidth/2);
   det->S2SupportTopCFRP.SetMother(det->world);
   det->S2SupportTopCFRP.SetMater(det->materCFRP);
   det->S2SupportBotCFRP = det->S2SupportTopCFRP;
   det->S2SupportBotCFRP.name = "S2SupportBotCFRP";

   G4double gap_S2_CC1 = ReadU("gaps", "S2--CC1");


   ///////////////////////////////////
  //              CC1              //
 ///////////////////////////////////

   G4bool CC1_supports = ReadB("CC1", "supports", true);

   // common:
   nlayers     = ReadI("CC1", "n-layers", 1);
   pitchY      = ReadU("CC1", "y-pitch", cm);
   pitchZ      = ReadU("CC1", "z-pitch", cm);
   spreading   = ReadU("CC1", "spreading", -1.);
   CC1_gaps_CS = ReadUv("CC1", "gaps(CsI--Si)", nlayers, front(ReadUv("CC1", "gaps", nlayers, 0.)));
   CC1_gaps_SC = ReadUv("CC1", "gaps(Si--CsI)", nlayers-1, front(CC1_gaps_CS));

   // Si (Y and Z):
   thicknesses = ReadUv("CC1", "Si-thickness", nlayers, 0.);
   ywidth      = ReadU("CC1", "Si-y-width", 0.);
   zwidth      = ReadU("CC1", "Si-z-width", 0.);
   det->CC1LayerSiY.nlayers = nlayers;
   det->CC1LayerSiY.name = "CC1LayerSiY";
   det->CC1LayerSiY.mater = det->materSi;
   det->CC1LayerSiY.SetMother(det->world);
   det->CC1LayerSiY.attr.strip.is = true;
   det->CC1LayerSiY.attr.strip.spreading = spreading;
   det->CC1LayerSiY.attr.strip.nstrips = G4int(ywidth/pitchY);
   det->CC1LayerSiZ = det->CC1LayerSiY;
   det->CC1LayerSiZ.name = "CC1LayerSiZ";
   det->CC1LayerSiZ.attr.strip.is = true;
   det->CC1LayerSiZ.attr.strip.nstrips = G4int(zwidth/pitchZ);
   dFORs(ilayer, 0, det->CC1LayerSiY.nlayers) {
     vol = det->CC1LayerSiY.DefaultChild();
     vol.name = det->CC1LayerSiY.name + "[" + TFormat::itoa(ilayer) + "]";
     vol.dims.set(thicknesses[ilayer]/2, ywidth/2, zwidth/2);
     det->CC1LayerSiY.Add(vol);
     vol = det->CC1LayerSiZ.DefaultChild();
     vol.name = det->CC1LayerSiZ.name + "[" + TFormat::itoa(ilayer) + "]";
     vol.dims.set(thicknesses[ilayer]/2, ywidth/2, zwidth/2);
     det->CC1LayerSiZ.Add(vol);
   }

   // CsI(Tl):
   G4double XoCsI = det->materCsI->GetRadlen();
   thicknesses = ReadUv("CC1", "CsI-thickness", nlayers, 0., XoCsI);
   ywidth      = ReadU("CC1", "CsI-y-width", 0., XoCsI);
   zwidth      = ReadU("CC1", "CsI-z-width", 0., XoCsI);
   det->CC1LayerCsI.nlayers = nlayers;
   det->CC1LayerCsI.name = "CC1LayerCsI";
   det->CC1LayerCsI.mater = det->materCsI;
   det->CC1LayerCsI.SetMother(det->world);
   dFORs(ilayer, 0, det->CC1LayerCsI.nlayers) {
     vol = det->CC1LayerCsI.DefaultChild();
     vol.name = det->CC1LayerCsI.name + "[" + TFormat::itoa(ilayer) + "]";
     vol.dims.set(thicknesses[ilayer]/2, ywidth/2, zwidth/2);
     det->CC1LayerCsI.Add(vol);
   }

   //det->CC1LayerSiY.Print();
   //det->CC1LayerSiZ.Print();
   //det->CC1LayerCsI.Print();
   //Disp(det->CC1LayerCsI[0].dims.x()/cm, "DEBUG      thickness = ");
   //G4cerr << "DEBUG      thickness = " << (det->CC1LayerCsI[0].dims.x()/cm) << "" << G4endl;

   // Al:
   thicknesses = ReadUv("CC1", "Al(CsI)-thickness", nlayers, front(ReadUv("CC1", "Al-thickness", nlayers+1, 0.)));
   alTypes     = ReadIv("CC1", "Al(CsI)-type", nlayers,      front(ReadIv("CC1", "Al-type", nlayers+1, 0)));
   ywidth      = ReadU("CC1", "Al(CsI)-y-width",             ReadU("CC1", "Al-y-width", 0.));
   zwidth      = ReadU("CC1", "Al(CsI)-z-width",             ReadU("CC1", "Al-z-width", 0.));
   det->CC1SupportAl_CsI.nlayers = nlayers;
   det->CC1SupportAl_CsI.name = "CC1SupportAl(CsI)";
   det->CC1SupportAl_CsI.SetMother(det->world);
   dFORs(ilayer, 0, det->CC1SupportAl_CsI.nlayers) {
     vol = det->CC1SupportAl_CsI.DefaultChild();
     vol.name = det->CC1SupportAl_CsI.name + "[" + TFormat::itoa(ilayer) + "]";
     vol.dims.set(thicknesses[ilayer]/2, ywidth/2, zwidth/2);
     vol.SetMater(AlType(alTypes[ilayer]));
     det->CC1SupportAl_CsI.Add(vol);
   }
   thicknesses = ReadUv("CC1", "Al(Si)-thickness", nlayers, front(ReadUv("CC1", "Al-thickness", nlayers+1, 0.)));
   alTypes     = ReadIv("CC1", "Al(Si)-type", nlayers,      front(ReadIv("CC1", "Al-type", nlayers+1, 0)));
   ywidth      = ReadU("CC1", "Al(Si)-y-width",             ReadU("CC1", "Al-y-width", 0.));
   zwidth      = ReadU("CC1", "Al(Si)-z-width",             ReadU("CC1", "Al-z-width", 0.));
   det->CC1SupportAl_Si.nlayers = nlayers;
   det->CC1SupportAl_Si.name = "CC1SupportAl(Si)";
   det->CC1SupportAl_Si.SetMother(det->world);
   dFORs(ilayer, 0, det->CC1SupportAl_Si.nlayers) {
     vol = det->CC1SupportAl_Si.DefaultChild();
     vol.name = det->CC1SupportAl_Si.name + "[" + TFormat::itoa(ilayer) + "]";
     vol.dims.set(thicknesses[ilayer]/2, ywidth/2, zwidth/2);
     vol.SetMater(AlType(alTypes[ilayer]));
     det->CC1SupportAl_Si.Add(vol);
   }

   // CFRP (Top and Bot):
   thicknesses = ReadUv("CC1", "CFRP(CsI)-thickness", nlayers, front(ReadUv("CC1", "CFRP-thickness", nlayers+1, 0.)));
   ywidth      = ReadU("CC1", "CFRP(CsI)-y-width",             ReadU("CC1", "CFRP-y-width", 0.));
   zwidth      = ReadU("CC1", "CFRP(CsI)-z-width",             ReadU("CC1", "CFRP-z-width", 0.));
   det->CC1SupportTopCFRP_CsI.nlayers = nlayers;
   det->CC1SupportTopCFRP_CsI.name = "CC1SupportTopCFRP(CsI)";
   det->CC1SupportTopCFRP_CsI.mater = det->materCFRP;
   det->CC1SupportTopCFRP_CsI.SetMother(det->world);
   det->CC1SupportBotCFRP_CsI = det->CC1SupportTopCFRP_CsI;
   det->CC1SupportBotCFRP_CsI.name = "CC1SupportBotCFRP(CsI)";
   dFORs(ilayer, 0, det->CC1SupportTopCFRP_CsI.nlayers) {
     vol = det->CC1SupportTopCFRP_CsI.DefaultChild();
     vol.name = det->CC1SupportTopCFRP_CsI.name + "[" + TFormat::itoa(ilayer) + "]";
     vol.dims.set(thicknesses[ilayer]/2, ywidth/2, zwidth/2);
     det->CC1SupportTopCFRP_CsI.Add(vol);
     vol = det->CC1SupportBotCFRP_CsI.DefaultChild();
     vol.name = det->CC1SupportBotCFRP_CsI.name + "[" + TFormat::itoa(ilayer) + "]";
     vol.dims.set(thicknesses[ilayer]/2, ywidth/2, zwidth/2);
     det->CC1SupportBotCFRP_CsI.Add(vol);
   }
   thicknesses = ReadUv("CC1", "CFRP(Si)-thickness", nlayers, front(ReadUv("CC1", "CFRP-thickness", nlayers+1, 0.)));
   ywidth      = ReadU("CC1", "CFRP(Si)-y-width",             ReadU("CC1", "CFRP-y-width", 0.));
   zwidth      = ReadU("CC1", "CFRP(Si)-z-width",             ReadU("CC1", "CFRP-z-width", 0.));
   det->CC1SupportTopCFRP_Si.nlayers = nlayers;
   det->CC1SupportTopCFRP_Si.name = "CC1SupportTopCFRP(Si)";
   det->CC1SupportTopCFRP_Si.mater = det->materCFRP;
   det->CC1SupportTopCFRP_Si.SetMother(det->world);
   det->CC1SupportBotCFRP_Si = det->CC1SupportTopCFRP_Si;
   det->CC1SupportBotCFRP_Si.name = "CC1SupportBotCFRP(Si)";
   dFORs(ilayer, 0, det->CC1SupportTopCFRP_Si.nlayers) {
     vol = det->CC1SupportTopCFRP_Si.DefaultChild();
     vol.name = det->CC1SupportTopCFRP_Si.name + "[" + TFormat::itoa(ilayer) + "]";
     vol.dims.set(thicknesses[ilayer]/2, ywidth/2, zwidth/2);
     det->CC1SupportTopCFRP_Si.Add(vol);
     vol = det->CC1SupportBotCFRP_Si.DefaultChild();
     vol.name = det->CC1SupportBotCFRP_Si.name + "[" + TFormat::itoa(ilayer) + "]";
     vol.dims.set(thicknesses[ilayer]/2, ywidth/2, zwidth/2);
     det->CC1SupportBotCFRP_Si.Add(vol);
   }

   G4double dist_S2_S3 = ReadU("gaps", "S2--S3");


   ///////////////////////////////////
  //              S3               //
 ///////////////////////////////////

   G4bool S3_supports = ReadB("S3", "supports", true);

   // common:
   nlayers = ReadI("S3", "n-layers", 1);
   det->S3.assign(nlayers, BoxVolume());
   S3_gaps = ReadUv("topAC", "gaps", nlayers, 0.);

   // scintillator:
   det->S3.name = "S3";
   thicknesses = ReadUv("S3", "pSty-thickness", nlayers, 0.);
   attrscints.is = true;
   attrscints.nstrips      = ReadIv("S3", "n-strips", nlayers, 1);
   attrscints.s_offset     = ReadUv("S3", "strip-offset", nlayers, 0.);
   attrscints.p_nbins      = ReadIv("S3", "p-bins", nlayers, 1);
   attrscints.t_nbins      = ReadIv("S3", "t-bins", nlayers, 1);
   attrscints.t_min        = ReadUv("S3", "t-min", nlayers, 0*ns);
   attrscints.t_max        = ReadUv("S3", "t-max", nlayers, 1*s);
   attrscints.speedoflight = ReadU ("S3", "speed-of-light", speed_of_light_in_vacuum);
   ywidth    = ReadU("S3", "pSty-y-width", 0.);
   zwidth    = ReadU("S3", "pSty-z-width", 0.);
   vFORi_BEGIN(BoxVolume, det->S3, vol, layer)
     vol->name = det->S3.name + "[" + TFormat::itoa(layer) + "]";
     vol->attr.scint.is = true;
     vol->attr.scint.nstrips      = attrscints.nstrips.at(layer);
     vol->attr.scint.s_offset     = attrscints.s_offset.at(layer);
     vol->attr.scint.p_nbins      = attrscints.p_nbins.at(layer);
     vol->attr.scint.t_nbins      = attrscints.t_nbins.at(layer);
     vol->attr.scint.t_min        = attrscints.t_min.at(layer);
     vol->attr.scint.t_max        = attrscints.t_max.at(layer);
     vol->attr.scint.speedoflight = attrscints.speedoflight;
     vol->dims.set(thicknesses[layer]/2, ywidth/2, zwidth/2);
     vol->SetMother(det->world);
     vol->SetMater(det->materPSty);
   END_vFORi

   // Al:
   det->S3SupportAl.name = "S3SupportAl";
   thickness = ReadU("S3", "Al-thickness", 0.);
   ywidth    = ReadU("S3", "Al-y-width", 0.);
   zwidth    = ReadU("S3", "Al-z-width", 0.);
   alType    = ReadI("S3", "Al-type", 0);
   det->S3SupportAl.dims.set(thickness/2, ywidth/2, zwidth/2);
   det->S3SupportAl.SetMother(det->world);
   det->S3SupportAl.SetMater(AlType(alType));

   // CFRP:
   det->S3SupportTopCFRP.name = "S3SupportTopCFRP";
   thickness = ReadU("S3", "CFRP-thickness", 0.);
   ywidth    = ReadU("S3", "CFRP-y-width", 0.);
   zwidth    = ReadU("S3", "CFRP-z-width", 0.);
   det->S3SupportTopCFRP.dims.set(thickness/2, ywidth/2, zwidth/2);
   det->S3SupportTopCFRP.SetMother(det->world);
   det->S3SupportTopCFRP.SetMater(det->materCFRP);
   det->S3SupportBotCFRP = det->S3SupportTopCFRP;
   det->S3SupportBotCFRP.name = "S3SupportBotCFRP";

   G4double gap_S3_CC2 = ReadU("gaps", "S3--CC2");


   ///////////////////////////////////
  //              CC2              //
 ///////////////////////////////////

   G4bool CC2_supports = ReadB("CC2", "supports");

   BEGIN_SCOPE

    det->CC2_is_segmented = CC2_segmented = ReadB("CC2", "segmented", true);
    nlayers = ReadI("CC2", "n-layers", 1);
    G4double CFRP_thickness = ReadU("CC2", "CFRP-between-thickness", 0.);
    G4double film_thickness = ReadU("CC2", "mirrorfilm-thickness", 0.);
    vector<G4double> item_height = ReadUv("CC2", "item-heights", nlayers, 0., det->materCsI->GetRadlen());
    vector<G4int> nitems_y = ReadIv("CC2", "n-items[Y]", nlayers, 1);
    vector<G4double> scint_width_y = ReadUv("CC2", "scint-width", nlayers, 0.);
    vector<G4double> CFRP_width_y = ReadUv("CC2", "CFRP-between-items-thickness[Y]", nlayers, 0.);
    vector<G4int> nitems_z = ReadIv("CC2", "n-items[Z]", nlayers, 1);
    vector<G4double>& scint_width_z = scint_width_y; // currently, y = z
    vector<G4double> CFRP_width_z = ReadUv("CC2", "CFRP-between-items-thickness[Z]", nlayers, 0.);

    G4double full_height; // physycal
    G4double wx = 0; // virtual
    vector<G4double> ywidths(nlayers), zwidths(nlayers); // physical
    vector<G4double> wy(nlayers), wz(nlayers); // virtual
    vector<G4double> dx(nlayers), dy(nlayers), dz(nlayers); // pitch

    dFORs(ilayer, 0, nlayers) {
      wy.at(ilayer) = nitems_y.at(ilayer) *  (( dy.at(ilayer) = scint_width_y.at(ilayer) + 2*film_thickness + CFRP_width_y.at(ilayer) + 4*microgap )) ;
      ywidths.at(ilayer) = wy.at(ilayer) + CFRP_width_y.at(ilayer);
      wz.at(ilayer) = nitems_z.at(ilayer) *  (( dz.at(ilayer) = scint_width_z.at(ilayer) + 2*film_thickness + CFRP_width_z.at(ilayer) + 4*microgap ));
      zwidths.at(ilayer) = wz.at(ilayer) + CFRP_width_z.at(ilayer);
      wx += dx.at(ilayer) = CFRP_thickness + 2*film_thickness + item_height.at(ilayer) + 4*microgap;
    }
    full_height = wx + CFRP_thickness;


      // scintillator:
      det->CC2_single.attr.calo3d.is = true;
      det->CC2_single.attr.calo3d.nx = nlayers;
      det->CC2_single.attr.calo3d.wx = wx;
      det->CC2_single.attr.calo3d.ny = nitems_y;//.assign(nlayers,0);
      det->CC2_single.attr.calo3d.wy = wy;//.assign(nlayers,0.);
      det->CC2_single.attr.calo3d.nz = nitems_z;//.assign(nlayers,0);
      det->CC2_single.attr.calo3d.wz = wz;//.assign(nlayers,0.);
    UNLESS( CC2_segmented ) // not segmented
      //dFORs(ilayer, 0, nlayers) {
      //  det->CC2.attr.calo3d.wy.at(ilayer) = (det->CC2.attr.calo3d.ny.at(ilayer) = nitems_y.at(ilayer)) * ywidths.at(ilayer);
      //  det->CC2.attr.calo3d.wz.at(ilayer) = (det->CC2.attr.calo3d.nz.at(ilayer) = nitems_z.at(ilayer)) * ywidths.at(ilayer);
      //}
      ywidth = ywidths.front(), zwidth = zwidths.front();
      det->CC2_single.dims.setX(full_height/2);
      det->CC2_single.dims.setY(ywidth/2);
      det->CC2_single.dims.setZ(zwidth/2);
      det->CC2_single.SetMother(det->world);
      det->CC2_single.SetMater(det->materCsI);

      // CFRP:
      det->CC2SupportTopCFRP.name = "CC2SupportTopCFRP";
      det->CC2SupportTopCFRP.dims.set(CFRP_thickness/2, ywidth/2, zwidth/2);
      det->CC2SupportTopCFRP.SetMother(det->world);
      det->CC2SupportTopCFRP.SetMater(det->materCFRP);

      det->CC2SupportBotCFRP.name = "CC2SupportBotCFRP";
      det->CC2SupportBotCFRP.dims.set(CFRP_thickness/2, ywidth/2, zwidth/2);
      det->CC2SupportBotCFRP.SetMother(det->world);
      det->CC2SupportBotCFRP.SetMater(det->materCFRP);

    ELSE // segmented

      #include "subfiles/Geometry.CC2.icc"

    END_IF

   END_SCOPE

   G4double gap_CC2_Plate = ReadU("gaps", "CC2--plate", 0.);


   ///////////////////////////////////
  //           AlPlate             //
 ///////////////////////////////////

   det->CC2SupportTopAl_o.name = "CC2SupportTopAl_o";
   thickness = ReadU("plate", "upper-outer-thickness", 0.);
   ywidth    = ReadU("plate", "y-width", 0.);
   zwidth    = ReadU("plate", "z-width", 0.);
   det->CC2SupportTopAl_o.SetMother(det->world);
   det->CC2SupportTopAl_o.SetMater(det->materAl);
   IF_ thickness < 0 THEN
     thickness = -thickness;
     det->CC2SupportTopAl_o.SetMater(det->materVacuum);
   END_IF
   det->CC2SupportTopAl_o.dims.set(thickness/2, ywidth/2, zwidth/2);

   det->CC2SupportTopAl_i.name = "CC2SupportTopAl_i";
   thickness = ReadU("plate", "upper-inner-thickness", 0.);
   det->CC2SupportTopAl_i.SetMother(det->world);
   det->CC2SupportTopAl_i.SetMater(det->materAl108);
   IF_ thickness < 0 THEN
     thickness = -thickness;
     det->CC2SupportTopAl_i.SetMater(det->materVacuum);
   END_IF
   det->CC2SupportTopAl_i.dims.set(thickness/2, ywidth/2, zwidth/2);

   det->CC2SupportBotAl_i.name = "CC2SupportBotAl_i";
   thickness = ReadU("plate", "lower-inner-thickness", 0.);
   det->CC2SupportBotAl_i.SetMother(det->world);
   det->CC2SupportBotAl_i.SetMater(det->materAl108);
   IF_ thickness < 0 THEN
     thickness = -thickness;
     det->CC2SupportBotAl_i.SetMater(det->materVacuum);
   END_IF
   det->CC2SupportBotAl_i.dims.set(thickness/2, ywidth/2, zwidth/2);

   det->CC2SupportBotAl_o.name = "CC2SupportBotAl_o";
   thickness = ReadU("plate", "lower-outer-thickness", 0.);
   det->CC2SupportBotAl_o.SetMother(det->world);
   det->CC2SupportBotAl_o.SetMater(det->materAl);
   IF_ thickness < 0 THEN
     thickness = -thickness;
     det->CC2SupportBotAl_o.SetMater(det->materVacuum);
   END_IF
   det->CC2SupportBotAl_o.dims.set(thickness/2, ywidth/2, zwidth/2);

   G4double gap_Plate_S4 = ReadU("gaps", "CC2--S4", 0.);

   ///////////////////////////////////
  //              S4               //
 ///////////////////////////////////

   G4bool S4_supports = ReadB("S4", "supports", true);

   // common:
   nlayers = ReadI("S4", "n-layers", 1);
   det->S4.assign(nlayers, BoxVolume());
   S4_gaps = ReadUv("topAC", "gaps", nlayers, 0.);

   // scintillator:
   det->S4.name = "S4";
   thicknesses = ReadUv("S4", "pSty-thickness", nlayers, 0.);
   attrscints.is = true;
   attrscints.nstrips      = ReadIv("S4", "n-strips", nlayers, 1);
   attrscints.s_offset     = ReadUv("S4", "strip-offset", nlayers, 0.);
   attrscints.p_nbins      = ReadIv("S4", "p-bins", nlayers, 1);
   attrscints.t_nbins      = ReadIv("S4", "t-bins", nlayers, 1);
   attrscints.t_min        = ReadUv("S4", "t-min", nlayers, 0*ns);
   attrscints.t_max        = ReadUv("S4", "t-max", nlayers, 1*s);
   attrscints.speedoflight = ReadU ("S4", "speed-of-light", speed_of_light_in_vacuum);
   ywidth    = ReadU("S4", "pSty-y-width", 0.);
   zwidth    = ReadU("S4", "pSty-z-width", 0.);
   vFORi_BEGIN(BoxVolume, det->S4, vol, layer)
     vol->name = det->S4.name + "[" + TFormat::itoa(layer) + "]";
     vol->attr.scint.is = true;
     vol->attr.scint.nstrips      = attrscints.nstrips.at(layer);
     vol->attr.scint.s_offset     = attrscints.s_offset.at(layer);
     vol->attr.scint.p_nbins      = attrscints.p_nbins.at(layer);
     vol->attr.scint.t_nbins      = attrscints.t_nbins.at(layer);
     vol->attr.scint.t_min        = attrscints.t_min.at(layer);
     vol->attr.scint.t_max        = attrscints.t_max.at(layer);
     vol->attr.scint.speedoflight = attrscints.speedoflight;
     vol->dims.set(thicknesses[layer]/2, ywidth/2, zwidth/2);
     vol->SetMother(det->world);
     vol->SetMater(det->materPSty);
   END_vFORi

   // Al:
   det->S4SupportAl.name = "S4SupportAl";
   thickness = ReadU("S4", "Al-thickness", 0.);
   ywidth    = ReadU("S4", "Al-y-width", 0.);
   zwidth    = ReadU("S4", "Al-z-width", 0.);
   alType    = ReadI("S4", "Al-type", 0);
   det->S4SupportAl.dims.set(thickness/2, ywidth/2, zwidth/2);
   det->S4SupportAl.SetMother(det->world);
   det->S4SupportAl.SetMater(AlType(alType));

   // CFRP:
   det->S4SupportTopCFRP.name = "S4SupportTopCFRP";
   thickness = ReadU("S4", "CFRP-thickness", 0.);
   ywidth    = ReadU("S4", "CFRP-y-width", 0.);
   zwidth    = ReadU("S4", "CFRP-z-width", 0.);
   det->S4SupportTopCFRP.dims.set(thickness/2, ywidth/2, zwidth/2);
   det->S4SupportTopCFRP.SetMother(det->world);
   det->S4SupportTopCFRP.SetMater(det->materCFRP);
   det->S4SupportBotCFRP = det->S4SupportTopCFRP;
   det->S4SupportBotCFRP.name = "S4SupportBotCFRP";

   G4double gap_S4_ND = ReadU("gaps", "S4--ND", 0.);

   ///////////////////////////////////
  //              ND               //
 ///////////////////////////////////

   G4bool ND_supports = ReadB("ND", "supports", true);

   // pEth:
   det->ND_pEth1.name = "ND_pEth1";
   thickness = ReadU("ND", "pEth1-thickness", 0.);
   ywidth    = ReadU("ND", "y-width", 0.);
   zwidth    = ReadU("ND", "z-width", 0.);
   det->ND_pEth1.dims.set(thickness/2, ywidth/2, zwidth/2);
   det->ND_pEth1.SetMother(det->world);
   det->ND_pEth1.SetMater(det->materPEth);

   det->ND_pEth2.name = "ND_pEth2";
   thickness = ReadU("ND", "pEth2-thickness", 0.);
   ywidth    = ReadU("ND", "y-width", 0.);
   zwidth    = ReadU("ND", "z-width", 0.);
   det->ND_pEth2.dims.set(thickness/2, ywidth/2, zwidth/2);
   det->ND_pEth2.SetMother(det->world);
   det->ND_pEth2.SetMater(det->materPEth);

   det->ND_pEth3.name = "ND_pEth3";
   thickness = ReadU("ND", "pEth3-thickness", 0.);
   ywidth    = ReadU("ND", "y-width", 0.);
   zwidth    = ReadU("ND", "z-width", 0.);
   det->ND_pEth3.dims.set(thickness/2, ywidth/2, zwidth/2);
   det->ND_pEth3.SetMother(det->world);
   det->ND_pEth3.SetMater(det->materPEth);

   // PMMA:
   det->ND_PMMA1.name = "ND_PMMA1";
   thickness = ReadU("ND", "PMMA1-thickness", 0.);
   ywidth    = ReadU("ND", "y-width", 0.);
   zwidth    = ReadU("ND", "z-width", 0.);
   det->ND_PMMA1.dims.set(thickness/2, ywidth/2, zwidth/2);
   det->ND_PMMA1.SetMother(det->world);
   det->ND_PMMA1.SetMater(det->materPMMA);

   det->ND_PMMA2.name = "ND_PMMA2";
   thickness = ReadU("ND", "PMMA2-thickness", 0.);
   ywidth    = ReadU("ND", "y-width", 0.);
   zwidth    = ReadU("ND", "z-width", 0.);
   det->ND_PMMA2.dims.set(thickness/2, ywidth/2, zwidth/2);
   det->ND_PMMA2.SetMother(det->world);
   det->ND_PMMA2.SetMater(det->materPMMA);

   det->ND_PMMA3.name = "ND_PMMA3";
   thickness = ReadU("ND", "PMMA3-thickness", 0.);
   ywidth    = ReadU("ND", "y-width", 0.);
   zwidth    = ReadU("ND", "z-width", 0.);
   det->ND_PMMA3.dims.set(thickness/2, ywidth/2, zwidth/2);
   det->ND_PMMA3.SetMother(det->world);
   det->ND_PMMA3.SetMater(det->materPMMA);

   det->ND_PMMA4.name = "ND_PMMA4";
   thickness = ReadU("ND", "PMMA4-thickness", 0.);
   ywidth    = ReadU("ND", "y-width", 0.);
   zwidth    = ReadU("ND", "z-width", 0.);
   det->ND_PMMA4.dims.set(thickness/2, ywidth/2, zwidth/2);
   det->ND_PMMA4.SetMother(det->world);
   det->ND_PMMA4.SetMater(det->materPMMA);

   // LiF ZnS:
   det->ND_LiFZnS1.name = "ND_LiFZnS1";
   thickness = ReadU("ND", "LiFZnS1-thickness", 0.);
   ywidth    = ReadU("ND", "y-width", 0.);
   zwidth    = ReadU("ND", "z-width", 0.);
   det->ND_LiFZnS1.dims.set(thickness/2, ywidth/2, zwidth/2);
   det->ND_LiFZnS1.SetMother(det->world);
   det->ND_LiFZnS1.SetMater(det->materLiFZnS);

   det->ND_LiFZnS2.name = "ND_LiFZnS2";
   thickness = ReadU("ND", "LiFZnS2-thickness", 0.);
   ywidth    = ReadU("ND", "y-width", 0.);
   zwidth    = ReadU("ND", "z-width", 0.);
   det->ND_LiFZnS2.dims.set(thickness/2, ywidth/2, zwidth/2);
   det->ND_LiFZnS2.SetMother(det->world);
   det->ND_LiFZnS2.SetMater(det->materLiFZnS);

   // Al:
   det->NDSupportAl.name = "NDSupportAl";
   thickness = ReadU("ND", "Al-thickness", 0.);
   ywidth    = ReadU("ND", "Al-y-width", 0.);
   zwidth    = ReadU("ND", "Al-z-width", 0.);
   alType    = ReadI("ND", "Al-type", 0);
   det->NDSupportAl.dims.set(thickness/2, ywidth/2, zwidth/2);
   det->NDSupportAl.SetMother(det->world);
   det->NDSupportAl.SetMater(AlType(alType));

   // CFRP:
   det->NDSupportTopCFRP.name = "NDSupportTopCFRP";
   thickness = ReadU("ND", "CFRP-thickness", 0.);
   ywidth    = ReadU("ND", "CFRP-y-width", 0.);
   zwidth    = ReadU("ND", "CFRP-z-width", 0.);
   det->NDSupportTopCFRP.dims.set(thickness/2, ywidth/2, zwidth/2);
   det->NDSupportTopCFRP.SetMother(det->world);
   det->NDSupportTopCFRP.SetMater(det->materCFRP);
   det->NDSupportBotCFRP = det->NDSupportTopCFRP;
   det->NDSupportBotCFRP.name = "NDSupportBotCFRP";

   G4double gap_ND_BottomStuff = ReadU("gaps", "ND--bottom-stuff", 0.);


   ///////////////////////////////////
  //         Bottom Stuff          //
 ///////////////////////////////////

   G4bool BottomStuff_supports = ReadB("Bottom Stuff", "supports", true);

   det->BottomStuff.name = "BottomStuff";
   thickness = ReadU("Bottom Stuff", "x-size", 0.);
   ywidth    = ReadU("Bottom Stuff", "y-size", 0.);
   zwidth    = ReadU("Bottom Stuff", "z-size", 0.);
   det->BottomStuff.dims.set(thickness/2, ywidth/2, zwidth/2);
   det->BottomStuff.SetMother(det->world);
   det->BottomStuff.SetMater(det->materBottomStuff);


   ///////////////////////////////////
  //             Gaps              //
 ///////////////////////////////////

   #define THICK(d) ( det->d.dims.x()*2 )
   #define TOP_PLANE(d) (det->d.pos.x() - det->d.dims.x())
   #define BOT_PLANE(d) (det->d.pos.x() + det->d.dims.x())
   #define THICKL(d) (BOT_PLANE(d.back()) - TOP_PLANE(d.front()))
   #define MIDPOINT(d) ((BOT_PLANE(d.back()) + TOP_PLANE(d.front()))/2)
   /*G4double dist_S1_SiArray2 = dist_S1_S2 - gap_SiArray2_S2 - THICK(SiArray2)
      - THICK(SiArray2SupportTopCFRP) - THICK(SiArray2SupportBotCFRP) - THICK(SiArray2SupportAl) - .5*THICK(S2);
   G4double gap_S1_SiArray2 = dist_S1_SiArray2 - THICK(S1SupportTopCFRP) - THICK(S1SupportBotCFRP)
      - THICK(S1SupportAl) - .5*THICK(S1);*/
   G4double gap_S1_S2 = dist_S1_S2 - .5*THICKL(S1) - .5*THICKL(S2) - THICK(S1SupportAl) - 2*THICK(S1SupportBotCFRP);
   // G4double gap_CC1_S3 = dist_S2_S3 - THICK(S2SupportTopCFRP) - THICK(S2SupportBotCFRP) - THICK(S2SupportAl) - gap_S2_CC1
   //    - .5*THICK(S2) - .5*THICK(S3);
   // if (!det->CC1LayerSiZ.empty())
   //   gap_CC1_S3 -= BOT_PLANE(CC1LayerSiZ.back()) -
   //                 TOP_PLANE(CC1SupportTopCFRP_CsI.front());


   ///////////////////////////////////
  //       Lateral detectors       //
 ///////////////////////////////////

   G4double xwidth;
   /*enum EDetSide {
     kLeft  = DetectorConstruction::kLeft,
     kFront = DetectorConstruction::kFront,
     kRight = DetectorConstruction::kRight,
     kBack  = DetectorConstruction::kBack
   };*/
   const DetectorConstruction::EDetSide kLeft =     DetectorConstruction::kLeft;   /* -z */
   const DetectorConstruction::EDetSide kFront =    DetectorConstruction::kFront; /* -y */
   const DetectorConstruction::EDetSide kRight =    DetectorConstruction::kRight; /* +z */
   const DetectorConstruction::EDetSide kBack =     DetectorConstruction::kBack;   /* +y */
   const DetectorConstruction::EDetSide kSomeSide = DetectorConstruction::kLeft;   /* when not important, be it left */

   G4double sideAC_protrude;
   G4double gap_topAC_sideAC = ReadU("gaps", "topAC--sideAC", 0.);
   xwidth    = ReadU("sideAC", "length", 0.);
   sideAC_protrude = ReadU("gaps", "topAC<>sideAC", 0.) + det->topAC.front().dims.y() + thickness;
   det->sideAC_horizontal = ReadB("sideAC", "strips-horizintal", false);
   G4int sideAC_nlayers = ReadI("sideAC", "n-layers", 1);
   vector<G4double> sideAC_thicknesses = ReadUv("sideAC", "thickness", sideAC_nlayers, 0.);
   vector<G4int> sideAC_nstrips      = ReadIv("sideAC", "n-strips", sideAC_nlayers, 1);
   vector<G4double> sideAC_s_offset  = ReadUv("sideAC", "strip-offset", sideAC_nlayers, 0.);
   vector<G4int> sideAC_p_nbins      = ReadIv("sideAC", "p-bins", sideAC_nlayers, 1);
   vector<G4int> sideAC_t_nbins      = ReadIv("sideAC", "t-bins", sideAC_nlayers, 1);
   vector<G4double> sideAC_t_min     = ReadUv("sideAC", "t-min", sideAC_nlayers, 0*ns);
   vector<G4double> sideAC_t_max     = ReadUv("sideAC", "t-max", sideAC_nlayers, 1*s);
   G4double sideAC_speedoflight      = ReadU("sideAC", "speed-of-light", speed_of_light_in_vacuum);
   vector<G4double> sideAC_gaps_between_layers = ReadUv("sideAC", "gaps", sideAC_nlayers-1, 0.);
   vector<G4double> sideAC_gaps_between_sides = ReadUv("sideAC", "side-gaps", sideAC_nlayers, 0.);
   det->sideAC.assign(4, BoxVolumes());
   uFORs(side_i, 0, 4) {
   //vFOR(BoxVolumes, det->sideAC, vols) {
     DetectorConstruction::EDetSide side = static_cast<DetectorConstruction::EDetSide>(side_i);
     det->sideAC[side].attr.scints.is = true;
     det->sideAC[side].attr.scints.nstrips      = sideAC_nstrips;
     det->sideAC[side].attr.scints.s_offset     = sideAC_s_offset;
     det->sideAC[side].attr.scints.p_nbins      = sideAC_p_nbins;
     det->sideAC[side].attr.scints.t_nbins      = sideAC_t_nbins;
     det->sideAC[side].attr.scints.t_min        = sideAC_t_min;
     det->sideAC[side].attr.scints.t_max        = sideAC_t_max;
     det->sideAC[side].attr.scints.speedoflight = sideAC_speedoflight;
     uFORs(layer, 0, sideAC_nlayers) {
       //vFOR(BoxVolume, det->sideAC[side], vol) {
       det->sideAC[side].push_back(det->sideAC[side].DefaultChild(layer));
       BoxVolume* vol = &det->sideAC[side].at(layer);
       vol->name = "sideAC-" + DetectorConstruction::DetSideName(side) + "[" + TFormat::itoa(layer) + "]";
       SWITCH (side)
         CASE kLeft OR kRight:
           vol->dims.set(.5*xwidth, .5*thickness, sideAC_protrude + microgap - .5*thickness);
         CASE kBack OR kFront:
           vol->dims.set(.5*xwidth, sideAC_protrude + microgap - .5*thickness, .5*thickness);
       END_SWITCH
       det->sideAC[side][layer].SetMater(det->materPSty);
       det->sideAC[side][layer].SetMother(det->world);
     }
   }
   //det->sideAC.assign(4, det->sideAC.DefaultChild());




   G4double TD_outerdist;
   G4double offset_S2_TD = ReadU("gaps", "S2--TD", 0.);
   xwidth    = ReadU("TD", "length", 0.);
   thickness = ReadU("TD", "thickness", 0.);
TD_outerdist = ReadU("gaps", "S2<>TD", 0.) + det->S2.front().dims.y() + thickness;
   det->TD.name = "TD";
   det->TD.SetMater(det->materPSty);
   det->TD.SetMother(det->world);
   det->TD.assign(4, det->TD.DefaultChild());
   det->TD[kLeft].dims.set(.5*xwidth, .5*thickness, TD_outerdist + microgap - .5*thickness);
   det->TD[kRight].dims = det->TD[kLeft].dims;
   det->TD[kBack].dims.set(.5*xwidth, TD_outerdist + microgap - .5*thickness, .5*thickness);
   det->TD[kFront].dims = det->TD[kBack].dims;
   det->TD[kLeft].name  += "[left]";
   det->TD[kRight].name += "[right]";
   det->TD[kBack].name  += "[back]";
   det->TD[kFront].name += "[front]";


   ///////////////////////////////////
  //           Build-up            //
 ///////////////////////////////////

   G4double currentpos = 0, tmppos;

   // World:
   det->world.CreateVolume();

   // topAC:
   det->topACSupportTopCFRP.CreateVolume(currentpos);
   det->topACSupportAl.CreateVolume(currentpos);
   det->topACSupportBotCFRP.CreateVolume(currentpos);
   //vFOR(BoxVolume, det->topAC, vol)
   uFORs(layer, 0, det->topAC.size()) {
     BoxVolume* vol = &det->topAC[layer];
     vol->CreateVolume(currentpos);
     if (layer < det->topAC.size() - 1) // not last
       currentpos += topAC_gaps.at(layer);
   }
   currentpos += gap_topAC_C;
                     //     ↖
   // sideAC:               ↙ go back     ↙ lower by a gap   ↙ lower to sideAC’s center
   tmppos = currentpos - gap_topAC_C + gap_topAC_sideAC + det->sideAC[kSomeSide].front().dims.x();
   //size_t sideAC_nlayers = det->sideAC[kSomeSide].size();
     vFOR(BoxVolumes, det->sideAC, subvol) {
       subvol->nlayers = 4; // <- Not sure it matters...
     }
   // ┌───┐ ┌──────────────────────────┐╶┐
   // │   │ │                          │ ├─ 2 * d[0] (sideAC_thicknesses)
   // │   │ └──────────────────────────┘╶┤_
   // │   │                        ┌───┐╶┘ ` g[0] (sideAC_gaps_between_layers)
   // │   │  ┌───┐ ┌────────────┐  │   │
   // │   │  │   │ │            │  │   │
   // │   │  │   │ └────────────┘ ╴│╶┄╴│╶┐_ g[1]
   // │   │  │   │          ┌───┐ ╴│╶┄╴│╶┘
   // │   │  │   │          │   │  │   │
   // │   │  │   │          │   │  │   │
   // │   │  └───┘          │   │  │   │
   // │   │  ┌────────────┐ │   │  │   │
   // │   │  │            │ │   │  │   │
   // │   │  └────────────┘ └───┘ ╴│╶┄╴│╶┐
   // └───┘                        │   │ ├─ m[0] (sideAC_gaps_between_sides)
   // ┌──────────────────────────┐┄│╶┄╴│╶┘
   // │                          │ │   │
   // └──────────────────────────┘ └───┘
   G4double cur_dist_to_midline = NaN, prev_dist_to_midline = NaN;
   G4double cur_halflength = NaN, prev_halflength = NaN;
   G4double cur_halfthick = NaN, prev_halfthick = NaN;
   G4double cur_halfgap = NaN, prev_halfgap = NaN;
   G4double cur_lgap = NaN, prev_lgap = NaN;
   G4double cur_shift = NaN;
   G4double outer_side = det->topAC.back().dims.y();
   uFORs(layer, 0, sideAC_nlayers) {
     cur_halfthick = sideAC_thicknesses.at(layer)/2; // d[i]
     cur_halfgap = sideAC_gaps_between_sides.at(layer)/2; // m[i]/2
     cur_shift = cur_halfthick + cur_halfgap; // z[i] = d[i] + m[i]/2
     cur_lgap = layer == sideAC_nlayers-1 // g[i]
       ? NaN
       : sideAC_gaps_between_layers.at(layer);
     if (layer == 0) {
       cur_dist_to_midline = outer_side - cur_halfthick; // y[0] = L - d[0]
       cur_halflength = cur_dist_to_midline - cur_halfgap - microgap; // l[0] = L - d[0] - m[0]/2
     } else {
       cur_dist_to_midline = prev_dist_to_midline - prev_halfthick - cur_halfthick
         - prev_lgap - microgap; // y[i] = y[i-1] - d[i-1] - d[i] - g[i-1]
       cur_halflength = prev_halflength - cur_halfthick - prev_halfthick - prev_lgap
         - cur_halfgap + prev_halfgap; // l[i] = l[i-1] - (d[i] + d[i-1]) - (m[i] - m[i-1])/2
     }
     det->sideAC[kFront][layer].pos.set(
       /* x */ tmppos,
       /* y */ - cur_dist_to_midline,
       /* z */ - cur_shift );
     det->sideAC[kBack][layer].pos.set(
       /* x */ tmppos,
       /* y */ + cur_dist_to_midline,
       /* z */ + cur_shift );
     det->sideAC[kRight][layer].pos.set(
       /* x */ tmppos,
       /* y */ - cur_shift,
       /* z */ + cur_dist_to_midline );
     det->sideAC[kLeft][layer].pos.set(
       /* x */ tmppos,
       /* y */ + cur_shift,
       /* z */ - cur_dist_to_midline );
     det->sideAC[kLeft][layer].dims.setY(cur_halflength);
     det->sideAC[kLeft][layer].dims.setZ(cur_halfthick);
     det->sideAC[kRight][layer].dims.setY(cur_halflength);
     det->sideAC[kRight][layer].dims.setZ(cur_halfthick);
     det->sideAC[kFront][layer].dims.setZ(cur_halflength);
     det->sideAC[kFront][layer].dims.setY(cur_halfthick);
     det->sideAC[kBack][layer].dims.setZ(cur_halflength);
     det->sideAC[kBack][layer].dims.setY(cur_halfthick);
     vFOR(BoxVolumes, det->sideAC, subvol) {
       (*subvol)[layer].CreateVolume();
     }
     
     // shift:
     prev_dist_to_midline = cur_dist_to_midline;
     prev_halflength = cur_halflength;
     prev_halfthick = cur_halfthick;
     prev_halfgap = cur_halfgap;
     prev_lgap = cur_lgap;
   }

   // C:
   bool C_interleave = ReadB("C", "interleave-layers", false);
   odd = true;
   nlayers = det->CLayerSiY.nlayers;
   if(nlayers) dFORs(ilayer, 0, nlayers+1) {
     det->CSupportTopCFRP[ilayer].CreateVolume(currentpos);
     det->CSupportAl     [ilayer].CreateVolume(currentpos);
     det->CSupportBotCFRP[ilayer].CreateVolume(currentpos);
     if(ilayer == nlayers) break;
     det->CLayerW        [ilayer].CreateVolume(currentpos);
     (odd?det->CLayerSiY:det->CLayerSiZ)[ilayer].CreateVolume(currentpos, C_gaps[ilayer]);
     (odd?det->CLayerSiZ:det->CLayerSiY)[ilayer].CreateVolume(currentpos);
     if(C_interleave) odd -= 1;
   };
   currentpos += gap_C_S1;

   // S1:
   vFOR(BoxVolume, det->S1, vol)
     vol->CreateVolume(currentpos);
   det->S1SupportTopCFRP.CreateVolume(currentpos);
   det->S1SupportAl.CreateVolume(currentpos);
   det->S1SupportBotCFRP.CreateVolume(currentpos, gap_S1_S2);

    // TD:
   tmppos = currentpos + offset_S2_TD;
   det->TD[kLeft] .pos.set(tmppos, -det->TD[kLeft] .dims.z()-microgap, -TD_outerdist-microgap+det->TD[kLeft] .dims.z());
   det->TD[kRight].pos.set(tmppos, +det->TD[kRight].dims.z()+microgap, +TD_outerdist+microgap-det->TD[kRight].dims.z());
   det->TD[kFront].pos.set(tmppos, -TD_outerdist-microgap+det->TD[kFront].dims.y(), +det->TD[kFront].dims.y()+microgap);
   det->TD[kBack] .pos.set(tmppos, +TD_outerdist+microgap-det->TD[kBack] .dims.y(), -det->TD[kBack] .dims.y()-microgap);
   vFOR(BoxVolume, det->TD, subvol) subvol->CreateVolume();

currentpos = MIDPOINT(S1)/*det->S1.pos.x()*/ + dist_S1_S2 - THICKL(S2)/2/*det->S2.dims.x()*/;

   // S2:
   vFOR(BoxVolume, det->S2, vol)
     vol->CreateVolume(currentpos);
   det->S2SupportTopCFRP.CreateVolume(currentpos);
   det->S2SupportAl.CreateVolume(currentpos);
   det->S2SupportBotCFRP.CreateVolume(currentpos, gap_S2_CC1);

   // CC1:
   //odd = true;
   nlayers = det->CC1LayerSiY.nlayers;
   if(nlayers) dFORs(ilayer, 0, nlayers) {
     det->CC1SupportTopCFRP_CsI[ilayer].CreateVolume(currentpos);
     det->CC1SupportAl_CsI     [ilayer].CreateVolume(currentpos);
     det->CC1SupportBotCFRP_CsI[ilayer].CreateVolume(currentpos);
     det->CC1LayerCsI          [ilayer].CreateVolume(currentpos, CC1_gaps_CS[ilayer]);
     det->CC1LayerSiY          [ilayer].CreateVolume(currentpos);
     det->CC1SupportTopCFRP_Si [ilayer].CreateVolume(currentpos);
     det->CC1SupportAl_Si      [ilayer].CreateVolume(currentpos);
     det->CC1SupportBotCFRP_Si [ilayer].CreateVolume(currentpos);
     det->CC1LayerSiZ          [ilayer].CreateVolume(currentpos);
     if(ilayer == nlayers) break;
     currentpos += CC1_gaps_SC[ilayer];
   };

   // currentpos += gap_CC1_S3;
   currentpos = MIDPOINT(S2)/*det->S2.pos.x()*/ + dist_S2_S3 - THICKL(S3)/2/*det->S3.dims.x()*/;


   // S3:
   vFOR(BoxVolume, det->S3, vol)
     vol->CreateVolume(currentpos);
   det->S3SupportTopCFRP.CreateVolume(currentpos);
   det->S3SupportAl.CreateVolume(currentpos);
   det->S3SupportBotCFRP.CreateVolume(currentpos, gap_S3_CC2);

   // CC2:
   det->CC2SupportTopAl_o.CreateVolume(currentpos);
   det->CC2SupportTopAl_i.CreateVolume(currentpos);
   IF(CC2_segmented)
     //det->CC2_mirrorfilm.CreateVolume(currentpos, -2*det->CC2_mirrorfilm.dims.x(), true);
     det->CC2_CFRP_x.CreateVolume(currentpos, -2*det->CC2_CFRP_x.dims.x(), true);
     det->CC2_CFRP_y.CreateVolume(currentpos, -2*det->CC2_CFRP_y.dims.x(), true);
     det->CC2_CFRP_z.CreateVolume(currentpos, -2*det->CC2_CFRP_z.dims.x(), true);
     det->CC2.CreateVolume(currentpos, gap_CC2_Plate);
     //currentpos += 2*det->CC2.dims.x();
   ELSE
     det->CC2SupportTopCFRP.CreateVolume(currentpos);
     det->CC2_single.CreateVolume(currentpos);
     det->CC2SupportBotCFRP.CreateVolume(currentpos, gap_CC2_Plate);
   END_IF
   det->CC2SupportBotAl_i.CreateVolume(currentpos);
   det->CC2SupportBotAl_o.CreateVolume(currentpos, gap_Plate_S4);

   // Al Plate:
   //det->AlPlate.CreateVolume(currentpos, gap_Plate_S4);

   // det->CC2SupportTopAl_o.Print();
   // det->CC2SupportTopAl_i.Print();
   // det->CC2SupportBotAl_i.Print();
   // det->CC2SupportBotAl_o.Print();

   // S4:
   vFOR(BoxVolume, det->S4, vol)
     vol->CreateVolume(currentpos);
   det->S4SupportTopCFRP.CreateVolume(currentpos);
   det->S4SupportAl.CreateVolume(currentpos);
   det->S4SupportBotCFRP.CreateVolume(currentpos, gap_S4_ND);

   // ND:
   det->ND_pEth1.CreateVolume(currentpos);
     det->ND_PMMA1.CreateVolume(currentpos);
       det->ND_LiFZnS1.CreateVolume(currentpos);
     det->ND_PMMA2.CreateVolume(currentpos);
   det->ND_pEth2.CreateVolume(currentpos);
     det->ND_PMMA3.CreateVolume(currentpos);
       det->ND_LiFZnS2.CreateVolume(currentpos);
     det->ND_PMMA4.CreateVolume(currentpos);
   det->ND_pEth3.CreateVolume(currentpos);
   det->NDSupportTopCFRP.CreateVolume(currentpos);
   det->NDSupportAl.CreateVolume(currentpos);
   det->NDSupportBotCFRP.CreateVolume(currentpos, gap_ND_BottomStuff);

   // BottomStuff:
   det->BottomStuff.CreateVolume(currentpos);


   ///////////////////////////////////
  //           Supports            //
 ///////////////////////////////////

   unless( topAC_supports && global_supports )
     det->topACSupportTopCFRP =
     det->topACSupportAl      =
     det->topACSupportBotCFRP =
     det->materVacuum;
   unless( C_supports && global_supports )
     det->CSupportTopCFRP =
     det->CSupportBotCFRP =
     det->CSupportAl      =
     det->materVacuum;
   unless( S1_supports && global_supports )
     det->S1SupportTopCFRP =
     det->S1SupportAl      =
     det->S1SupportBotCFRP =
     det->materVacuum;
   unless( S2_supports && global_supports )
     det->S2SupportTopCFRP =
     det->S2SupportAl      =
     det->S2SupportBotCFRP =
     det->materVacuum;
   unless( CC1_supports && global_supports )
     det->CC1SupportTopCFRP_CsI =
     det->CC1SupportBotCFRP_CsI =
     det->CC1SupportAl_CsI =
     det->CC1SupportTopCFRP_Si =
     det->CC1SupportBotCFRP_Si =
     det->CC1SupportAl_Si =
     det->materVacuum;
   unless( S3_supports && global_supports )
     det->S3SupportTopCFRP =
     det->S3SupportAl      =
     det->S3SupportBotCFRP =
     det->materVacuum;
   unless( CC2_supports && global_supports )
     det->CC2SupportTopCFRP =
     det->CC2SupportBotCFRP =
     det->materVacuum;
   unless( S4_supports && global_supports )
     det->S4SupportTopCFRP =
     det->S4SupportAl      =
     det->S4SupportBotCFRP =
     det->materVacuum;
   unless( ND_supports && global_supports )
     det->NDSupportTopCFRP =
     det->NDSupportAl      =
     det->NDSupportBotCFRP =
     det->materVacuum;
   unless( BottomStuff_supports && global_supports )
     det->BottomStuff =
     det->materVacuum;

   return true;

END(Geometry::Read)
