#include "HitCalo3D.hh"
#include "G4UnitsTable.hh"
#include "G4VVisManager.hh"
#include "G4Circle.hh"
#include "G4Colour.hh"
#include "G4VisAttributes.hh"

G4Allocator<HitCalo3D> hitAllocatorCalo3D;


HitCalo3D::HitCalo3D() : trackID(-1), edep(0) {}

HitCalo3D::~HitCalo3D() {}

HitCalo3D::HitCalo3D(const HitCalo3D& right)
  : G4VHit()
{
  trackID   = right.trackID;
  edep      = right.edep;
  pos       = right.pos;
}

const HitCalo3D& HitCalo3D::operator=(const HitCalo3D& right)
{
  trackID   = right.trackID;
  edep      = right.edep;
  pos       = right.pos;
  return *this;
}

G4int HitCalo3D::operator==(const HitCalo3D& right) const
{
  return (this==&right) ? 1 : 0;
}

void HitCalo3D::Draw()
{
  
}

void HitCalo3D::Print()
{
  G4cout << "  track ID:       " << trackID << '\n';
  G4cout << "  energy deposit: " << G4BestUnit(edep,"Energy") << '\n';
  G4cout << "  position:       " << G4BestUnit(pos,"Length") << G4endl;
}
