#include "SensitiveEnergy.hh"
#include "globals.hh"
#include "G4HCofThisEvent.hh"
#include "G4Step.hh"
#include "G4ThreeVector.hh"
#include "G4SDManager.hh"
#include "G4ios.hh"
#include "Parameterisation.hh"
#include "G4PVParameterised.hh"
#include "G4Box.hh"
#include "G4VPVParameterisation.hh"
#include "G4VSolid.hh"
#include "FOR.hh"

class Parameterisation;
class G4VPVParameterisation;
class G4PVParameterised;
class G4VSolid;
class G4Box;

SensitiveEnergy::SensitiveEnergy(G4String name, DetectorConstruction* aDet, G4VPhysicalVolume* pv,
       EnergySystem* egy, G4int dP, G4int copyNo,
       EDetectorType type /* = kOtherDetector*/)
:SensitiveDetector(name)
,egySystem(egy)
,hitsCollection(NULL)
,etot(0)
,gain(1)
,detPart(dP)
{
  detConstr = aDet;
  SDtype = type;
  fPV = pv;
  fCopyNo = copyNo;
G4cout << "Sensitive " << SensitiveDetectorName << " constructed" << G4endl;
  G4String tmp = name(1,1);
  tmp.toUpper();
  collectionName.insert(G4String("hitsCollection")+tmp+G4String(name(2,name.size()-2)));
  if(fPV->IsParameterised()) {
     Parameterisation* parampampam = (Parameterisation*) ((G4PVParameterised*)fPV)->GetParameterisation();
     fpos  = parampampam->GetPosition(fCopyNo);
     fdims = parampampam->GetDimentions(fCopyNo);
  }else{
     fpos  = fPV->GetTranslation();
     G4Box* box = (G4Box*) fPV->GetLogicalVolume()->GetSolid();
     fdims.set(box->GetXHalfLength(),box->GetYHalfLength(),box->GetZHalfLength());
  };
}

SensitiveEnergy::~SensitiveEnergy() { }

void SensitiveEnergy::Initialize(G4HCofThisEvent* HCE)
{
  hitsCollection = new HitsCollectionEnergy
                          (SensitiveDetectorName,collectionName[0]); 
  static G4int HCID = -1;
  if(HCID<0)
  { HCID = G4SDManager::GetSDMpointer()->GetCollectionID(collectionName[0]); }
  HCE->AddHitsCollection( HCID, hitsCollection );
  etot = 0.;
}


G4bool SensitiveEnergy::ProcessHits(G4Step* aStep, G4TouchableHistory*)
{
  G4double edep = aStep->GetTotalEnergyDeposit();
  if(edep==0.) return false;
  G4ThreeVector hitpos = aStep->GetPreStepPoint()->GetPosition();
  HitEnergy* newHit = new HitEnergy();
  newHit->SetTrackID(aStep->GetTrack()->GetTrackID());
  newHit->SetPos(hitpos);
  newHit->SetEdep(edep);
  if(verboseLevel>1) {
  G4cout << "Hit in " << SensitiveDetectorName << ";\tenergy: " << edep
         << ";\tposition: {" << hitpos.x()/cm << ", "
         << hitpos.y()/cm << ", " << hitpos.z()/cm << "} cm" <<  G4endl;
  };
  newHit->SetEdep(edep);
  newHit->SetPos(aStep->GetPostStepPoint()->GetPosition());
  hitsCollection->insert(newHit);
  
  etot += edep;
    
  //newHit->Print();
  //newHit->Draw();

  return true;
}

void SensitiveEnergy::EndOfEvent(G4HCofThisEvent*)
{
  if(verboseLevel>0) { 
     G4int nhits = hitsCollection->entries();
     G4cout << "\n-------->Hits Collection: in this event they are " << nhits
            << " hit" << ((nhits-1)?"s":"") << " in the detector "
            << SensitiveDetectorName << (nhits?": ":".") << G4endl;
     FOR(i, 0, nhits-1) (*hitsCollection)[i]->Print();
  };
  
  EnergyPoint tp;
      tp.edep = etot;
      tp.amplitude = GetAmplitude();
      tp.enabled = etot > 10*keV;
      tp.SDname = SensitiveDetectorName;
      tp.SDtype = SDtype;
      tp.detPart = detPart;
      tp.detLayer = fCopyNo;
      egySystem->Append(tp);
      etot = 0;
}
