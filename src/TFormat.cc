#include "TFormat.hh"
				namespace TFormat    {

G4String blanks(G4int n, G4String c)
{
	G4String str("");
	for(int i = 1; i <= n; i++)
		str += c;
	return str;
}

G4String blanks(G4int n, char c)
{
	G4String str("");
	for(int i = 1; i <= n; i++)
		str += c;
	return str;
}

G4String digitblock(const G4int num)
{
	char str[12] = "           ";
	if(num < 1000)
		sprintf(str,   "%11d", num);
	else if(num < 1000000) {
		sprintf(str,    "%7d", num/1000);
		str[7] = '`';
		sprintf(str+8, "%03d", num%1000);
	}
	else {
		sprintf(str,    "%3d", num/1000000);
		str[3] = '`';
		sprintf(str+4, "%03d", num%1000000/1000);
		str[7] = '`';
		sprintf(str+8, "%03d", num%1000);
	}
	G4String Str(str);
	return Str;
}

G4String postfix(const G4int num)
{
	if( num%100/10 == 1 )
		return "th";
	if( num%10 == 1 )
		return "st";
	if( num%10 == 2 )
		return "nd";
	if( num%10 == 3 )
		return "rd";
	return "th";
}

std::ostream& progress(std::ostream& out, G4int iev, G4int nev,
                  const char* name, G4int minprint)
{
	if(  !(iev % minprint) && iev && (iev % (10*minprint))  )
		out << '.' << std::flush;
	if(  !(iev % (10*minprint)) && iev  )
		{
		out << "" << digitblock(iev) << "-th " << name << std::flush;
		if(nev)
			out << " (" << /*std::setw(7) <<*/ (G4float)iev/nev*100 << " %)" << G4endl;
		else
			out << G4endl;
		}
	if(  !((iev+10*minprint) % (100*minprint)) && iev  )
		out << G4endl;
	return out;
}

G4String cropblanksleft(G4String str)
{
	G4String Str(str);
	while(Str.c_str()[0] == ' ')
		Str = Str(1, Str.size()-1);
	return Str;
}

G4String cropblanksright(G4String str)
{
	G4String Str(str);
	while(Str.c_str()[Str.size()-1] == ' ')
		Str = Str(0, Str.size()-1);
	return Str;
}

G4String digitblockcrop(const G4int num)
{
	return cropblanksleft(digitblock(num));
}

G4String itoa(G4int num, G4int buffersize)
{
	char* buffer = (char*) calloc(buffersize, sizeof(char));
	sprintf(buffer, "%d", num);
	return G4String(buffer);
}

G4String ftoa(G4double num, G4int buffersize)
{
	char* buffer = (char*) calloc(buffersize, sizeof(char));
	sprintf(buffer, "%f", num);
	return G4String(buffer);
}

G4String centered(G4String str, const str_size len, const char c /*= ' '*/)
{
	G4String Str(str);
	//if(righter && Str.size()<len) Str = G4String(" ") + Str;
	while(Str.size()<len) {
		Str += c;
		if(Str.size()<len) Str = G4String(c) + Str;
	};
	return Str;
}

G4String padright(G4String str, const str_size len, const char c /*= ' '*/)
{
	str_size initlen = str.size();
	if(initlen >= len) return str;
	return str + blanks(len-initlen, c);
}

G4String padleft(G4String str, const str_size len, const char c /*= ' '*/)
{
	str_size initlen = str.size();
	if(initlen >= len) return str;
	return blanks(len-initlen, c) + str;
}
				/* namespace TFormat */ }
