#include "SensitiveScintStrip.hh"
#include "globals.hh"
#include "G4HCofThisEvent.hh"
#include "G4Step.hh"
#include "G4ThreeVector.hh"
#include "G4SDManager.hh"
#include "G4ios.hh"
#include "Parameterisation.hh"
#include "G4PVParameterised.hh"
#include "G4Box.hh"
#include "G4VPVParameterisation.hh"
#include "G4VSolid.hh"
#include "FOR.hh"

class Parameterisation;
class G4VPVParameterisation;
class G4PVParameterised;
class G4VSolid;
class G4Box;

// magic constant
const G4double SensitiveScintStrip::kNoSignal = -9.87654321e31;

SensitiveScintStrip::SensitiveScintStrip(
    G4String name,
    DetectorConstruction* aDet,
    G4VPhysicalVolume* pv,
    ScintillationSystem* sc,
    G4int nstrips,
    G4int dP,
    G4int copyNo,
    EDetectorType type /* = kOtherDetector*/,
    EStripProj ss /* = kNoStripProj */,
    EStripProj pp /* = kNoStripProj */)
: SensitiveStrip(name, aDet, pv, NULL, nstrips, nstrips, dP, copyNo, type, ss, pp, 0, true)
, earliest(kNoSignal)
, latest(kNoSignal)
, Etot(0.)
, hassignal(false)
, system(sc)
, hitsCollection(NULL)
, sAxis(ss)
, pAxis(pp)
{
G4cout << "Sensitive " << SensitiveDetectorName << " constructed" << G4endl;
  G4String tmp = name(1,1);
  tmp.toUpper();
  collectionName.insert( G4String("hitsCollection") + tmp + G4String(name(2, name.size() - 2)) );
  data.sensitive = this;
  stripEtot = SelectByStripProj(acrossaxis, &etotX, &etotY, &etotZ);
  //sc.lavers.push_back(&data);
}


SensitiveScintStrip::~SensitiveScintStrip() { }

void SensitiveScintStrip::Initialize(G4HCofThisEvent* HCE)
{
  hitsCollection = new HitsCollectionScintStrip(
    SensitiveDetectorName,
    collectionName[0]);
  static G4int HCID = -1;
  if (HCID<0)
    HCID = G4SDManager::GetSDMpointer()->GetCollectionID(collectionName[0]);
  HCE->AddHitsCollection( HCID, hitsCollection );
}

#if 0
void SensitiveScintStrip::Setup(G4double t_min, G4double t_max, G4double t_bin,
                               G4double p_bin, G4double speedoflight)
{
  Disp("SensitiveScintStrip::Setup {");
  G4Box* box = (G4Box*) fPV->GetLogicalVolume()->GetSolid();
  //G4double p_min, p_max, s_min, s_max, s_nbins;
  if (fAxis == kYStripProj) {
    data.s_min = fPV->GetTranslation().y() - box->GetYHalfLength();
    data.s_max = fPV->GetTranslation().y() + box->GetYHalfLength();
    data.p_min = fPV->GetTranslation().z() - box->GetZHalfLength();
    data.p_max = fPV->GetTranslation().z() + box->GetZHalfLength();
    data.s_nbins = fNstripsY;
  } else {
    data.s_min = fPV->GetTranslation().z() - box->GetZHalfLength();
    data.s_max = fPV->GetTranslation().z() + box->GetZHalfLength();
    data.p_min = fPV->GetTranslation().y() - box->GetYHalfLength();
    data.p_max = fPV->GetTranslation().y() + box->GetYHalfLength();
    data.s_nbins = fNstripsZ;
  }
  data.t_min = t_min;
  data.t_max = t_max;
  data.t_nbins = bincnt(t_min, t_max, t_bin);
  data.p_nbins = bincnt(data.p_min, data.p_max, p_bin);
  data.speedoflight = speedoflight;
  data.proj = fAxis;
  stripEtot->assign(0, data.s_nbins);
  
  data.Initialize();
  Disp("SensitiveScintStrip::Setup }");
}
#endif

void SensitiveScintStrip::Setup(
    G4double t_min,
    G4double t_max,
    G4int t_nbins,
    G4int p_nbins,
    G4int s_nbins,
    G4double speedoflight)
{
  G4Box* box = (G4Box*)fPV->GetLogicalVolume()->GetSolid();
  #ifndef SET_SCINT_STRIP_MIN_MAX
  #define SET_SCINT_STRIP_MIN_MAX(proj, msg)          \
  switch (proj##Axis) {                                \
    case kXStripProj:                                   \
      data.proj##_min = x_minmax(-1, box);         /* */ \
      data.proj##_max = x_minmax(+1, box);        /* */   \
    break;                                       /* */     \
    case kYStripProj:                           /* */       \
      data.proj##_min = y_minmax(-1, box);     /* */    /* */\
      data.proj##_max = y_minmax(+1, box);    /* \*\   /x */  \
    break;                                   /*   \*\ /x x/\*/ \
    case kZStripProj:                       /*     \+/x x/  \*/ \
      data.proj##_min = z_minmax(-1, box);          /* */   /* */\
      data.proj##_max = z_minmax(+1, box);         /* */   /* */  \
    break;                                        /* * \*\/x */    \
    default:                                     /* *   \+x */ \
      G4Exception(                              /* */   /* */   \
        "SensitiveScintStrip::Setup",          /* */   /* *  x */\
        "Fatal exception",                    /* */   /* * \* */  \
        FatalException,                      /* */        /* */    \
        msg " X, Y or Z direction.");       /*  /xx*/    /* */      \
  }
  #endif
  SET_SCINT_STRIP_MIN_MAX(s, "Strips can only have")
  SET_SCINT_STRIP_MIN_MAX(p, "Strips can only be parallel to")
  data.t_min = t_min;
  data.t_max = t_max;
  data.t_nbins = t_nbins;
  data.p_nbins = p_nbins;
  data.s_nbins = s_nbins;
  data.speedoflight = speedoflight;
  data.acrossproj = acrossaxis;
  data.alongproj = alongaxis;
  
  data.Initialize();
}



G4bool SensitiveScintStrip::ProcessHits(G4Step* aStep, G4TouchableHistory*)
{
  G4double edep = aStep->GetTotalEnergyDeposit();
  //data[ScintStripIndex(0,0,0)].edep += 1.111111+edep;
  if (edep == 0.) return false;
  G4StepPoint* point = aStep->GetPreStepPoint();
  G4double t = point->GetGlobalTime();
  G4ThreeVector r = point->GetPosition();
  //G4double /*x = r.x(),*/  y = r.y(),  z = r.z();
  
  //G4double bin_t = findbin(data.t_min, data.t_max, data.t_nbins, t);
  //G4double bin_s, bin_p;
  //bin_s = findbin(data.s_min, data.s_max, data.s_nbins,
  //                    fAxis == kYStripProj ?   y : z  );
  //bin_p = findbin(data.p_min, data.p_max, data.p_nbins,
  //                    fAxis == kYStripProj ?   z : y  );
   // Warning! Be careful with ?over-range? bins when using offsets :
  Etot += edep;
  G4int si = findbin(
    data.s_min,
    data.s_bin(),
    r[IndexOfStripProj(acrossaxis)]
  );
  if (0 <= si and si < data.s_nbins)
    stripEtot->at(si) += edep;

  if (!hassignal or t < earliest)  earliest = t;
  if (!hassignal or t > latest  )  latest   = t;
  hassignal = true;
  
  ScintStripIndex i = data.FindIndex(r, t);
  G4int& bin_s = i.s;  G4int& bin_p = i.p;  G4int& bin_t = i.t;
  #ifdef DEBUG
    std::cerr << "Name: " << GetName() << "\n";
    std::cerr << "Time: " << t/ns << " ns\n";
    std::cerr << "Position: " << r/mm << " mm\n";
    std::cerr << "Index: s = " << bin_s << ", p = " << bin_p << ", t = " << bin_t << "\n";
    std::cerr << "S-axis: " << _EStripProjSimple[data.acrossproj] << "\n";
    std::cerr << "P-axis: " << _EStripProjSimple[data.alongproj] << "\n";
    std::cerr << "Range:\n";
    std::cerr << "        s = " << data.s_min/mm << " .. " << data.s_max/mm << " mm, " << data.s_nbins << " bins\n";
    std::cerr << "        p = " << data.p_min/mm << " .. " << data.p_max/mm << " mm, " << data.p_nbins << " bins\n";
    std::cerr << "        t = " << data.t_min/ns << " .. " << data.t_max/ns << " ns, " << data.t_nbins << " bins\n";
    std::cerr << std::endl;
  #endif
  ensure( bin_s >= 0 and bin_s < data.s_nbins );
  ensure( bin_p >= 0 and bin_p < data.p_nbins );
  ensure( bin_t >= 0 and bin_t < data.t_nbins );
  data[i].edep += edep;
  
  return true;
}

void SensitiveScintStrip::EndOfEvent(G4HCofThisEvent*)
{
  if (verboseLevel>0) {
    G4int nhits = hitsCollection->entries();
    
    FOR (i, 0, nhits-1) (*hitsCollection)[i]->Print();
  };
  
  
  
  /*if(fAxis & kYStripProj)
    FORs(i, 0, fNstripsY)
      etotY[i] = 0;
  if(fAxis & kZStripProj)
    FORs(i, 0, fNstripsZ)
      etotZ[i] = 0;*/
}


void SensitiveScintStrip::SetSOffset(const G4double& o)
{
  SetStripOffset(o);
  G4Box* box = (G4Box*) fPV->GetLogicalVolume()->GetSolid();
  //G4double p_min, p_max, s_min, s_max, s_nbins;
  switch (acrossaxis) {
    case kXStripProj:
      data.s_min = fPV->GetTranslation().x() - box->GetXHalfLength() + o;
      data.s_max = fPV->GetTranslation().x() + box->GetXHalfLength() + o;
    break;
    case kYStripProj:
      data.s_min = fPV->GetTranslation().y() - box->GetYHalfLength() + o;
      data.s_max = fPV->GetTranslation().y() + box->GetYHalfLength() + o;
    break;
    case kZStripProj:
      data.s_min = fPV->GetTranslation().z() - box->GetZHalfLength() + o;
      data.s_max = fPV->GetTranslation().z() + box->GetZHalfLength() + o;
    break;
    default:
      // Must not have been be here!
      break;
  }

}

