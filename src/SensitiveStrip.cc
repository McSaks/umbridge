#include "SensitiveStrip.hh"
#include "globals.hh"
#include "G4HCofThisEvent.hh"
#include "G4Step.hh"
#include "G4ThreeVector.hh"
#include "G4SDManager.hh"
#include "G4ios.hh"
#include "Parameterisation.hh"
#include "G4PVParameterised.hh"
#include "G4Box.hh"
#include "G4VPVParameterisation.hh"
#include "G4VSolid.hh"
#include "FOR.hh"

class Parameterisation;
class G4VPVParameterisation;
class G4PVParameterised;
class G4VSolid;
class G4Box;

SensitiveStrip::SensitiveStrip(G4String name, DetectorConstruction* aDet, G4VPhysicalVolume* pv,
       PositionSystem* tr, G4int nstripsY, G4int nstripsZ, G4int dP, G4int copyNo,
       EDetectorType type /* = kOtherDetector*/, EStripProj ax /* = kNoStripProj */, EStripProj bx /* = kNoStripProj */,
       G4double spreading, G4bool inherited)
:SensitiveDetector(name)
,posSystem(tr)
,hitsCollection(NULL)
,acrossaxis(ax)
,alongaxis(bx)
,fNstripsY(nstripsY)
,fNstripsZ(nstripsZ)
,stripspreading(spreading)
,stripoffset(0)
{
  detConstr = aDet;
  SDtype = type;
  fPV = pv;
  detPart = dP;
  fCopyNo = copyNo;
  unless(inherited) G4cout << "Sensitive " << SensitiveDetectorName << " constructed" << G4endl;
  G4String tmp = name(1,1);
  tmp.toUpper();
  collectionName.insert(
    G4String("hitsCollection") + tmp + G4String( name(2, name.size() - 2) )
  );
  IF ( fPV->IsParameterised() )
    Parameterisation* parampampam = (Parameterisation*) ((G4PVParameterised*)fPV)->GetParameterisation();
    fpos  = parampampam->GetPosition(fCopyNo);
    fdims = parampampam->GetDimentions(fCopyNo);
  ELSE
    fpos  = fPV->GetTranslation();
    G4Box* box = (G4Box*) fPV->GetLogicalVolume()->GetSolid();
    fdims.set(box->GetXHalfLength(),box->GetYHalfLength(),box->GetZHalfLength());
  END_IF
  if(acrossaxis & kXStripProj)
    etotX.assign(fNstripsX, 0);
  if(acrossaxis & kYStripProj)
    etotY.assign(fNstripsY, 0);
  if(acrossaxis & kZStripProj)
    etotZ.assign(fNstripsZ, 0);
}


/*SensitiveStrip::SensitiveStrip(G4String name, DetectorConstruction* aDet, G4int nstrips,
                               G4ThreeVector pos, G4ThreeVector dims, EAxis ax = kYAxis)
:G4VSensitiveDetector(name)
,fNstrips(nstrips)
,detConstr(aDet)
,fAxis(ax)
,fpos(pos)
,fdims(dims)
{
  collectionName.insert("hitsCollection"+name(1,1).ToUpper()+name(2,name.size()-2));
}*/

SensitiveStrip::~SensitiveStrip() { }

void SensitiveStrip::Initialize(G4HCofThisEvent* HCE)
{
  hitsCollection = new HitsCollectionStrip( SensitiveDetectorName,collectionName[0] );
  static G4int HCID = -1;
  if(HCID<0)
  { HCID = G4SDManager::GetSDMpointer()->GetCollectionID(collectionName[0]); }
  HCE->AddHitsCollection( HCID, hitsCollection );
}

G4double SensitiveStrip::GetStripPosY(const G4int& stripNo)
{
  return fpos.y() - fdims.y() + fdims.y()/fNstripsY * (2*stripNo + 1) + stripoffset;
}
G4double SensitiveStrip::GetStripPosZ(const G4int& stripNo)
{
  return fpos.z() - fdims.z() + fdims.z()/fNstripsZ * (2*stripNo + 1) + stripoffset;
}

G4ThreeVector SensitiveStrip::GetStripPos(StripIndex stripNo)
{
  G4ThreeVector strippos(fpos.x(), 0, 0);
  
  if(acrossaxis & kYStripProj)
    strippos.setY(fpos.y() - fdims.y() + fdims.y()/fNstripsY * (2*stripNo.y + 1) + stripoffset);
  else
    strippos.setY(fpos.y());
  
  if(acrossaxis & kZStripProj)
    strippos.setZ(fpos.z() - fdims.z() + fdims.z()/fNstripsZ * (2*stripNo.z + 1) + stripoffset);
  else
    strippos.setZ(fpos.z());
  
  return strippos;
}

G4ThreeVector SensitiveStrip::GetStripPos(G4int ystripNo, G4int zstripNo)
{
  return GetStripPos(StripIndex(-1, ystripNo, zstripNo));
}

G4ThreeVector SensitiveStrip::GetStripPos(G4int stripNo, EStripProj proj)
{
  G4ThreeVector strippos(fpos.x(), 0, 0);
  
  if(proj != kYStripProj && proj != kZStripProj)
    G4Exception("SensitiveStrip::GetStripPos", "Fatal error in Argument", FatalErrorInArgument, "Ambiguous. Please, specify Y or Z direction");
  
  if(proj & kYStripProj)
    strippos.setY(fpos.y() - fdims.y() + fdims.y()/fNstripsY * (2*stripNo + 1) + stripoffset);
  else
    strippos.setY(fpos.y());
  
  if(proj & kZStripProj)
    strippos.setZ(fpos.z() - fdims.z() + fdims.z()/fNstripsZ * (2*stripNo + 1) + stripoffset);
  else
    strippos.setZ(fpos.z());
  
  return strippos;
}

G4ThreeVector SensitiveStrip::GetStripDims(EStripProj proj)
{
  G4ThreeVector stripdims(fdims.x(), fdims.y(), fdims.z());
  
  if(proj != kYStripProj && proj != kZStripProj)
    G4Exception("SensitiveStrip::GetStripPos", "Fatal error in Argument", FatalErrorInArgument, "Ambiguous. Please, specify Y or Z direction");
  
  if(proj & kYStripProj)
    stripdims.setY(stripdims.y()/fNstripsY);
  
  if(proj & kZStripProj)
    stripdims.setZ(stripdims.z()/fNstripsZ);
  
  return stripdims;
}

G4int SensitiveStrip::ComputeStripX(const G4double& position)
{
  G4double xmin = fpos.x() - fdims.x();
  G4double xmax = fpos.x() + fdims.x();
  if(xmin > position || position > xmax) return kOutOfVolume;
  G4int stripNum = G4int((position-stripoffset - xmin) / (xmax - xmin) * fNstripsX);
  if(stripNum == fNstripsX) stripNum = fNstripsX-1;
  return stripNum;
}
G4int SensitiveStrip::ComputeStripX(const G4ThreeVector& position)
{
  G4double xmin = fpos.x() - fdims.x();
  G4double xmax = fpos.x() + fdims.x();
  G4double ymin = fpos.y() - fdims.y();
  G4double ymax = fpos.y() + fdims.y();
  G4double zmin = fpos.z() - fdims.z();
  G4double zmax = fpos.z() + fdims.z();
  if(xmin > position.x() || position.x() > xmax) return kOutOfVolume;
  if(ymin > position.y() || position.y() > ymax) return kOutOfVolume;
  if(zmin > position.z() || position.z() > zmax) return kOutOfVolume;
  G4int stripNum = G4int((position.x()-stripoffset - xmin) / (xmax - xmin) * fNstripsX);
  if(stripNum == fNstripsX) stripNum = fNstripsX-1;
  return stripNum;
}

G4int SensitiveStrip::ComputeStripY(const G4double& position)
{
  G4double ymin = fpos.y() - fdims.y();
  G4double ymax = fpos.y() + fdims.y();
  if(ymin > position || position > ymax) return kOutOfVolume;
  G4int stripNum = G4int((position-stripoffset - ymin) / (ymax - ymin) * fNstripsY);
  if(stripNum == fNstripsY) stripNum = fNstripsY-1;
  return stripNum;
}
G4int SensitiveStrip::ComputeStripY(const G4ThreeVector& position)
{
  G4double xmin = fpos.x() - fdims.x();
  G4double xmax = fpos.x() + fdims.x();
  G4double ymin = fpos.y() - fdims.y();
  G4double ymax = fpos.y() + fdims.y();
  G4double zmin = fpos.z() - fdims.z();
  G4double zmax = fpos.z() + fdims.z();
  if(xmin > position.x() || position.x() > xmax) return kOutOfVolume;
  if(ymin > position.y() || position.y() > ymax) return kOutOfVolume;
  if(zmin > position.z() || position.z() > zmax) return kOutOfVolume;
  G4int stripNum = G4int((position.y()-stripoffset - ymin) / (ymax - ymin) * fNstripsY);
  if(stripNum == fNstripsY) stripNum = fNstripsY-1;
  return stripNum;
}

G4int SensitiveStrip::ComputeStripZ(const G4double& position)
{
  G4double zmin = fpos.z() - fdims.z();
  G4double zmax = fpos.z() + fdims.z();
  if(zmin > position || position > zmax) return kOutOfVolume;
  G4int stripNum = G4int((position-stripoffset - zmin) / (zmax - zmin) * fNstripsZ);
  if(stripNum == fNstripsZ) stripNum = fNstripsZ-1;
  return stripNum;
}
G4int SensitiveStrip::ComputeStripZ(const G4ThreeVector& position)
{
  G4double xmin = fpos.x() - fdims.x();
  G4double xmax = fpos.x() + fdims.x();
  G4double ymin = fpos.y() - fdims.y();
  G4double ymax = fpos.y() + fdims.y();
  G4double zmin = fpos.z() - fdims.z();
  G4double zmax = fpos.z() + fdims.z();
  if(xmin > position.x() || position.x() > xmax) return kOutOfVolume;
  if(ymin > position.y() || position.y() > ymax) return kOutOfVolume;
  if(zmin > position.z() || position.z() > zmax) return kOutOfVolume;
  G4int stripNum = G4int((position.z()-stripoffset - zmin) / (zmax - zmin) * fNstripsZ);
  if(stripNum == fNstripsZ) stripNum = fNstripsZ-1;
  return stripNum;
}

StripIndex SensitiveStrip::ComputeStrip(G4ThreeVector& position)
{
  if(acrossaxis == kXStripProj)
     return StripIndex(ComputeStripX(position), kUnsensitive, kUnsensitive);
  if(acrossaxis == kYStripProj)
     return StripIndex(kUnsensitive, ComputeStripY(position), kUnsensitive);
  if(acrossaxis == kZStripProj)
     return StripIndex(kUnsensitive, kUnsensitive, ComputeStripZ(position));
  if(acrossaxis == kBothStripProj)
     return StripIndex( kUnsensitive, ComputeStripY(position), ComputeStripZ(position) );
  if(acrossaxis == kNoStripProj)
     return StripIndex(kUnsensitive);
  G4Exception("SensitiveStrip::GetStripPos", "Fatal error in Argument", FatalErrorInArgument, "Strips can only have X, Y or Z direction.");
  return StripIndex(kErrorInComputing);
}

G4bool SensitiveStrip::ProcessHits(G4Step* aStep, G4TouchableHistory*)
{
  G4double edep = aStep->GetTotalEnergyDeposit();
  if(edep==0.) return false;
  G4ThreeVector hitpos = aStep->GetPreStepPoint()->GetPosition();
  HitStrip* newHit = new HitStrip();
  newHit->SetTrackID(aStep->GetTrack()->GetTrackID());
  newHit->SetPos(hitpos);
  newHit->SetEdep(edep);
  StripIndex stripNum = ComputeStrip(hitpos);
  if(verboseLevel>1) {
  G4cout << "Hit in " << SensitiveDetectorName << ";\tenergy: " << edep
         << ";\tposition: {" << hitpos.x()/cm << ", "
         << hitpos.y()/cm << ", " << hitpos.z()/cm << "} cm;\tstripY#: " << stripNum.y
         <<  G4endl;
  };
  if((acrossaxis & kYStripProj) && stripNum.y < 0) return false;
  if((acrossaxis & kZStripProj) && stripNum.z < 0) return false;
  newHit->SetStripNum(stripNum);
  newHit->SetEdep(edep);
  newHit->SetPos(aStep->GetPostStepPoint()->GetPosition());
  hitsCollection->insert(newHit);
  
  IF ( !UseSpreading() )
    if(acrossaxis & kYStripProj)
      etotY[stripNum.y] += edep;
    if(acrossaxis & kZStripProj)
      etotZ[stripNum.z] += edep;
  ELSE
    G4double min, max, a, b;
    G4double stripwidth, hit;
    size_t minstrip, maxstrip;
    IF (acrossaxis & kYStripProj)
      stripwidth = fdims.y()/fNstripsY; hit = hitpos.y();
      min = hit - 3*stripspreading;  max = hit + 3*stripspreading;
      minstrip = ComputeStripY(min); maxstrip = ComputeStripY(max);
      //DispNTimes(tag_SDname, 20, SensitiveDetectorName, "-\n");
      //DispNTimes(tag_minstrip, 20, minstrip, "minstrip = ");
      //DispNTimes(tag_maxstrip, 20, maxstrip, "maxstrip = ");
      a = GetStripPosY(minstrip)-stripwidth;
      FOR(i, minstrip, maxstrip) {
        b = GetStripPosY(i)+stripwidth;
        if(i>=0 && i<fNstripsY)
          etotY[i] += edep * ComputeChargeBetween(hit, a, b);
        a = b;
      }
    END_IF
    IF (acrossaxis & kZStripProj)
      stripwidth = fdims.z()/fNstripsZ; hit = hitpos.z();
      min = hit - 3*stripspreading;  max = hit + 3*stripspreading;
      minstrip = ComputeStripZ(min); maxstrip = ComputeStripZ(max);
      a = GetStripPosZ(minstrip)-stripwidth;
      FOR(i, minstrip, maxstrip) {
        b = GetStripPosZ(i)+stripwidth;
        if(i>=0 && i<fNstripsZ)
          etotZ[i] += edep * ComputeChargeBetween(hit, a, b);
        a = b;
      }
    END_IF
  END ( if !UseSpreading() )
    
  //newHit->Print();
  //newHit->Draw();

  return true;
}

void SensitiveStrip::EndOfEvent(G4HCofThisEvent*)
{
  IF (verboseLevel > 0)
     G4int nhits = hitsCollection->entries();
     G4String axis;
     switch (acrossaxis) {
       CASE kXStripProj    : axis = "X-";
       CASE kYStripProj    : axis = "Y-";
       CASE kZStripProj    : axis = "Z-";
       CASE kBothStripProj : axis = "";
       DEFAULT: axis = "somewhat ";
     };
     G4cout << "\n-------->Hits Collection: in this event they are " << nhits
            << " hit" << ((nhits-1)?"s":"") << " in the " << axis
            << "plane of the detector " << SensitiveDetectorName << (nhits?": ":".") << G4endl;
     FORs(i, 0, nhits) (*hitsCollection)[i]->Print();
  END_IF
  
  TrackPoint tp;
  if(posSystem) if(acrossaxis & kXStripProj)
    FORs(i, 0, fNstripsX)
    {
      tp.stripNo = i;
      tp.point  = GetStripPos(i, tp.proj = kXStripProj);
      tp.last = i == fNstripsX-1;
      tp.edep = etotX[i];
      tp.weight = ComputeWeight(tp, etotX[i]);
      tp.enabled = etotX[i] > 1*keV;
      tp.SDname = SensitiveDetectorName;
      tp.SDtype = SDtype;
      tp.detPart = detPart;
      tp.detLayer = fCopyNo;
      posSystem->Append(tp);
    };
  if(posSystem) if(acrossaxis & kYStripProj)
    FORs(i, 0, fNstripsY)
    {
      tp.stripNo = i;
      tp.point  = GetStripPos(i, tp.proj = kYStripProj);
      tp.last = i == fNstripsY-1;
      tp.edep = etotY[i];
      tp.weight = ComputeWeight(tp, etotY[i]);
      tp.enabled = etotY[i] > 1*keV;
      tp.SDname = SensitiveDetectorName;
      tp.SDtype = SDtype;
      tp.detPart = detPart;
      tp.detLayer = fCopyNo;
      posSystem->Append(tp);
    };
  if(posSystem) if(acrossaxis & kZStripProj)
    FORs(i, 0, fNstripsZ)
    {
      tp.stripNo = i;
      tp.point  = GetStripPos(i, tp.proj = kZStripProj);
      tp.last = i == fNstripsZ-1;
      tp.edep = etotZ[i];
      tp.weight = ComputeWeight(tp, etotZ[i]);
      tp.enabled = etotZ[i] > 1*keV;
      tp.SDname = SensitiveDetectorName;
      tp.SDtype = SDtype;
      tp.detPart = detPart;
      tp.detLayer = fCopyNo;
      posSystem->Append(tp);
    };
  
  #ifdef DEBUG
  if(fAxis & kYStripProj) {
    G4cout << /*"\n" << SensitiveDetectorName << ":" <<*/ G4endl;
    FORs(i, 0, fNstripsY)
     if(etotY[i])
      G4cout << "  (" << GetStripPos(i,kYStripProj).x() << ", " << GetStripPos(i,kYStripProj).y() << ", " << etotY[i] << ")";
    G4cout << G4endl;
  };
  #endif
  
  if(acrossaxis & kYStripProj)
    FORs(i, 0, fNstripsY)
      etotY[i] = 0;
  if(acrossaxis & kZStripProj)
    FORs(i, 0, fNstripsZ)
      etotZ[i] = 0;
}



G4double SensitiveStrip::ComputeWeight(const TrackPoint& pt, G4double edep)
{
  //if(edep < 10*keV) return 0;
  G4double w;
//  G4String namebeg(pt.SDname);
//  namebeg = namebeg(0,3);
  w = edep/MeV;//w = pow(edep/MeV, .33333);
  w += pt.point.x()*0;
//  if(pt.SDtype == kCalorimeter ,0)
//    w /= (pt.point.x() - detConstr->CC1_pos.x() + 1.1*detConstr->CC1_dims.x()) / (1*cm);
  return w;
}


#include "CLHEP/Units/PhysicalConstants.h"
//#include "CLHEP/GenericFunctions/Exp.hh"
//#include "CLHEP/GenericFunctions/Sqrt.hh"
#include <cmath>
G4double SensitiveStrip::ComputeChargeDensity(G4double hit, G4double pos)
{
  using CLHEP::twopi;
  //static const Genfun::Exp exp;
  //static const Genfun::Sqrt sqrt;
  G4double& sigma = stripspreading;
  G4double par = pos - hit;
  par /= sigma;
  par *= par;
  return exp(-par/2)/sqrt(twopi)/sigma;
}

//#include "CLHEP/GenericFunctions/Erf.hh"
G4double SensitiveStrip::ComputeChargeBetween(G4double hit, G4double a, G4double b)
{
  //static const Genfun::Erf erf;
  G4double& sigma = stripspreading;
  static const G4double sqrt2 = 1.41421356237309504880168872421;
  G4double para = a - hit;  para /= sigma;  para = erf(para/sqrt2);
  G4double parb = b - hit;  parb /= sigma;  parb = erf(parb/sqrt2);
  return ( parb - para ) / 2;
}
