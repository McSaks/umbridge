#include <iomanip>

#include "G4Polyline.hh"
#include "G4Circle.hh"
#include "G4VisAttributes.hh"
#include "G4VVisManager.hh"

#include "DetectorConstruction.hh"
#include "PositionSystem.hh"
#include "EventAction.hh"
#include "EventData.hh"
#include "FOR.hh"


using namespace std;


void TrackPoint::Draw()
{
  G4VVisManager* pVVisManager = G4VVisManager::GetConcreteInstance();
  if(pVVisManager) {
    G4Circle circle(point);
    G4VisAttributes attr(G4Color::White());
    circle.SetScreenSize(3.);
    circle.SetFillStyle(G4Circle::filled);
    circle.SetVisAttributes(attr);
    pVVisManager->Draw(circle);
  };
}

ostream& operator<<(ostream& stream, const TrackPoint& tp)
{
  if(!tp.enabled) return stream;
  G4String str = tp.SDname;
  G4String del = "LayerSi";
  size_t pos;
  while((pos=str.find(del))!=G4String::npos)
  stream << str.replace(pos,del.size(),"")<< " " ;
//stream << _EStripProj[tp.proj]  << " " ;
//  stream << tp.proj               << " " ;
  stream << tp.stripNo            << " " ;
//stream << tp.enabled            << " " ;
  stream << tp.point.x()/cm       << " " ;
  if(tp.proj&kYStripProj)
    stream << tp.point.y()/cm       << " " ;
  if(tp.proj&kZStripProj)
    stream << tp.point.z()/cm       << " " ;
  stream << tp.edep/MeV           << " " ;
  stream << tp.weight             << "\n";
  return stream;
}


PositionSystem::PositionSystem()
  : trackPoints(), detector(NULL), messenger(NULL), currentEvent(NULL), data(NULL)
{
  
}

PositionSystem::~PositionSystem()
{
  
}

void PositionSystem::SetMessenger(DetectorMessenger* mess)
{
  messenger = mess;
  mess->SetPositionSystem(this);
}

void PositionSystem::Append(TrackPoint pt)
{
  trackPoints.push_back(pt);
}

void PositionSystem::Join(const PositionSystem* tr)
{
  uFORs(i, 0, tr->trackPoints.size())
    trackPoints.push_back(tr->trackPoints.at(i));
}

size_t PositionSystem::NOnPoints() const
{
  size_t size = 0;
  if(!trackPoints.size()) return 0;
  vFORc(TrackPoint, trackPoints, it)
    if( it->enabled )
      size++;
  return size;
}

/*size_t PositionSystem::NOnPoints() const
{G4cerr << "size_t PositionSystem::NOnPoints() const\n{\n" << G4endl;
  G4cerr << "  size_t size = 0;" << G4endl;
  size_t size = 0;
  G4cerr << "  if(!trackPoints.size()) return 0;" << G4endl;
  if(!trackPoints.size()) return 0;
  G4cerr << "  uFOR(i, 0, trackPoints.size()-1)" << G4endl;
  G4cerr << "  // trackPoints.size() == " << trackPoints.size() << G4endl;
  uFOR(i, 0, trackPoints.size()-1) {
    G4cerr << "    if( trackPoints.at(i).enabled )" << G4endl;
    if( trackPoints.at(i).enabled ) {
      G4cerr << "      size++;" << G4endl;
      size++;
    }
  };
 G4cerr << "  return size;\n}" << G4endl;
  return size;
}*/

G4bool PositionSystem::Trigger() const
{
  TrackPoint tp;
  G4bool hitted1Y = false, hitted2Y = false, hitted1Z = false, hitted2Z = false;
  //G4cerr << "\n"; uFORs(i, 0, trackPoints.size()) { G4cerr << _EDetectorType[trackPoints.at(i).SDtype] << " " << trackPoints.at(i).enabled << " | "; }; G4cerr << G4endl;
  const G4int lastlayer = detector->CLayerSiY.nlayers-1;
  uFORs(i, 0, trackPoints.size()) {
    tp = trackPoints.at(i);
    if(tp.SDtype != kConverter)
      break;
    if(tp.enabled  &&  tp.detPart == 2) {
      if(tp.proj & kYStripProj) {
        if(tp.detLayer == lastlayer-1) hitted1Y = true;
        if(tp.detLayer == lastlayer) hitted2Y = true;
      };
      if(tp.proj & kZStripProj) {
        if(tp.detLayer == lastlayer-1) hitted1Z = true;
        if(tp.detLayer == lastlayer) hitted2Z = true;
      };
    };
  };
  return hitted1Y and hitted2Y and hitted1Z and hitted2Z;
}



G4bool PositionSystem::Write() const
{
  
  if(!data->ok()) return false;
  
  if(data->write_trackPoints)
    data->positionData.trackPoints = trackPoints;
  
  if(data->write_structured) {
    vFORc(TrackPoint, trackPoints, it) if(it->enabled) {
      switch(it->SDtype) {
        case kConverter:
          switch(it->proj) {
            case kYStripProj: data->positionData.C[it->detLayer].Y.push_back(*it); break;
            case kZStripProj: data->positionData.C[it->detLayer].Z.push_back(*it); break;
            default: { };
          };
          break;
        case kMidi:
          switch(it->proj) {
            case kYStripProj: data->positionData.CDmidi[it->detLayer].Y.push_back(*it); break;
            case kZStripProj: data->positionData.CDmidi[it->detLayer].Z.push_back(*it); break;
            default: { };
          };
          break;
        case kCalorimeter:
          switch(it->proj) {
            case kYStripProj: data->positionData.CC1[it->detLayer].Y.push_back(*it); break;
            case kZStripProj: data->positionData.CC1[it->detLayer].Z.push_back(*it); break;
            default: { };
          };
          break;
        default: { };
      };
    };
  };
  
  return true;
}
