// Read an INI file into easy-to-access name/value pairs.

#include <algorithm>
#include <cctype>
#include <cstdlib>
#include "ini.h"
#include "INIReader.h"
//#include <set>

#include <iostream>
using std::string;
using std::cout;
using std::cerr;
using std::endl;

INIReader::INIReader(const string& filename)
    : _error(-1)
    , constants_section ("constants")
    , file_name (filename)
    , allow_default (false)
{ }

bool INIReader::Parse()
{
    _error = ini_parse(file_name.c_str(), ValueHandler, this);
    /*for ( std::map<string, string>::const_iterator i = _values.begin(); i != _values.end(); ++i )
        cerr << i->first << " : " << i->second << endl;*/
    return (_error);
}

int INIReader::ParseError()
{
    return _error;
}

string INIReader::Get(const string& section, const string& name, const string& default_value, int depth)
{
    string key = MakeKey(section, name);
    if (!_values.count(key)) {
      cerr << "\n"
        "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n"
        "!!                         !!\n"
        "!!         Warning         !!\n"
        "!!                         !!\n"
        "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n"
        "!!\n"
        "!!  Cannot find key\n"
        "!!    ‘[" << section << "]." << name << "’\n"
        "!!  in file\n"
        "!!    ‘" << file_name << "’.\n"
        "!!  Using default value.\n"
        "!!\n"
        "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n"
        << endl;
      if (!allow_default) throw nodefault_exception("[" + section + "]." + name);
      return default_value;
    }
    string value = _values[key];
    string replace_name, replace_val, new_section;
    size_t buck, right_del, dot;
    bool brace, section_specified = false;
    //std::set<size_t> ignore;
    while ( (buck = value.find_first_of('$')) != string::npos ) {
        if (buck == value.size() - 1)
            break;
        if (( brace = value.at(buck + 1) == '{' )) {
            right_del = value.find_first_of('}', buck);
        } else {
            right_del = value.find_first_of(" \t", buck);
        }
        replace_name = value.substr(buck + (brace ? 2 : 1), right_del - buck - (brace ? 2 : 1));
        if ( (dot = replace_name.find_first_of('.')) != string::npos ) {
            new_section = replace_name.substr(0, dot);
            replace_name = replace_name.substr(dot + 1, string::npos);
            section_specified = true;
        } else section_specified = false;
        replace_val = Get(section_specified ? new_section : constants_section, replace_name, "\04", depth + 1);
        //std::cerr << "brace: " << brace << ", $" << replace_name << " = " << replace_val << std::endl;
        if (replace_val == "\04")
            value.replace(buck, 1, "\04");
        else
            value.replace(buck, right_del - buck - (brace ? -1 : 0), replace_val);
    }
    while ( (buck = value.find_first_of('\04')) != string::npos ) {
        value.replace(buck, 1, "$");
    }
    return value;
}

long INIReader::GetInteger(const string& section, const string& name, long default_value)
{
    string valstr = Get(section, name, "");
    const char* value = valstr.c_str();
    char* end;
    // This parses "1234" (decimal) and also "0x4D2" (hex)
    long n = strtol(value, &end, 0);
    return end > value ? n : default_value;
}

double INIReader::GetReal(const string& section, const string& name, double default_value)
{
    string valstr = Get(section, name, "");
    const char* value = valstr.c_str();
    char* end;
    double n = strtod(value, &end);
    return end > value ? n : default_value;
}

bool INIReader::GetBoolean(const string& section, const string& name, bool default_value)
{
    string valstr = Get(section, name, "");
    // Convert to lower case to make string comparisons case-insensitive
    std::transform(valstr.begin(), valstr.end(), valstr.begin(), ::tolower);
    if (valstr == "true" || valstr == "yes" || valstr == "on" || valstr == "1")
        return true;
    else if (valstr == "false" || valstr == "no" || valstr == "off" || valstr == "0")
        return false;
    else
        return default_value;
}

string INIReader::MakeKey(const string& section, const string& name)
{
    string key = section + "." + name;
    // cerr<<key<<endl;
    // Convert to lower case to make section/name lookups case-insensitive
    std::transform(key.begin(), key.end(), key.begin(), ::tolower);
    return key;
}

int INIReader::ValueHandler(void* user, const char* section, const char* name,
                            const char* value)
{
    INIReader* reader = (INIReader*)user;
    string key = MakeKey(section, name);
    if (reader->_values[key].size() > 0)
        reader->_values[key] += "\n";
    reader->_values[key] += value;
    return 1;
}
