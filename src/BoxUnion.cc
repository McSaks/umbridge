#include "BoxUnion.hh"

#include "G4Box.hh"
#include "G4UnionSolid.hh"
#include "G4IntersectionSolid.hh"
#include "G4SubtractionSolid.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4Material.hh"
#include "G4VSensitiveDetector.hh"
#include "G4PVPlacement.hh"
#include "FOR.hh"


#if 0
BoxUnion::BoxUnion(const G4String& name_)
: name(name_)
, copyNo(0)
, solid(NULL)
{ }

BoxUnion::BoxUnion(G4VSolid& s)
: name(s.GetName())
, copyNo(1)
, solid(&s)
{ }

BoxUnion::BoxUnion(G4VSolid* s)
: name(s->GetName())
, copyNo(1)
, solid(s)
{ }

BoxUnion::~BoxUnion()
{
  vFOR(G4VSolid*, solids, item)
    {delete0(*item);}
  solids.clear();
}

//BoxUnion BoxUnion::operator|(G4VSolid* that) const
//{
//  //return G4UnionSolid(this->);
//}

BoxUnion& BoxUnion::operator|=(G4VSolid* that)
{
  if(!solid)
    solid = that;
  else
    solids.push_back(that),
    solid = new G4UnionSolid(UniquifyName(), solid, that, NULL, translation);
  solids.push_back(solid);
  ++copyNo;
  return *this;
}

BoxUnion& BoxUnion::operator&=(G4VSolid* that)
{
  if(!solid)
    solid = that;
  else
    solids.push_back(that),
    solid = new G4IntersectionSolid(UniquifyName(), solid, that, NULL, translation);
  solids.push_back(solid);
  ++copyNo;
  return *this;
}

BoxUnion& BoxUnion::operator-=(G4VSolid* that)
{
  if(!solid)
    solid = that;
  else
    solids.push_back(that),
    solid = new G4SubtractionSolid(UniquifyName(), solid, that, NULL, translation);
  solids.push_back(solid);
  ++copyNo;
  return *this;
}

G4String BoxUnion::UniquifyName()
{
  std::stringstream str;
  str << name << '#';
  str.width(3), str.fill('0');
  str << copyNo;
  return str.str();
}

// Translate3D(G4ThreeVector(x,y,z))
#endif

BoxUnionVolume::BoxUnionVolume()
 : mother(0), solid(0), logic(0), mater(0), sensitive(0), copyNo(0) { }
BoxUnionVolume::~BoxUnionVolume() { }

void BoxUnionVolume::CreateVolume(G4bool multiple)
{
 //if(dims.mag2()) {
 if(operator!()) return;
 
 if(multiple) {
// return;
  G4String name_i;
  G4ThreeVector v;
  BoxVolume* vol = NULL;
  volumes.reserve(posdims.size());
  vFORc(PosDims, posdims, i) {
   if( i->dims.x() && i->dims.y() && i->dims.z() ) {} else continue;
   name_i = UniquifyName();
   if(i->dims.x() > 0) {
     v = i->pos;
     //volumes.push_back(vol = BoxVolume());
     vol = new BoxVolume;
     vol->pos = v, vol->dims = G4ThreeVector(i->dims.x(), i->dims.y(), i->dims.z());
     vol->solid = new G4Box(name_i, i->dims.x(), i->dims.y(), i->dims.z());
     vol->logic = new G4LogicalVolume(vol->solid, // its solid
                                     mater,      // its material
                                     name_i,    // its name
                                     NULL,     // field
                                     NULL,    // sensitive detector
                                     NULL);  // user limits
                                            //
     vol->physic = new G4PVPlacement(NULL, // no rotation
                        pos + i->pos,     // its position
                        vol->logic,      // its logical volume
                        name_i,         // its name
       mother ? mother->logic : NULL,  // its mother volume
                        false,        // no boolean operations
                        0);          // copy number
     
     volumes.push_back(*vol);
   } else if(i->dims.x() < 0) {
     volumes.back().solid = new G4SubtractionSolid(name_i, volumes.back().solid,
                                  new G4Box(UniquifyName(), -i->dims.x(),-i->dims.y(),-i->dims.z()), NULL, i->pos-v);
     vol->logic->SetSolid(vol->solid);
   }
  }
  /* _PLACEHOLDER_ */
 } else { 
  size_t size = posdims.size();
  voidensure(size > 0);
  
  //const G4ThreeVector& first_pos = posdims.front().pos;
  const G4ThreeVector& first_dims = posdims.front().dims;
  
  //solid.SetName(name);
  // First:
  /*if(first_dims.x() > 0)
    solid |= new G4Box(name,  first_dims.x(),  first_dims.y(),  first_dims.z());
  else
    solid -= new G4Box(name, -first_dims.x(), -first_dims.y(), -first_dims.z());
  // Rest:
  if(size > 1)
  vFORc(PosDims, posdims, i) {
    if(i->dims.x() && i->dims.y() && i->dims.z()) {
      //solid.AddTranslation(i->pos - (i-1)->pos);
      solid.SetTranslation(i->pos - first_pos);
      if(i->dims.x() > 0)
        solid |= new G4Box(name,  i->dims.x(),  i->dims.y(),  i->dims.z());
      else
        solid -= new G4Box(name, -i->dims.x(), -i->dims.y(), -i->dims.z());
    }
  }*/
  if(!solid)  solid = new G4Box(name, first_dims.x(),  first_dims.y(),  first_dims.z());
  
  logic = new G4LogicalVolume(solid,      // its solid
                              mater,     // its material
                              name,     // its name
                              NULL,    // field
                              NULL,   // sensitive detector
                              NULL); // user limits
  G4int conum = 0;
  
  vFORc(PosDims, posdims, pd) {
    physics.push_back( new G4PVPlacement(NULL, // no rotation
                             pos + pd->pos,   // its position
                             logic,          // its logical volume
                             name,          // its name
            mother ? mother->logic : NULL, // its mother volume
                             false,       // no boolean operations
                             conum)          // copy number  
    );
    conum++;
  }
 }
}

void BoxUnionVolume::CreateVolume(G4double& currentpos, const G4double& gap_after, G4bool multiple)
{
  SetPos(currentpos, gap_after);
  CreateVolume(multiple);
}


void BoxUnionVolume::SetPos(G4double& currentpos, const G4double& gap_after)
{
  pos.set(currentpos + dims.x(), 0, 0);
  currentpos += 2*dims.x() + gap_after;

}

void BoxUnionVolume::push(const G4ThreeVector& pos_, const G4ThreeVector& dims_)
{
  posdims.push_back( PosDims( pos_, dims_ ) );
}

G4String BoxUnionVolume::UniquifyName()
{
  std::stringstream str;
  str << name << '#';
  str.width(4), str.fill('0');
  str << ++copyNo;
  return str.str();
}

