#include "RunAction.hh"

#include "G4Run.hh"
#include "EventAction.hh"
#include "PrimaryGeneratorAction.hh"
#include <iomanip>
using namespace std;

#define BRIEF_RUN_DATA_FORMAT 1

RunAction::RunAction(EventAction* ev, G4String runfile)
: myEventAction(ev), gun(NULL), angres(0.), allCounter(0), goodCounter(0), effectiveCounter(0)
, rundatafile(runfile, std::ofstream::out|std::ofstream::app)
{ }

RunAction::~RunAction()
{
  rundatafile.close();
  G4cout << "RunAction deleted." << G4endl;
}

void RunAction::BeginOfRunAction(const G4Run* aRun)
{
  goodCounter = allCounter = effectiveCounter = 0;
  G4cout << "### Run " << aRun->GetRunID() << " start." << G4endl;
}

void RunAction::EndOfRunAction(const G4Run* aRun)
{

  G4cout << "### Run " << aRun->GetRunID() << " finished." << G4endl;
  G4cout << goodCounter << " good event"
         << (goodCounter-1?"s":"") << G4endl;
  if(allCounter) {
    #ifndef BRIEF_RUN_DATA_FORMAT
      rundatafile << gun->GetParticleEnergy()/GeV << " GeV ";
      rundatafile << gun->GetParticleName() << ": ";
      rundatafile << goodCounter << "/" << effectiveCounter << G4endl;
    #else
      rundatafile << setw(20) << setprecision(10) << gun->GetParticleEnergy()/GeV << " ";
      rundatafile << setw(10) << goodCounter << " ";
      rundatafile << setw(10) << effectiveCounter  << G4endl;
    #endif
  }
}
