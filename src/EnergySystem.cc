#include <iomanip>

#include "G4Polyline.hh"
#include "G4Circle.hh"
#include "G4VisAttributes.hh"
#include "G4VVisManager.hh"

#include "DetectorConstruction.hh"
#include "EnergySystem.hh"
#include "PositionSystem.hh"
#include "EventAction.hh"
#include "EventData.hh"
#include "FOR.hh"


using namespace std;


EnergyPoint::EnergyPoint(const TrackPoint& p)
  : edep(p.edep), amplitude(p.weight), enabled(p.enabled)
  , SDname(p.SDname), SDtype(p.SDtype), detPart(p.detPart), detLayer(p.detLayer)
  { }


EnergySystem::EnergySystem()
  : detector(NULL), messenger(NULL), currentEvent(NULL), data(NULL), posSystem(NULL)
{
  
}

EnergySystem::~EnergySystem()
{
  
}

void EnergySystem::Append(const EnergyPoint& pt)
{
  points.push_back(pt);
}


G4bool EnergySystem::Write() const
{
  
  if(!data->ok()) return false;
  
  if(data->write_structured) {
    vFORc(EnergyPoint, points, it) {
      switch(it->SDtype) {
        case kToF: break;
          /*switch(it->detPart) {
            case 1:
              data->energyData.S1 = *it; break;
            case 2:
              data->energyData.S2 = *it; break;
            case 3:
              data->energyData.S3 = *it; break;
            case 4:
              data->energyData.S4 = *it; break;
          }; break; */
        case kCalorimeter:
          switch(it->detPart) {
            case 1:
              data->energyData.CC1[it->detLayer] = *it;
              break;
            case 2:
              data->energyData.CC2 = *it;
              break;
          }; break;
        default: { };
      };
    };
    
   #if 0
   if(data->write_position_energy) {
    vFOR(TrackPoint, posSystem->GetPoints(), it) {
      switch(it->SDtype) {
        case kConverter:
          data->energyData.C.push_back(*it); break;
        case kCalorimeter:
          switch(it->detPart) {
            case 1:
              data->energyData.CC1.push_back(*it); break;
          }; break;
        default: { };
      };
    };
   };
   #endif
  
  };
  
  return true;
}
