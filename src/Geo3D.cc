#include "Geo3D.hh"

G4int index(const Eaxis& a)
{
  switch(a) {
    case kXaxis: return G4ThreeVector::X;
    case kYaxis: return G4ThreeVector::Y;
    case kZaxis: return G4ThreeVector::Z;
    default    : return -1;
  }
}

G4ThreeVector Line3D::FindPoint(G4double x, const Eaxis& ax) const
{
  G4int ind = index(ax);
  if(!v[ind])
    {if(a[ind]==x) return a; else return Geo3D_fail();}
  return a + v * (x-a[ind]) / v[ind];
}

Cuboid3D::EType Cuboid3D::GetType() const
{
  EType type = kBox;
  if(d.x() <= 0) type &= ~kXPlane;
  if(d.y() <= 0) type &= ~kYPlane;
  if(d.z() <= 0) type &= ~kZPlane;
  return type;
}

G4bool operator&(const Line3D& l, const Cuboid3D& b)
{
  G4ThreeVector min = b.r - b.d , max = b.r + b.d , pt;
  Cuboid3D::EType type = b.GetType();
  
  // X:
  if( type & ~Cuboid3D::kXPlane & Cuboid3D::kPoint ) { } else {
    pt = l.FindPoint(min.x(), kXaxis);
    if( min.y() <= pt.y() && pt.y() <= max.y() && min.z() <= pt.z() && pt.z() <= max.z()) return true;
    pt = l.FindPoint(max.x(), kXaxis);
    if( min.y() <= pt.y() && pt.y() <= max.y() && min.z() <= pt.z() && pt.z() <= max.z()) return true;
  }
  // Y:
  if( type & ~Cuboid3D::kYPlane & Cuboid3D::kPoint ) { } else {
    pt = l.FindPoint(min.y(), kYaxis);
    if( min.z() <= pt.z() && pt.z() <= max.z() && min.x() <= pt.x() && pt.x() <= max.x()) return true;
    pt = l.FindPoint(max.y(), kYaxis);
    if( min.z() <= pt.z() && pt.z() <= max.z() && min.x() <= pt.x() && pt.x() <= max.x()) return true;
  }
  // Z:
  if( type & ~Cuboid3D::kZPlane & Cuboid3D::kPoint ) { } else {
    pt = l.FindPoint(min.z(), kZaxis);
    if( min.x() <= pt.x() && pt.x() <= max.x() && min.y() <= pt.y() && pt.y() <= max.y()) return true;
    pt = l.FindPoint(max.z(), kZaxis);
    if( min.x() <= pt.x() && pt.x() <= max.x() && min.y() <= pt.y() && pt.y() <= max.y()) return true;
  }
  
  return false;
}
