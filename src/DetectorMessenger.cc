
#include "DetectorMessenger.hh"
#include "Directions.hh"
#include "PrimaryGeneratorAction.hh"

#include "DetectorConstruction.hh"
#include "PositionSystem.hh"
#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithoutParameter.hh"

#include "G4ParticleGun.hh"
#include "G4UnitsTable.hh"

#include "globals.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UImanager.hh"
#include "Geo3D.hh"
#include "FOR.hh"
#include "TFormat.hh"
using namespace TFormat;



/*
       ,gggg,
     ,88"""Y8b,                                    I8                                     I8
    d8"     `Y8                                    I8                                     I8
   d8'   8b  d8                                 88888888                               88888888
  ,8I    "Y88P'                                    I8                                     I8
  I8'            ,ggggg,  ,ggg,,ggg,     ,g,      I8    ,gggggg,  gg      gg    ,gggg,   I8     ,ggggg,  ,gggggg,
  d8            dP"  "Y8gg8" "8P" "8,   ,8'8,     I8    dP""""8I  I8      8I   dP"  "Yb  I8    dP"  "Y8ggdP""""8I
  Y8,          i8'    ,8I     8I   8I  ,8'  Yb   ,I8,  ,8'    8I  I8,    ,8I  i8'       ,I8,  i8'    ,8I       8I
  `Yba,,_____,,d8,   ,d8'     8I   Yb,,8'_   8) ,d88b,,dP     Y8,,d8b,  ,d8b,,d8,_    _,d88b,,d8,   ,d8'       Y8,
    `"Y8888888P"Y8888P"       8I   `Y8P' "YY8P8P8P""Y88P      `Y88P'"Y88P"`Y8P""Y8888PP8P""Y8P"Y8888P"         `Y8

*/


DetectorMessenger::DetectorMessenger(DetectorConstruction* myDet, PositionSystem* myRec)
  : myDetector(myDet), myReconstruction(myRec), gun(NULL)
{
  G4String unit_candidates = "pc km m cm mm um nm Ang fm parsec kilometer meter centimeter";
  G4String unit_candidates_same_as_dunit = unit_candidates+" same_as_dunit";
  
  projDir = new G4UIdirectory("/project/");
  projDir->SetGuidance("UI commands specific to this project.");
  
  detDir = new G4UIdirectory("/project/det/");
  detDir->SetGuidance("Detector control.");
  
  drawDir = new G4UIdirectory("/project/draw/");
  drawDir->SetGuidance("Draw objects associated with the proect.");
  
  gunRandom = new G4UIdirectory("/gun/random/");
  gunRandom->SetGuidance("Set random properties of a gun.");
  
  gunRandomTrigger = new G4UIdirectory("/gun/random/trigger/");
  gunRandomTrigger->SetGuidance("Set random trigger.");
  
  DoNothing_cmd = new G4UIcmdWithoutParameter("/control/doNothing", this);
  DoNothing_cmd->SetGuidance("Do nothing.");
  //DoNothing_cmd->AvailableForStates(G4State_PreInit, G4State_Idle);
  
  SetMaterial_world_cmd = new G4UIcmdWithAString("/project/det/material_world", this);
  SetMaterial_world_cmd->SetGuidance("Select Material of the world.");
  SetMaterial_world_cmd->SetParameterName("choice", false);
  SetMaterial_world_cmd->AvailableForStates(G4State_PreInit, G4State_Idle);
  
  StepMax_cmd = new G4UIcmdWithADoubleAndUnit("/project/det/stepMax", this);
  StepMax_cmd->SetGuidance("Define a step max.");
  StepMax_cmd->SetParameterName("stepMax", false);
  StepMax_cmd->SetUnitCategory("Length");
  StepMax_cmd->AvailableForStates(G4State_Idle);
  
  Scale_cmd = new G4UIcmdWithoutParameter("/project/scale", this);
  Scale_cmd->SetGuidance("Handy scale for front view.");
  Scale_cmd->AvailableForStates(G4State_PreInit, G4State_Idle);
  
  Descale_cmd = new G4UIcmdWithoutParameter("/project/descale", this);
  Descale_cmd->SetGuidance("Return to default scale.");
  Descale_cmd->AvailableForStates(G4State_PreInit, G4State_Idle);
  
  Verbose_cmd = new G4UIcmdWithAnInteger("/verbose", this);
  Verbose_cmd->SetGuidance("Verbose all.");
  Verbose_cmd->SetParameterName("level", false);
  Verbose_cmd->AvailableForStates(G4State_PreInit, G4State_Idle);
  
/*  DrawTrack_cmd = new G4UIcmdWithoutParameter("/project/draw/track",this);
  DrawTrack_cmd->SetGuidance("Draws the reconstructed track.");
  DrawTrack_cmd->AvailableForStates(G4State_PreInit, G4State_Idle);*/
  
  DrawHitted_cmd = new G4UIcmdWithoutParameter("/project/draw/hitted", this);
  DrawHitted_cmd->SetGuidance("Draws the hitted strips along the track.");
  DrawHitted_cmd->AvailableForStates(G4State_PreInit, G4State_Idle);

  SetAngle_cmd = new G4UIcommand("/gun/angle", this);
  SetAngle_cmd->SetGuidance("Set momentum direction via spherical angles (degrees).");
  //SetAngle_cmd->AvailableForStates(G4State_PreInit, G4State_Idle);
  SetAngle_theta = new G4UIparameter("theta", 'd', true);
  SetAngle_theta->SetDefaultValue(0.);
  SetAngle_cmd->SetParameter(SetAngle_theta);
  SetAngle_phi = new G4UIparameter("phi", 'd', true);
  SetAngle_phi->SetDefaultValue(90.);
  SetAngle_cmd->SetParameter(SetAngle_phi);
  
  SetRandomPosition_cmd = new G4UIcommand("/gun/random/position", this);
  SetRandomPosition_cmd->SetGuidance("Set random (in cuboid) starting position of the particle.\n\n/gun/random/position off  switches randomization off.");
  //SetRandomPosition_cmd->AvailableForStates(G4State_PreInit, G4State_Idle);
  SetRandomPosition_X = new G4UIparameter("X", 's', true);
  SetRandomPosition_X->SetDefaultValue(0.);
  SetRandomPosition_cmd->SetParameter(SetRandomPosition_X);
  SetRandomPosition_Y = new G4UIparameter("Y", 'd', true);
  SetRandomPosition_Y->SetDefaultValue(0.);
  SetRandomPosition_cmd->SetParameter(SetRandomPosition_Y);
  SetRandomPosition_Z = new G4UIparameter("Z", 'd', true);
  SetRandomPosition_Z->SetDefaultValue(0.);
  SetRandomPosition_cmd->SetParameter(SetRandomPosition_Z);
  SetRandomPosition_unit = new G4UIparameter("unit", 's', true);
  SetRandomPosition_unit->SetDefaultValue("same_as_dunit");
  SetRandomPosition_unit->SetParameterCandidates(unit_candidates_same_as_dunit);
  SetRandomPosition_cmd->SetParameter(SetRandomPosition_unit);
  SetRandomPosition_dX = new G4UIparameter("dX", 'd', true);
  SetRandomPosition_dX->SetDefaultValue(0.);
  SetRandomPosition_cmd->SetParameter(SetRandomPosition_dX);
  SetRandomPosition_dY = new G4UIparameter("dY", 'd', true);
  SetRandomPosition_dY->SetDefaultValue(0.);
  SetRandomPosition_cmd->SetParameter(SetRandomPosition_dY);
  SetRandomPosition_dZ = new G4UIparameter("dZ", 'd', true);
  SetRandomPosition_dZ->SetDefaultValue(0.);
  SetRandomPosition_cmd->SetParameter(SetRandomPosition_dZ);
  SetRandomPosition_dunit = new G4UIparameter("dunit", 's', true);
  SetRandomPosition_dunit->SetDefaultValue("cm");
  SetRandomPosition_unit->SetParameterCandidates(unit_candidates);
  SetRandomPosition_cmd->SetParameter(SetRandomPosition_dunit);
  
  SetRandomDirection_cmd = new G4UIcommand("/gun/random/direction", this);
  SetRandomDirection_cmd->SetGuidance("Set random momentum direction of the particle.\n\n/gun/random/direction off  switches randomization off.");
  //SetRandomDirection_cmd->AvailableForStates(G4State_PreInit, G4State_Idle);
  SetRandomDirection_type = new G4UIparameter("type", 's', true);
  SetRandomDirection_type->SetDefaultValue("semisphere_down");
  SetRandomDirection_type->SetParameterCandidates("off sphere semisphere_down semisphere_up plane_down plane_up random_phi");
  SetRandomDirection_cmd->SetParameter(SetRandomDirection_type);
  SetRandomDirection_par1 = new G4UIparameter("par1", 's', true);
  SetRandomDirection_par1->SetDefaultValue("dummy");
  SetRandomDirection_cmd->SetParameter(SetRandomDirection_par1);

  SetRandomDirectionMaxAngle_cmd = new G4UIcommand("/gun/random/direction/maxAngle", this);
  SetRandomDirectionMaxAngle_cmd->SetGuidance("Set a cone semiopening.");
  //SetRandomDirectionMaxAngle_cmd->AvailableForStates(G4State_PreInit, G4State_Idle);
  SetRandomDirectionMaxAngle_angle = new G4UIparameter("angle", 'd', true);
  SetRandomDirectionMaxAngle_angle->SetDefaultValue("90");
  SetRandomDirectionMaxAngle_cmd->SetParameter(SetRandomDirectionMaxAngle_angle);

  SetRandomDirectionMeanDir_cmd = new G4UIcommand("/gun/random/direction/meanDir", this);
  SetRandomDirectionMeanDir_cmd->SetGuidance("Set a cone direction.");
  //SetRandomDirectionMeanDir_cmd->AvailableForStates(G4State_PreInit, G4State_Idle);
  SetRandomDirectionMeanDir_theta = new G4UIparameter("theta", 'd', true);
  SetRandomDirectionMeanDir_theta->SetDefaultValue("0");
  SetRandomDirectionMeanDir_cmd->SetParameter(SetRandomDirectionMeanDir_theta);
  SetRandomDirectionMeanDir_phi = new G4UIparameter("phi", 'd', true);
  SetRandomDirectionMeanDir_phi->SetDefaultValue("0");
  SetRandomDirectionMeanDir_cmd->SetParameter(SetRandomDirectionMeanDir_phi);

  SetRandomTriggerAddCuboid_cmd = new G4UIcommand("/gun/random/trigger/add/cuboid", this);
  SetRandomTriggerAddCuboid_cmd->SetGuidance("Add a cuboid to be necessarily intersected by a gun direction.");
  //SetRandomTriggerAddCuboid_cmd->AvailableForStates(G4State_PreInit, G4State_Idle);
  SetRandomTriggerAddCuboid_X = new G4UIparameter("X", 'd', true);
  SetRandomTriggerAddCuboid_X->SetDefaultValue(0.);
  SetRandomTriggerAddCuboid_cmd->SetParameter(SetRandomTriggerAddCuboid_X);
  SetRandomTriggerAddCuboid_Y = new G4UIparameter("Y", 'd', true);
  SetRandomTriggerAddCuboid_Y->SetDefaultValue(0.);
  SetRandomTriggerAddCuboid_cmd->SetParameter(SetRandomTriggerAddCuboid_Y);
  SetRandomTriggerAddCuboid_Z = new G4UIparameter("Z", 'd', true);
  SetRandomTriggerAddCuboid_Z->SetDefaultValue(0.);
  SetRandomTriggerAddCuboid_cmd->SetParameter(SetRandomTriggerAddCuboid_Z);
  SetRandomTriggerAddCuboid_unit = new G4UIparameter("unit", 's', true);
  SetRandomTriggerAddCuboid_unit->SetDefaultValue("same_as_dunit");
  SetRandomTriggerAddCuboid_unit->SetParameterCandidates(unit_candidates_same_as_dunit);
  SetRandomTriggerAddCuboid_cmd->SetParameter(SetRandomTriggerAddCuboid_unit);
  SetRandomTriggerAddCuboid_dX = new G4UIparameter("dX", 'd', true);
  SetRandomTriggerAddCuboid_dX->SetDefaultValue(0.);
  SetRandomTriggerAddCuboid_cmd->SetParameter(SetRandomTriggerAddCuboid_dX);
  SetRandomTriggerAddCuboid_dY = new G4UIparameter("dY", 'd', true);
  SetRandomTriggerAddCuboid_dY->SetDefaultValue(0.);
  SetRandomTriggerAddCuboid_cmd->SetParameter(SetRandomTriggerAddCuboid_dY);
  SetRandomTriggerAddCuboid_dZ = new G4UIparameter("dZ", 'd', true);
  SetRandomTriggerAddCuboid_dZ->SetDefaultValue(0.);
  SetRandomTriggerAddCuboid_cmd->SetParameter(SetRandomTriggerAddCuboid_dZ);
  SetRandomTriggerAddCuboid_dunit = new G4UIparameter("dunit", 's', true);
  SetRandomTriggerAddCuboid_dunit->SetDefaultValue("cm");
  SetRandomTriggerAddCuboid_unit->SetParameterCandidates(unit_candidates);
  SetRandomTriggerAddCuboid_cmd->SetParameter(SetRandomTriggerAddCuboid_dunit);
  
  SetRandomTriggerInvert_cmd = new G4UIcmdWithAString("/gun/random/trigger/invert", this);
  SetRandomTriggerInvert_cmd->SetGuidance("Invert random trigger.");
  SetRandomTriggerInvert_cmd->SetParameterName("state", true);
  SetRandomTriggerInvert_cmd->AvailableForStates(G4State_PreInit, G4State_Idle);
  SetRandomTriggerInvert_cmd->SetCandidates("on off toggle");
  SetRandomTriggerInvert_cmd->SetDefaultValue("toggle");
  
  SetRandomTriggerClear_cmd = new G4UIcmdWithoutParameter("/gun/random/trigger/clear", this);
  SetRandomTriggerClear_cmd->SetGuidance("Clear random trigger.");
  
  GunShow_cmd = new G4UIcmdWithoutParameter("/gun/show",this);
  GunShow_cmd->SetGuidance("Show gun properties.");
  
  ControlExecute_cmd = new G4UIcmdWithAString("/exec", this);
  ControlExecute_cmd->SetGuidance("Same as /control/execute in/mac/<cmd>.");
  ControlExecute_cmd->SetParameterName("cmd", false);
  ControlExecute_cmd->AvailableForStates(G4State_PreInit, G4State_Idle);
  
}


/*
   ,gggggggggggg,
  dP"""88""""""Y8b,                     I8                                     I8
  Yb,  88       `8b,                    I8                                     I8
   `"  88        `8b                 88888888                               88888888
       88         Y8                    I8                                     I8
       88         d8 ,ggg,     ,g,      I8    ,gggggg,  gg      gg    ,gggg,   I8     ,ggggg,   ,gggggg,
       88        ,8Pi8" "8i   ,8'8,     I8    dP""""8I  I8      8I   dP"  "Yb  I8    dP"  "Y8gggdP""""8I
       88       ,8P'I8, ,8I  ,8'  Yb   ,I8,  ,8'    8I  I8,    ,8I  i8'       ,I8,  i8'    ,8I        8I
       88______,dP' `YbadP' ,8'_   8) ,d88b,,dP     Y8,,d8b,  ,d8b,,d8,_    _,d88b,,d8,   ,d8'        Y8,
      888888888P"  888P"Y888P' "YY8P8P8P""Y88P      `Y88P'"Y88P"`Y8P""Y8888PP8P""Y8P"Y8888P"          `Y8

*/


DetectorMessenger::~DetectorMessenger()
{
  delete projDir;
  delete detDir;
  delete drawDir;
  delete gunRandom;
  delete gunRandomTrigger;
  delete DoNothing_cmd;
  delete SetMaterial_world_cmd;
  delete StepMax_cmd;
  delete Scale_cmd;
  delete Descale_cmd;
  delete Verbose_cmd;
  delete SetAngle_cmd;
  delete SetRandomPosition_cmd;
  delete SetRandomDirection_cmd;
  delete SetRandomDirectionMaxAngle_cmd;
  delete SetRandomDirectionMeanDir_cmd;
  delete SetRandomTriggerAddCuboid_cmd;
  delete SetRandomTriggerInvert_cmd;
  delete SetRandomTriggerClear_cmd;
  delete DrawHitted_cmd;
  delete ControlExecute_cmd;
}



/*
        ,gg,                     ,ggg, ,ggggggg,                          ,ggg,         ,gg
       i8""8i             I8    dP""Y8,8P"""""Y8b                        dP""Y8a       ,8P          ,dPYb,
       `8,,8'             I8    Yb, `8dP'     `88                        Yb, `88       d8'          IP'`Yb
        `88'           88888888  `"  88'       88                         `"  88       88           I8  8I
        dP"8,             I8         88        88                             88       88           I8  8'
       dP' `8a   ,ggg,    I8         88        88   ,ggg,   gg    gg    gg    I8       8I ,gggg,gg  I8 dP  gg      gg   ,ggg,
      dP'   `Yb i8" "8i   I8         88        88  i8" "8i  I8    I8    88bg  `8,     ,8'dP"  "Y8I  I8dP   I8      8I  i8" "8i
  _ ,dP'     I8 I8, ,8I  ,I8,        88        88  I8, ,8I  I8    I8    8I     Y8,   ,8Pi8'    ,8I  I8P    I8,    ,8I  I8, ,8I
  "888,,____,dP `YbadP' ,d88b,       88        Y8, `YbadP' ,d8,  ,d8,  ,8I      Yb,_,dP,d8,   ,d8b,,d8b,_ ,d8b,  ,d8b, `YbadP'
  a8P"Y88888P"8888P"Y8888P""Y8       88        `Y8888P"Y888P""Y88P""Y88P"        "Y8P" P"Y8888P"`Y88P'"Y888P'"Y88P"`Y8888P"Y888b_

*/


void DetectorMessenger::SetNewValue(G4UIcommand* command, G4String newValue)
{
  const char* str = newValue;
  std::istringstream buffer( (char*)str );
  WHICH
    
    INCASE  command == DoNothing_cmd  THEN
      RELAX;
    
    INCASE  command == SetMaterial_world_cmd  THEN
      myDetector->SetMaterial_world(newValue);
    
    INCASE  command == StepMax_cmd  THEN
      myDetector->SetMaxStep(StepMax_cmd->GetNewDoubleValue(newValue));
    
    INCASE  command == Scale_cmd  THEN
      G4UImanager::GetUIpointer()->ApplyCommand("/vis/viewer/set/viewpointVector 0.0001 2 0");
      G4UImanager::GetUIpointer()->ApplyCommand("/vis/viewer/scaleTo 3.3 3.3");
      G4UImanager::GetUIpointer()->ApplyCommand("/vis/viewer/panTo 0 -80 cm");
    
    INCASE  command == Descale_cmd  THEN
      G4UImanager::GetUIpointer()->ApplyCommand("/vis/viewer/scaleTo 2.5 2.5 1");
      G4UImanager::GetUIpointer()->ApplyCommand("/vis/viewer/panTo 0 0 cm");
    
    INCASE  command == Verbose_cmd  THEN
      G4UImanager::GetUIpointer()->ApplyCommand("/vis/verbose " + newValue);
      G4UImanager::GetUIpointer()->ApplyCommand("/hits/verbose " + newValue);
      G4UImanager::GetUIpointer()->ApplyCommand("/material/verbose " + newValue);
      G4UImanager::GetUIpointer()->ApplyCommand("/process/verbose " + newValue);
      G4UImanager::GetUIpointer()->ApplyCommand("/particle/verbose " + newValue);
      G4UImanager::GetUIpointer()->ApplyCommand("/cuts/verbose " + newValue);
      G4UImanager::GetUIpointer()->ApplyCommand("/event/verbose " + newValue);
      G4UImanager::GetUIpointer()->ApplyCommand("/tracking/verbose " + newValue);
      G4UImanager::GetUIpointer()->ApplyCommand("/particle/verbose " + newValue);
      G4UImanager::GetUIpointer()->ApplyCommand("/run/verbose " + newValue);
    #if 0
    INCASE  command == DrawTrack_cmd  THEN
      myReconstruction->GetLine().Draw(2*m);
    #endif
    
    INCASE  command == DrawHitted_cmd  THEN
      G4cerr << "if( command == DrawHitted_cmd )\n {" << G4endl;
      G4cerr << "PositionSystem " << myReconstruction << " used" << G4endl;
      G4cerr << "  uFOR(i, 0, myReconstruction->NPoints()-1)" << G4endl;
      size_t n = myReconstruction->NPoints()-1;
      uFOR(i, 0, n) {
        if(i>181960)G4cerr << "    myReconstruction->GetPoint("<<i<<").point.x() == "<<myReconstruction->GetPoint(i).point.x()/cm<<" cm;" << G4endl;
        if(i>181960)G4cerr << "    if(myReconstruction->GetPoint("<<i<<").enabled) // of "<<n << G4endl;
        if(myReconstruction->GetPoint(i).enabled) {
          if(i>181960)G4cerr << "      myReconstruction->GetPoint("<<i<<").Draw();" << G4endl;
          myReconstruction->GetPoint(i).Draw();
        }
      }
      G4cerr << " }" << G4endl;
      /*uFOR(i, 0, myReconstruction->NPoints()-1)
        if(myReconstruction->GetPoint(i).enabled)
          myReconstruction->GetPoint(i).Draw();*/
    
    INCASE  command == SetAngle_cmd  THEN
      size_t ws = newValue.find(' ');
      G4String par1 = newValue(0, ws);
      G4String par2 = newValue(ws, newValue.size()-1);
      G4double theta = atof(par1) * deg;
      G4double phi   = atof(par2) * deg;
      G4ThreeVector v = Angle3D(theta, phi);
      G4UImanager::GetUIpointer()->ApplyCommand(
        "/gun/direction " + ftoa(v.x())
                    + " " + ftoa(v.y())
                    + " " + ftoa(v.z())
      );
    
    INCASE  command == SetRandomPosition_cmd  THEN
      size_t ws = newValue.find(' ');
      G4String par1 = newValue(0, ws);
      if(par1=="off") { gun->position_random_use = false; return; }
      G4double x, y, z, dx, dy, dz, unit, dunit;
      G4String unit_s, dunit_s;
      buffer >> x >> y >> z >> unit_s >> dx >> dy >> dz >> dunit_s;
      dunit = command->ValueOf(dunit_s);
      if(unit_s == "same_as_dunit") unit = dunit;
      else unit = command->ValueOf(unit_s);
      gun->position_random_use = true;
      gun->position_random_r.set(  x*unit,   y*unit,   z*unit  );
      gun->position_random_d.set( dx*dunit, dy*dunit, dz*dunit );
    
    INCASE  command == SetRandomDirection_cmd  THEN
      G4String type;
      // G4double dir_theta;
      // G4double dir_phi;
      // G4double delta_dir;
      buffer >> type;
      gun->direction_random_use = true;
      WHICH
        INCASE type == "sphere" THEN
          gun->direction_random_type = PrimaryGeneratorAction::kSphere;
        INCASE type == "semisphere_up" THEN
          gun->direction_random_type = PrimaryGeneratorAction::kSemisphere_up;
        INCASE type == "semisphere_down" THEN
          gun->direction_random_type = PrimaryGeneratorAction::kSemisphere_down;
        INCASE type == "plane_down" THEN
          gun->direction_random_type = PrimaryGeneratorAction::kPlaneUniformSource_down;
          // buffer >> delta_dir;
        INCASE type == "plane_up" THEN
          gun->direction_random_type = PrimaryGeneratorAction::kPlaneUniformSource_up;
        INCASE type == "random_phi" THEN
          gun->direction_random_type = PrimaryGeneratorAction::kRandomPhi;
          G4double theta; buffer >> theta;
          gun->direction_random_theta = theta*pi/180;
        ELSE //if(type == "off")
          gun->direction_random_use = false;
      END_WHICH
    
    INCASE  command == SetRandomDirectionMaxAngle_cmd  THEN
      G4double max_angle;
      buffer >> max_angle;
      gun->direction_random_use = true;
      gun->direction_random_max_angle = max_angle;
    
    INCASE  command == SetRandomDirectionMeanDir_cmd  THEN
      G4double theta, phi;
      buffer >> theta >> phi;
      gun->direction_random_use = true;
      gun->direction_random_theta = theta;
      gun->direction_random_phi = phi;
    
    INCASE  command == SetRandomTriggerAddCuboid_cmd  THEN
      gun->line_trigger_use = true;
      G4double x, y, z, dx, dy, dz, unit, dunit;
      G4String unit_s, dunit_s;
      buffer >> x >> y >> z >> unit_s >> dx >> dy >> dz >> dunit_s;
      dunit = command->ValueOf(dunit_s);
      if(unit_s == "same_as_dunit") unit = dunit;
      else unit = command->ValueOf(unit_s);
      G4ThreeVector r(  x*unit,   y*unit,   z*unit  );
      G4ThreeVector d( dx*dunit, dy*dunit, dz*dunit );
      gun->line_trigger_volumes.push_back(Cuboid3D(r,d));
    
    INCASE  command == SetRandomTriggerInvert_cmd  THEN
      G4String val;
      buffer >> val;
      WHICH
        INCASE val == "on"     THEN gun->line_trigger_inverted = true;
        INCASE val == "off"    THEN gun->line_trigger_inverted = false;
        INCASE val == "toggle" THEN gun->line_trigger_inverted -= 1;
      END_WHICH
    
    INCASE  command == SetRandomTriggerClear_cmd  THEN
      gun->line_trigger_use = false;
      gun->line_trigger_volumes.clear();
    
    INCASE  command == GunShow_cmd  THEN
      G4ParticleGun* theGun = gun->GetParticleGun();
      G4cout << "\n____________\nParticle Info:\n";
      G4cout << "  Name: " << theGun->GetParticleDefinition()->GetParticleName() << "\n";
      G4cout << "  Kinetic energy: " << G4BestUnit(theGun->GetParticleEnergy(), "Energy") << "\n";
      Angle3D angle = theGun->GetParticleMomentumDirection();
      G4cout << "  Direction: theta = " << angle.theta/deg << " deg, phi = " << angle.phi/deg << " deg" << "\n";
      G4ThreeVector pos = theGun->GetParticlePosition();
      G4cout << "  Position: (" << G4BestUnit(pos.x(),"Length") << ", "
                                << G4BestUnit(pos.y(),"Length") << ", "
                                << G4BestUnit(pos.z(),"Length") << ")\n";
      G4cout << "____________\n" << G4endl;
    
    INCASE  command == ControlExecute_cmd  THEN
      G4UImanager::GetUIpointer()->ApplyCommand("/control/execute in/mac/" + newValue);
    
  END_WHICH
  
}



/*
       _,ggg,_                                            ,ggg,         ,gg
      d8P"""Y8b,                                         dP""Y8a       ,8P          ,dPYb,
      88,    "8b,                                        Yb, `88       d8'          IP'`Yb
       "Y8baaa`8b                                         `"  88       88           I8  8I
          `""" Y8                                             88       88           I8  8'
               d8  gg      gg   ,ggg,    ,gggggg,  gg     gg  I8       8I ,gggg,gg  I8 dP  gg      gg   ,ggg,
  ,ad8888b,   ,8P  I8      8I  i8" "8i   dP""""8I  I8     8I  `8,     ,8'dP"  "Y8I  I8dP   I8      8I  i8" "8i
  I8P'  `"Y8a,8P'  I8,    ,8I  I8, ,8I  ,8'    8I  I8,   ,8I   Y8,   ,8Pi8'    ,8I  I8P    I8,    ,8I  I8, ,8I
  I8b,,___,,888b,_,d8b,  ,d8b, `YbadP' ,dP     Y8,,d8b, ,d8I    Yb,_,dP,d8,   ,d8b,,d8b,_ ,d8b,  ,d8b, `YbadP'
   `"Y88888P"' "Y88P'"Y88P"`Y8888P"Y8888P      `Y8P""Y88P"888    "Y8P" P"Y8888P"`Y88P'"Y888P'"Y88P"`Y8888P"Y888b_
                                                        ,d8I'
                                                      ,dP'8I
                                                     ,8"  8I
                                                     I8   8I
                                                     `8, ,8I
                                                      `Y8P"
*/

G4String DetectorMessenger::GetCurrentValue(G4UIcommand* command)
{
  WHICH
    
    INCASE  command == SetAngle_cmd  THEN
      return Angle3D(gun->GetParticleGun()->GetParticleMomentumDirection());
    
    INCASE  command == SetRandomTriggerInvert_cmd  THEN
      return _bool[gun->line_trigger_inverted];
    
  END_WHICH
  return "";
}
