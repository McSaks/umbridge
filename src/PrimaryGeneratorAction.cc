#include "PrimaryGeneratorAction.hh"
#include "DetectorConstruction.hh"
#include "RunAction.hh"
#include "Directions.hh"

#include "Randomize.hh"
#include "G4Event.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4UImanager.hh"
#include "globals.hh"
#include "TFormat.hh"
#include "FOR.hh"
using namespace TFormat;
/* /#include "G4VVisManager.hh"
#include "G4Circle.hh"
#include "G4Colour.hh"
#include "G4VisAttributes.hh"*/

#include "CLHEP/Geometry/Transform3D.h"
#include "CLHEP/Geometry/Point3D.h"
using namespace HepGeom;

PrimaryGeneratorAction::PrimaryGeneratorAction(DetectorConstruction* myDC)
: direction_random_type(kSphere), position_random_use(false), direction_random_use(false)
, line_trigger_use(false), line_trigger_inverted(false), myDetector(myDC), run_action(NULL)
{
  G4int n_particle = 1;
  particleGun = new G4ParticleGun(n_particle);
  direction_random_theta = 0;
  direction_random_phi = 0;
  direction_random_max_angle = 90;

// default particle

  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
  G4ParticleDefinition* particle = particleTable->FindParticle("gamma");
  
  particleGun->SetParticleDefinition(particle);
  particleGun->SetParticleMomentumDirection(G4ThreeVector(1.,0.,0.));
  particleGun->SetParticleEnergy(100*GeV);
  particleGun->SetParticlePosition(G4ThreeVector(-10*cm,0,0));
}

PrimaryGeneratorAction::~PrimaryGeneratorAction()
{
  delete particleGun;
}

inline G4double sqr(G4double x) { return x * x; }
void PrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{
  G4bool flag = true;
  Line3D line;
  REPEAT
    flag = true;
    if(position_random_use) {
      G4ThreeVector& r = position_random_r;
      G4ThreeVector& d = position_random_d;
      G4double  x=r.x(),   y=r.y(),   z=r.z();
      G4double dx=d.x(),  dy=d.y(),  dz=d.z();
      if(dx > 0) dx = (G4UniformRand()*2-1) * dx; else dx = 0;
      if(dy > 0) dy = (G4UniformRand()*2-1) * dy; else dy = 0;
      if(dz > 0) dz = (G4UniformRand()*2-1) * dz; else dz = 0;
      x += dx;    y += dy;    z += dz;
      particleGun->SetParticlePosition(G4ThreeVector(x,y,z));
      /* x /= cm;    y /= cm;    z /= cm;
      G4UImanager::GetUIpointer()->ApplyCommand(
        "/gun/position " + ftoa(x)
                   + " " + ftoa(y)
                   + " " + ftoa(z)
                   + " " + "cm"
      );*/
    }
    
    IF_ direction_random_use THEN
      G4double theta = 0, phi;
      SWITCH (direction_random_type)
        CASE kSphere:                   theta = std::acos(G4UniformRand()*2-1);
        CASE kSemisphere_up:            theta = direction_random_max_angle == 90
                                          ? std::acos(-G4UniformRand())
                                          : std::acos(-1 + G4UniformRand() * (1 - std::cos(direction_random_max_angle * deg)));
        CASE kSemisphere_down:          theta = direction_random_max_angle == 90
                                          ? std::acos(G4UniformRand())
                                          : std::acos(1 - G4UniformRand() * (1 - std::cos(direction_random_max_angle * deg)));
        CASE kPlaneUniformSource_down:  theta = direction_random_max_angle == 90
                                          ? std::acos(G4UniformRand()*2-1)/2
                                          : std::acos((1 - G4UniformRand() * (1 - sqr(std::cos(direction_random_max_angle * deg))))*2-1)/2;
        CASE kPlaneUniformSource_up:    theta = pi - ( direction_random_max_angle == 90
                                          ? std::acos(G4UniformRand()*2-1)/2
                                          : std::acos((1 - G4UniformRand() * (1 - sqr(std::cos(direction_random_max_angle * deg))))*2-1)/2 );
        CASE kRandomPhi:                theta = direction_random_theta;
      END_SWITCH
      phi = G4UniformRand() * twopi;
      G4ThreeVector dir = Angle3D(theta, phi);
      // G4cerr << "direction_random_theta: " << direction_random_theta << '\n';
      // G4cerr << "direction_random_phi: " << direction_random_phi << '\n';
      // G4cerr << "orig: " << Angle3D(dir) << '\n';
      IF_ direction_random_theta != 0 THEN
        Point3D<G4double> origin = Point3D<G4double>();
        Point3D<G4double> down = Point3D<G4double>(1, 0, 0);
        Rotate3D T = Rotate3D(
            direction_random_theta * deg,
            /*Vector3D<G4double>(1, 0, 0).cross(
                Vector3D<G4double>( Angle3D(direction_random_theta, direction_random_phi) )
              )*/
            // Vector3D<G4double>( (G4ThreeVector) Angle3D(90, direction_random_phi + 90) )
            Vector3D<G4double>(0, -std::sin(direction_random_phi * deg), std::cos(direction_random_phi * deg))
          );
        // G4cerr << "T =\n" << T.xx() << '\t' << T.xy() << '\t' << T.xz() << '\n'
        //                   << T.yx() << '\t' << T.yy() << '\t' << T.yz() << '\n'
        //                   << T.zx() << '\t' << T.zy() << '\t' << T.zz() << G4endl;
        dir = T * Vector3D<G4double>(dir);
        // G4cerr << "transformed axis: " << Angle3D(G4ThreeVector(T * Vector3D<G4double>(1,0,0))) << '\n' << G4endl;
      END_IF
      // G4cerr << "transformed: " << Angle3D(dir) << '\n' << G4endl;
      particleGun->SetParticleMomentumDirection(dir);
      /*G4UImanager::GetUIpointer()->ApplyCommand(Disp(
        "/gun/angle " + ftoa(theta)
                + " " + ftoa(phi)
      ));*/
    END_IF
    
    run_action->IncreaseEffectiveCounter();
    line.set(particleGun->GetParticlePosition(), particleGun->GetParticleMomentumDirection());
    //G4cerr << "Source position: " << particleGun->GetParticlePosition()/cm << " cm\n";
    //G4cerr << "Gun Direction: " << Angle3D(particleGun->GetParticleMomentumDirection()) << "\n";
    //G4cerr << G4endl;
    vFOR(Cuboid3D, line_trigger_volumes, it)
      IF_
        line_trigger_inverted
          ?  (line & *it)
          : !(line & *it)
      THEN
        flag = false;
        break;
      END_IF
  REPEAT_WHILE ( line_trigger_use && !flag );
  
  particleGun->GeneratePrimaryVertex(anEvent);
  
/*/  G4VVisManager* pVVisManager = G4VVisManager::GetConcreteInstance();
  if(pVVisManager) {
    G4Circle circle(particleGun->GetParticlePosition());
    circle.SetScreenSize(4.);
    circle.SetFillStyle(G4Circle::filled);
    G4VisAttributes attribs(G4Color(1.,0.,1.));
    circle.SetVisAttributes(attribs);
    pVVisManager->Draw(circle);
  }; /*/
}


G4double PrimaryGeneratorAction::GetParticleEnergy() {return particleGun->GetParticleEnergy();}
G4String PrimaryGeneratorAction::GetParticleName() {return particleGun->GetParticleDefinition()->GetParticleName();}
