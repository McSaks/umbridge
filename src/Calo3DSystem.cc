#include <iomanip>

#include "DetectorConstruction.hh"
#include "SensitiveCalo3D.hh"
#include "Calo3DSystem.hh"
#include "EventAction.hh"
#include "EventData.hh"
#include "FOR.hh"


using namespace std;


Calo3DSystem::Calo3DSystem()
  : array(0)
  , etot(0)
  , detector(NULL)
  , messenger(NULL)
  , currentEvent(NULL)
  , data(0)
EMPTY

Calo3DSystem::~Calo3DSystem() EMPTY

void Calo3DSystem::Clear()
{
  if (array) array->Reset();
}

G4bool Calo3DSystem::Write() const
{
  
  if (!data->ok()) return false;
  if (!etot) return true;
  
  data->calo3dData.active = true;
  data->calo3dData.CC2.edep = *etot;
  data->calo3dData.CC2.array = array; // Every time?
  
  return true;
}
