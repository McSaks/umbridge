#include "EventAction.hh"

#include "G4Event.hh"
#include "G4EventManager.hh"
#include "G4TrajectoryContainer.hh"
#include "G4Trajectory.hh"
#include "G4ios.hh"
#include "TFormat.hh"
#include "PositionSystem.hh"
#include "ScintillationSystem.hh"
#include "Calo3DSystem.hh"
#include "Trigger.hh"
#include "RunAction.hh"
#include "EventData.hh"
#include "CLHEP/Random/Random.h"

#include "FOR.hh"

EventAction::EventAction(Trigger* trig)
 : posSystem(NULL), egySystem(NULL), calo3dSystem(NULL)
 , ToF(NULL), AC(NULL), trigger(trig), good_event_id(0)
 , runaction(NULL), data(NULL)
{ }
 
EventAction::~EventAction()
{ G4cout << "EventAction deleted." << G4endl; }
 
void EventAction::BeginOfEventAction(const G4Event*)
{
  
  METHOD(posSystem,Clear)();
  METHOD(ToF,Clear)();
  METHOD(egySystem,Clear)();
  METHOD(calo3dSystem,Clear)();
  METHOD(AC,Clear)();
}

void EventAction::EndOfEventAction(const G4Event* evt)
{
  runaction->IncreaseAllCounter();
  G4int event_id = evt->GetEventID() + 1;
  
  // get number of stored trajectories
  //
  G4TrajectoryContainer* trajectoryContainer = evt->GetTrajectoryContainer();
  G4int n_trajectories = 0;
  if (trajectoryContainer) n_trajectories = trajectoryContainer->entries();
  
  // periodic printing
  //
  TFormat::progress(G4cout, event_id, 0, "event", 100);
  if (event_id < 10 || event_id%1000 == 0) {
//    G4cout << ">>> Event " << event_id << G4endl;
    G4cout << "    " << n_trajectories
	   << " trajectories stored in this event." << G4endl;
  }
  
  #define M_true(obj,method) (obj ? obj->method() : true)
  #define WRITE(obj) M_true(obj, Write)
  G4bool ch = trigger->IsGoodEvent() \
    && WRITE(posSystem) && WRITE(ToF) && WRITE(AC) && WRITE(egySystem) && WRITE(calo3dSystem);
  

  if(!ch) {
    G4cout << "\t\tEvent # " << event_id << " (bad)" << G4endl;
    return;
  }
  
  ++good_event_id;
  data->Write();
  data->Clear();
  // posSystem->GetPoints().reserve(posSystem->GetPoints().size());
    
  G4cout << "\t\tEvent # " << event_id << " ("
         << good_event_id << TFormat::postfix(good_event_id) << " good)" << G4endl;
  
  /*if(trigger->IsGoodEvent()) */
  /*size_t good_event_id_ = */ runaction->operator++();
}

EventAction::operator G4bool() const { return trigger->IsGoodEvent(); }
