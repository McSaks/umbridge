#include "DetectorConstruction.hh"
#include "DetectorMessenger.hh"
#include "Parameterisation.hh"

#include "G4UnitsTable.hh"
#include "G4Material.hh"
#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4PVParameterised.hh"
#include "G4SDManager.hh"
#include "G4GeometryTolerance.hh"
#include "G4GeometryManager.hh"

#include "G4UserLimits.hh"

#include "G4VisAttributes.hh"
#include "G4Colour.hh"

#include "G4NistManager.hh"

#include "SensitiveStrip.hh"
#include "SensitiveEnergy.hh"
#include "SensitiveCalo3D.hh"
#include "SensitiveScintStrip.hh"

#include "Geometry.hh"

#include "G4ios.hh"
#include "globals.hh"
#include <algorithm>
#include <iostream>
#include <iomanip>
#include "TFormat.hh"
using namespace TFormat;
using namespace std;

#include "FOR.hh"
#include "types.hh"

#define array_assign(n,u) assign(u, (u) + (n))
#define push_back_vector(x,y,z) push_back(G4ThreeVector(x,y,z))

DetectorConstruction::DetectorConstruction(G4String geofile)
 : microgap_(0.1*um), stepLimit(0)
 , geo( new Geometry(this, geofile) )
{
}


DetectorConstruction::~DetectorConstruction()
{
  delete stepLimit;
  //delete detectorMessenger;
  delete geo;

  delete materW;      delete materSi;     delete materVacuum;   delete materBGO;    delete materPEth;
  delete materPSty;   delete materAl16;   delete materAl48;     delete materCFRP;   delete materCsI;
  delete materAl;     delete materPMMA;   delete materLiFZnS;

}


G4VPhysicalVolume* DetectorConstruction::Construct()
{

  vector<G4ThreeVector>::iterator dims_it;
  vector<G4Material*>::iterator mater_it;
  register G4int ilayer;
  //register G4bool odd;

  ///////////////////////////////////////////////////////////////////////////////
 //--------------------------- Material definition ---------------------------//
///////////////////////////////////////////////////////////////////////////////

  //                (   name      , symbol , Z  ,     A           );
/*  G4Element _elemH  ( "Hydrogen"  , "H"    ,  1 ,   1.0074  * g/mole );  G4Element* elemH  = &_elemH  ;
  G4Element _elemC  ( "Carbon"    , "C"    ,  6 ,  12.01    * g/mole );  G4Element* elemC  = &_elemC  ;
  G4Element _elemN  ( "Nitrogen"  , "N"    ,  7 ,  14.01    * g/mole );  G4Element* elemN  = &_elemN  ;
  G4Element _elemO  ( "Oxygen"    , "O"    ,  8 ,  16.00    * g/mole );  G4Element* elemO  = &_elemO  ;
  G4Element _elemSi ( "Silicon"   , "Si"   , 14 ,  28.0855  * g/mole );  G4Element* elemSi = &_elemSi ;
  G4Element _elemAl ( "Aluminum"  , "Al"   , 13 ,  26.9815  * g/mole );  G4Element* elemAl = &_elemAl ;
  G4Element _elemFe ( "Iron"      , "Fe"   , 26 ,  55.845   * g/mole );  G4Element* elemFe = &_elemFe ;
  G4Element _elemNi ( "Nickel"    , "Ni"   , 28 ,  58.69334 * g/mole );  G4Element* elemNi = &_elemNi ;
  G4Element _elemCu ( "Copper"    , "Cu"   , 29 ,  63.546   * g/mole );  G4Element* elemCu = &_elemCu ;
  G4Element _elemGe ( "Germanium" , "Ge"   , 32 ,  72.64    * g/mole );  G4Element* elemGe = &_elemGe ;
  G4Element _elemI  ( "Iodine"    , "I"    , 53 , 126.904   * g/mole );  G4Element* elemI  = &_elemI  ;
  G4Element _elemCs ( "Cesium"    , "Cs"   , 53 , 126.904   * g/mole );  G4Element* elemCs = &_elemCs ;
  G4Element _elemW  ( "Tungsten"  , "W"    , 74 , 183.84    * g/mole );  G4Element* elemW  = &_elemW  ;
  G4Element _elemBi ( "Bismuth"   , "Bi"   , 83 , 208.980   * g/mole );  G4Element* elemBi = &_elemBi ;
*/  //                              (   name      , symbol , Z  ,     A           );
  G4NistManager* nist = G4NistManager::Instance();
  G4Element* elemH  = nist->FindOrBuildElement("H");
  G4Element* elemC  = nist->FindOrBuildElement("C");
//G4Element* elemN  = nist->FindOrBuildElement("N");
  G4Element* elemO  = nist->FindOrBuildElement("O");
  G4Element* elemSi = nist->FindOrBuildElement("Si");
  G4Element* elemAl = nist->FindOrBuildElement("Al");
//G4Element* elemFe = nist->FindOrBuildElement("Fe");
//G4Element* elemNi = nist->FindOrBuildElement("Ni");
//G4Element* elemCu = nist->FindOrBuildElement("Cu");
  G4Element* elemGe = nist->FindOrBuildElement("Ge");
  G4Element* elemI  = nist->FindOrBuildElement("I");
  G4Element* elemCs = nist->FindOrBuildElement("Cs");
  G4Element* elemW  = nist->FindOrBuildElement("W");
  G4Element* elemTl = nist->FindOrBuildElement("Tl");
  G4Element* elemBi = nist->FindOrBuildElement("Bi");
  G4Element* elemLi = nist->FindOrBuildElement("Li");
  G4Element* elemF  = nist->FindOrBuildElement("F");
  G4Element* elemZn = nist->FindOrBuildElement("Zn");
  G4Element* elemS  = nist->FindOrBuildElement("S");

  //                          (    name        , density       , nElem );
  materW      = new G4Material( "Tungsten"     , 19.25 * g/cm3 , 1     );
  materSi     = new G4Material( "Silicon"      , 2.333 * g/cm3 , 1     );
  materBGO    = new G4Material( "BGO"          , 7.13  * g/cm3 , 3     );
  materPEth   = new G4Material( "Polyethylene" , 0.890 * g/cm3 , 2     );
  materPSty   = new G4Material( "Polystyrene"  , 1.060 * g/cm3 , 2     );
  materAl16   = new G4Material( "Aluminium_16" , 16    * kg/m3 , 1     );
  materAl48   = new G4Material( "Aluminium_48" , 48    * kg/m3 , 1     );
  materAl108  = new G4Material( "Aluminium_108", 1.08  * g/cm3 , 1     );
  materAl     = new G4Material( "Aluminium"    , 2.7   * g/cm3 , 1     );
  materCFRP   = new G4Material( "CFRP"         , 1.46  * g/cm3 , 3     ); // Carbon-fiber-reinforced polymer
  materCsI    = new G4Material( "CsI(Tl)"      , 4.53  * g/cm3 , 3     );
  materMirror = new G4Material( "VM2000"       , 0.9   * g/cm3 , 2     );
  materPMMA   = new G4Material( "Poly(methyl "
                                "methacrylate)", 1.19  * g/cm3 , 3     );
  materLiFZnS = new G4Material( "LiF ZnS"      , 3     * g/cm3 , 4     );
  //
  //                          (   name   ,  Z ,     A      ,       density         ,   state   , temperature ,   pressure    )
  materVacuum = new G4Material( "Vacuum" ,  1 , 1.01*g/mole, universe_mean_density , kStateGas , 2.73*kelvin , 3.e-18*pascal );

  materW      -> AddElement( elemW  ,  1  );
  materSi     -> AddElement( elemSi ,  1  );
  materBGO    -> AddElement( elemO  , 12  );
  materBGO    -> AddElement( elemGe ,  3  );
  materBGO    -> AddElement( elemBi ,  4  );
  materPEth   -> AddElement( elemH  ,  2  );
  materPEth   -> AddElement( elemC  ,  1  );
  materPSty   -> AddElement( elemH  ,.0774);
  materPSty   -> AddElement( elemC  ,.9226);
  materAl16   -> AddElement( elemAl ,  1  );
  materAl48   -> AddElement( elemAl ,  1  );
  materAl108  -> AddElement( elemAl ,  1  );
  materAl     -> AddElement( elemAl ,  1  );
  materCFRP   -> AddElement( elemC  ,.7195);
  materCFRP   -> AddElement( elemH  ,.2439);
  materCFRP   -> AddElement( elemO  ,.0366);
  materCsI    -> AddElement( elemCs ,  5  );
  materCsI    -> AddElement( elemI  ,  5  );
  materCsI    -> AddElement( elemTl ,  0  );
  materMirror -> AddElement( elemC  ,  2  );
  materMirror -> AddElement( elemH  ,  4  );
  materPMMA   -> AddElement( elemC  ,  5  );
  materPMMA   -> AddElement( elemO  ,  2  );
  materPMMA   -> AddElement( elemH  ,  8  );
  materLiFZnS -> AddElement( elemLi ,.2422);
  materLiFZnS -> AddElement( elemF  ,.2422);
  materLiFZnS -> AddElement( elemZn ,.2578);
  materLiFZnS -> AddElement( elemS  ,.2578);

  materBottomStuff = materAl;


  // Print all the materials defined.
  G4cout << "\nThe materials defined are : \n\n";
  G4cout << *(G4Material::GetMaterialTable()) << G4endl;


  ///////////////////////////////////////////////////////////////////////////////
 //------------------------- Geometrical parameters --------------------------//
///////////////////////////////////////////////////////////////////////////////

  G4GeometryManager::GetInstance()->SetWorldMaximumExtent(10*m);
  geo->Read();

  ///////////////////////////////////////////////////////////////////////////////
 //------------------- Definitions of Sensitive Detectors --------------------//
///////////////////////////////////////////////////////////////////////////////

  G4SDManager* SDman = G4SDManager::GetSDMpointer();
  G4String name;

  /* * * * * * *\
  |*   world   *|
  \* * * * * * */
  G4cout << "Computed tolerance = "
         << G4GeometryTolerance::GetInstance()->GetSurfaceTolerance()/mm
         << " mm" << G4endl;

  /* * * * * * *\
  |*   topAC   *|
  \* * * * * * */
  vFOR(BoxVolume, topAC, vol) {
    vol->sensitive = new SensitiveScintStrip(
      "/" + vol->name,
      this,
      vol->physic,
      AC,
      vol->attr.scint.nstrips,
      0, 0,
      kAC,
      kYStripProj, kZStripProj);
    ((SensitiveScintStrip*)vol->sensitive)->Add(AC);
    ((SensitiveScintStrip*)vol->sensitive)->Setup(
      vol->attr.scint.t_min,
      vol->attr.scint.t_max,
      vol->attr.scint.t_nbins,
      vol->attr.scint.p_nbins,
      vol->attr.scint.nstrips,
      vol->attr.scint.speedoflight);
    SDman->AddNewDetector(vol->sensitive);

    vol->SetSensitiveDetector();
    G4cout << "topAC is " << G4BestUnit(2*vol->dims.x(), "Length") << " of "
           << vol->mater->GetName() << G4endl;
  }
  /* * * * * * *\
  |*  sideAC   *|
  \* * * * * * */

  tFOR(/*EDetSide*/ G4int, side, kLeft, kBack)  BEGIN
   BoxVolumes& AC_side = sideAC[side];
   vFOR(BoxVolume, AC_side, vol_iter)  BEGIN
    BoxVolume& vol = *vol_iter;
    vol.sensitive = new SensitiveScintStrip(
      "/" + vol.name,
      this,
      vol.physic,
      AC,
      vol.attr.scint.nstrips,
      0, 0,
      kAC,
      sideAC_horizontal
        ? kXStripProj
        : side % 2
          ? kZStripProj   // kBack, kFront
          : kYStripProj,  // kLeft, kRight
      sideAC_horizontal
        ? side % 2
          ? kZStripProj   // kBack, kFront
          : kYStripProj   // kLeft, kRight
        : kXStripProj
    );
    ((SensitiveScintStrip*)vol.sensitive)->Add(AC);
    ((SensitiveScintStrip*)vol.sensitive)->Setup(
    vol.attr.scint.t_min,
    vol.attr.scint.t_max,
    vol.attr.scint.t_nbins,
    vol.attr.scint.p_nbins,
    vol.attr.scint.nstrips,
    vol.attr.scint.speedoflight);
    SDman->AddNewDetector(vol.sensitive);
    vol.SetSensitiveDetector();
   END_FOR
  END_FOR

  /* * * * * * *\
  |* SiArray1  *|
  \* * * * * * */

  /* * * * * * *\
  |*     C     *|
  \* * * * * * */
  ilayer = 0;
  vFOR(BoxVolume, CLayerSiY, vol) {
    vol->sensitive = new SensitiveStrip
                      ("/"+vol->name, this, vol->physic, posSystem,
                       vol->attr.strip.nstrips, vol->attr.strip.nstrips, 2, ilayer,
                       kConverter, kYStripProj, kZStripProj, vol->attr.strip.spreading);
    SDman->AddNewDetector(vol->sensitive);
    vol->SetSensitiveDetector();
    ++ilayer;
  }
  ilayer = 0;
  vFOR(BoxVolume, CLayerSiZ, vol) {
    vol->sensitive = new SensitiveStrip
                      ("/"+vol->name, this, vol->physic, posSystem,
                       vol->attr.strip.nstrips, vol->attr.strip.nstrips, 2, ilayer,
                       kConverter, kZStripProj, kYStripProj, vol->attr.strip.spreading);
    SDman->AddNewDetector(vol->sensitive);
    vol->SetSensitiveDetector();
    ++ilayer;
  }
  G4cout << "C constructed" << G4endl;

  /* * * * * * *\
  |*    S1     *|
  \* * * * * * */
  vFOR(BoxVolume, S1, vol) {
    vol->sensitive = new SensitiveScintStrip("/"+vol->name, this, vol->physic,
                        ToF, vol->attr.scint.nstrips,
                        0, 1,
                        kToF, kYStripProj, kZStripProj);
    ((SensitiveScintStrip*)vol->sensitive)->Add(ToF);
    ((SensitiveScintStrip*)vol->sensitive)->Setup(
      vol->attr.scint.t_min,
      vol->attr.scint.t_max,
      vol->attr.scint.t_nbins,
      vol->attr.scint.p_nbins,
      vol->attr.scint.nstrips,
      vol->attr.scint.speedoflight);
    SDman->AddNewDetector(vol->sensitive);

    vol->SetSensitiveDetector();
    G4cout << "S1 is " << G4BestUnit(2*vol->dims.x(), "Length") << " of "
           << vol->mater->GetName() << G4endl;
  }

  /* * * * * * *\
  |*    TD     *|
  \* * * * * * */

  /* * * * * * *\
  |*    S2     *|
  \* * * * * * */
  vFOR(BoxVolume, S2, vol) {
    vol->sensitive = new SensitiveScintStrip("/"+vol->name, this, vol->physic,
                        ToF, vol->attr.scint.nstrips,
                        0, 2,
                        kToF, kYStripProj, kZStripProj);
    ((SensitiveScintStrip*)vol->sensitive)->Add(ToF);
    ((SensitiveScintStrip*)vol->sensitive)->Setup(
      vol->attr.scint.t_min, vol->attr.scint.t_max,
      vol->attr.scint.t_nbins,
      vol->attr.scint.p_nbins,
      vol->attr.scint.nstrips,
      vol->attr.scint.speedoflight);
    SDman->AddNewDetector(vol->sensitive);

    vol->SetSensitiveDetector();
    G4cout << "S2 is " << G4BestUnit(2*vol->dims.x(), "Length") << " of "
         << vol->mater->GetName() << G4endl;
  }

  /* * * * * * *\
  |*    CC1    *|
  \* * * * * * */
  ilayer = 0;
  vFOR(BoxVolume, CC1LayerSiY, vol) {
    vol->sensitive = new SensitiveStrip
                      ("/"+vol->name, this, vol->physic, posSystem,
                       vol->attr.strip.nstrips, vol->attr.strip.nstrips, 2, ilayer,
                       kCalorimeter, kYStripProj, kZStripProj, vol->attr.strip.spreading);
    SDman->AddNewDetector(vol->sensitive);
    vol->SetSensitiveDetector();
    ++ilayer;
  }
  ilayer = 0;
  vFOR(BoxVolume, CC1LayerSiZ, vol) {
    vol->sensitive = new SensitiveStrip
                      ("/"+vol->name, this, vol->physic, posSystem,
                       vol->attr.strip.nstrips, vol->attr.strip.nstrips, 2, ilayer,
                       kCalorimeter, kZStripProj, kYStripProj, vol->attr.strip.spreading);
    SDman->AddNewDetector(vol->sensitive);
    vol->SetSensitiveDetector();
    ++ilayer;
  }
  ilayer = 0;
  vFOR(BoxVolume, CC1LayerCsI, vol) {
    vol->sensitive = new SensitiveEnergy
                      ("/"+vol->name, this, vol->physic, caloSystem,
                       1, ilayer, kCalorimeter);
    SDman->AddNewDetector(vol->sensitive);
    vol->SetSensitiveDetector();
    ++ilayer;
  }
  G4cout << "CC1 constructed" << G4endl;

  /* * * * * * *\
  |*    S3     *|
  \* * * * * * */
  vFOR(BoxVolume, S3, vol) {
    vol->sensitive = new SensitiveScintStrip("/"+vol->name, this, vol->physic,
                        ToF, vol->attr.scint.nstrips,
                        0, 3,
                        kToF, kYStripProj, kZStripProj);
    ((SensitiveScintStrip*)vol->sensitive)->Add(ToF);
    ((SensitiveScintStrip*)vol->sensitive)->Setup(
      vol->attr.scint.t_min, vol->attr.scint.t_max,
      vol->attr.scint.t_nbins,
      vol->attr.scint.p_nbins,
      vol->attr.scint.nstrips,
      vol->attr.scint.speedoflight);
    SDman->AddNewDetector(vol->sensitive);

    vol->SetSensitiveDetector();
    G4cout << "S3 is " << G4BestUnit(2*vol->dims.x(), "Length") << " of "
           << vol->mater->GetName() << G4endl;
  }

  /* * * * * * *\
  |*    CC2    *|
  \* * * * * * */
  IF (CC2_is_segmented)
    CC2.sensitive = new SensitiveCalo3D(
      "/" + CC2.name,  this,  NULL,  cc2System,
      CC2.attr.calo3d.nx,
      CC2.attr.calo3d.ny,
      CC2.attr.calo3d.nz,
      CC2.attr.calo3d.wy,
      CC2.attr.calo3d.wz,
      kCalorimeter
    );
    SDman->AddNewDetector(CC2.sensitive);
    CC2.SetSensitiveDetector();
  ELSE
    CC2_single.sensitive = new SensitiveEnergy(
      "/" + CC2_single.name,  this,  CC2_single.physic,  caloSystem,
      2, 0,
      kCalorimeter
    );
    SDman->AddNewDetector(CC2_single.sensitive);

    CC2_single.SetSensitiveDetector();
    G4cout << "CC2 is " << G4BestUnit(2*CC2_single.dims.x(), "Length") << " of "
           << CC2_single.mater->GetName() << G4endl;
  END (IF CC2_is_segmented)

  /* * * * * * *\
  |*    S4     *|
  \* * * * * * */
  vFOR(BoxVolume, S4, vol) {
    vol->sensitive = new SensitiveScintStrip("/"+vol->name, this, vol->physic,
                        ToF, vol->attr.scint.nstrips,
                        0, 4,
                        kToF, kYStripProj, kZStripProj);
    ((SensitiveScintStrip*)vol->sensitive)->Add(ToF);
    ((SensitiveScintStrip*)vol->sensitive)->Setup(
      vol->attr.scint.t_min, vol->attr.scint.t_max,
      vol->attr.scint.t_nbins,
      vol->attr.scint.p_nbins,
      vol->attr.scint.nstrips,
      vol->attr.scint.speedoflight);
    SDman->AddNewDetector(vol->sensitive);

    vol->SetSensitiveDetector();
    G4cout << "S4 is " << G4BestUnit(2*vol->dims.x(), "Length") << " of "
           << vol->mater->GetName() << G4endl;
  }

  /* * * * * * * * * * * *\
  |*    Visualization    *|
  \* * * * * * * * * * * */

  __attribute__ ((unused))
  G4VisAttributes* colorInvisible =
         new G4VisAttributes(G4Colour(0, 0, 0));
  G4VisAttributes* colorWorld =
         new G4VisAttributes(G4Colour(0.788235, 0.788235, 0.788235, 0.04));
  colorWorld->SetForceWireframe(true); //colorWorld->SetVisibility(false);
  G4VisAttributes* colorW =
         new G4VisAttributes(G4Colour(0.000000, 0.874510, 0.000000, 1.00));
  colorW->SetForceSolid(true);
  G4VisAttributes* colorCsI =
         new G4VisAttributes(G4Colour(1.000000, 0.913725, 0.000000, 1.00));
  colorCsI->SetForceSolid(true);
  G4VisAttributes* colorSi =
         new G4VisAttributes(G4Colour(1.000000, 0.400000, 0.000000, 1.00));
  colorSi->SetForceSolid(true);
  colorSi->SetLineWidth(1.5);
  G4VisAttributes* colorBGO =
         new G4VisAttributes(G4Colour(0.000000, 0.700000, 0.900000, 1.00));
  colorBGO->SetForceSolid(true);
  G4VisAttributes* colorPSty =
         new G4VisAttributes(G4Colour(0.000000, 0.400000, 0.800000, 1.00));
  colorPSty->SetForceSolid(true);
  G4VisAttributes* colorPEth =
         new G4VisAttributes(G4Colour(1.000000, 0.800000, 0.600000, 1.00));
  colorPEth->SetForceSolid(true);
  G4VisAttributes* colorAl =
         new G4VisAttributes(G4Colour(0.807843, 0.890196, 0.854902, 1.00));
  colorAl->SetForceSolid(true);
  G4VisAttributes* colorAl108 =
         new G4VisAttributes(G4Colour(0.807843, 0.890196, 0.854902, 0.50));
  colorAl108->SetForceSolid(true);
  G4VisAttributes* colorCFRP =
         new G4VisAttributes(G4Colour(0.000000, 0.588235, 0.588235, 0.00));
  colorCFRP->SetForceSolid(true);
  G4VisAttributes* colorMirror =
         new G4VisAttributes(G4Colour(0.945098, 0.937255, 0.894118, 1.00));
  colorMirror->SetForceSolid(true);
  G4VisAttributes* colorPMMA =
         new G4VisAttributes(G4Colour(0.925490, 0.705882, 0.650980, 1.00));
  colorPMMA->SetForceSolid(true);
  G4VisAttributes* colorLiFZnS =
         new G4VisAttributes(G4Colour(0.172549, 0.168627, 0.325490, 1.00));
  colorLiFZnS->SetForceSolid(true);
  G4VisAttributes* colorBottomStuff =
         new G4VisAttributes(G4Colour(0.807843, 0.890196, 0.854902, 0.20));
  colorBottomStuff->SetForceSolid(true);

  world.logic                  ->       SetVisAttributes(colorWorld);
                      vFOR(BoxVolume, topAC, vol) if(*vol)
  vol->logic                   ->       SetVisAttributes(colorPSty);
  topACSupportTopCFRP.logic    ->       SetVisAttributes(colorCFRP);
  topACSupportAl.logic         ->       SetVisAttributes(colorAl);
  topACSupportBotCFRP.logic    ->       SetVisAttributes(colorCFRP);
                      vFOR(BoxVolumes, sideAC, vols)
                      vFOR(BoxVolume, *vols, vol) if(*vol)
  vol->logic                   ->       SetVisAttributes(colorWorld);
  vFOR(BoxVolume, CLayerW,      vol) if(*vol) vol->logic -> SetVisAttributes(colorW);
  vFOR(BoxVolume, CLayerSiY,    vol) if(*vol) vol->logic -> SetVisAttributes(colorSi);
  vFOR(BoxVolume, CLayerSiZ,    vol) if(*vol) vol->logic -> SetVisAttributes(colorSi);
  vFOR(BoxVolume, CSupportAl,   vol) if(*vol) vol->logic -> SetVisAttributes(colorAl);
  vFOR(BoxVolume, CSupportTopCFRP, vol) vol->logic -> SetVisAttributes(colorCFRP);
  vFOR(BoxVolume, CSupportBotCFRP, vol) vol->logic -> SetVisAttributes(colorCFRP);
                      vFOR(BoxVolume, S1, vol) if(*vol)
  vol->logic                   ->       SetVisAttributes(colorPSty);
  S1SupportTopCFRP.logic       ->       SetVisAttributes(colorCFRP);
  S1SupportAl.logic            ->       SetVisAttributes(colorAl);
  S1SupportBotCFRP.logic       ->       SetVisAttributes(colorCFRP);
                      vFOR(BoxVolume, TD, vol) if(*vol)
  vol->logic                   ->       SetVisAttributes(colorWorld);
                      vFOR(BoxVolume, S2, vol) if(*vol)
  vol->logic                   ->       SetVisAttributes(colorPSty);
  S2SupportTopCFRP.logic       ->       SetVisAttributes(colorCFRP);
  S2SupportAl.logic            ->       SetVisAttributes(colorAl);
  S2SupportBotCFRP.logic       ->       SetVisAttributes(colorCFRP);
  vFOR(BoxVolume, CC1LayerCsI,    vol) if(*vol) vol->logic -> SetVisAttributes(colorCsI);
  vFOR(BoxVolume, CC1LayerSiY,    vol) if(*vol) vol->logic -> SetVisAttributes(colorSi);
  vFOR(BoxVolume, CC1LayerSiZ,    vol) if(*vol) vol->logic -> SetVisAttributes(colorSi);
  vFOR(BoxVolume, CC1SupportAl_CsI,   vol)    if(*vol) vol->logic -> SetVisAttributes(colorAl);
  vFOR(BoxVolume, CC1SupportTopCFRP_CsI, vol) if(*vol) vol->logic -> SetVisAttributes(colorCFRP);
  vFOR(BoxVolume, CC1SupportBotCFRP_CsI, vol) if(*vol) vol->logic -> SetVisAttributes(colorCFRP);
  vFOR(BoxVolume, CC1SupportAl_Si,   vol)     if(*vol) vol->logic -> SetVisAttributes(colorAl);
  vFOR(BoxVolume, CC1SupportTopCFRP_Si, vol)  if(*vol) vol->logic -> SetVisAttributes(colorCFRP);
  vFOR(BoxVolume, CC1SupportBotCFRP_Si, vol)  if(*vol) vol->logic -> SetVisAttributes(colorCFRP);
                      vFOR(BoxVolume, S3, vol) if(*vol)
  vol->logic                   ->       SetVisAttributes(colorPSty);
  S3SupportTopCFRP.logic       ->       SetVisAttributes(colorCFRP);
  S3SupportAl.logic            ->       SetVisAttributes(colorAl);
  S3SupportBotCFRP.logic       ->       SetVisAttributes(colorCFRP);
  IF(CC2_is_segmented)
    if(CC2.logic)  CC2.logic   ->       SetVisAttributes(colorCsI);
    vFOR(BoxVolume, CC2_mirrorfilm.volumes, vol) if(*vol) vol->logic -> SetVisAttributes(colorMirror);
    vFOR(BoxVolume, CC2_CFRP_x.volumes, vol) if(*vol) vol->logic -> SetVisAttributes(colorCFRP);
    vFOR(BoxVolume, CC2_CFRP_y.volumes, vol) if(*vol) vol->logic -> SetVisAttributes(colorCFRP);
    vFOR(BoxVolume, CC2_CFRP_z.volumes, vol) if(*vol) vol->logic -> SetVisAttributes(colorCFRP);
  ELSE
    if(CC2SupportTopCFRP.logic)
      CC2SupportTopCFRP.logic  ->       SetVisAttributes(colorCFRP);
    CC2_single.logic           ->       SetVisAttributes(colorCsI);
    if(CC2SupportBotCFRP.logic)
      CC2SupportBotCFRP.logic  ->       SetVisAttributes(colorCFRP);
  END_IF
  if(CC2SupportTopAl_o.logic)
    CC2SupportTopAl_o.logic    ->       SetVisAttributes(colorAl);
  if(CC2SupportTopAl_i.logic)
    CC2SupportTopAl_i.logic    ->       SetVisAttributes(colorAl108);
  if(CC2SupportBotAl_i.logic)
    CC2SupportBotAl_i.logic    ->       SetVisAttributes(colorAl108);
  if(CC2SupportBotAl_o.logic)
    CC2SupportBotAl_o.logic    ->       SetVisAttributes(colorAl);
  //AlPlate.logic                ->       SetVisAttributes(colorAl);
                      vFOR(BoxVolume, S4, vol) if(*vol)
  vol->logic                   ->       SetVisAttributes(colorPSty);
  S4SupportTopCFRP.logic       ->       SetVisAttributes(colorCFRP);
  S4SupportAl.logic            ->       SetVisAttributes(colorAl);
  S4SupportBotCFRP.logic       ->       SetVisAttributes(colorCFRP);
  ND_pEth1.logic               ->       SetVisAttributes(colorPEth);
  ND_pEth2.logic               ->       SetVisAttributes(colorPEth);
  ND_pEth3.logic               ->       SetVisAttributes(colorPEth);
  ND_PMMA1.logic               ->       SetVisAttributes(colorPMMA);
  ND_PMMA2.logic               ->       SetVisAttributes(colorPMMA);
  ND_PMMA3.logic               ->       SetVisAttributes(colorPMMA);
  ND_PMMA4.logic               ->       SetVisAttributes(colorPMMA);
  ND_LiFZnS1.logic             ->       SetVisAttributes(colorLiFZnS);
  ND_LiFZnS2.logic             ->       SetVisAttributes(colorLiFZnS);

  NDSupportTopCFRP.logic       ->       SetVisAttributes(colorCFRP);
  NDSupportAl.logic            ->       SetVisAttributes(colorAl);
  NDSupportBotCFRP.logic       ->       SetVisAttributes(colorCFRP);
  if(BottomStuff.logic)
    BottomStuff.logic          ->       SetVisAttributes(colorBottomStuff);

#if 0
  G4Box* test_solid = new G4Box("test", 1*cm, 1*cm, 1*cm);
  G4LogicalVolume* test_logic = new G4LogicalVolume(test_solid,      // its solid
                              materBGO,     // its material
                              "test",     // its name
                              NULL,    // field
                              NULL,   // sensitive detector
                              NULL); // user limits
  G4PVPlacement* test_physic = new G4PVPlacement(NULL,             // no rotation
                             G4ThreeVector(1.5,0,0)*cm,             // its position
                             test_logic,          // its logical volume
                             "test",          // its name
                             world.logic, // its mother volume
                             false,       // no boolean operations
                             0);         // copy number
  test_logic->SetVisAttributes(colorBGO);
#endif
//--------- example of User Limits -------------------------------

  // below is an example of how to set tracking constraints in a given
  // logical volume(see also in PhysicsList how to setup the processes
  // G4StepLimiter or G4UserSpecialCuts).

  // Sets a max Step length in the tracker region, with G4StepLimiter
  //
  //G4double maxStep = 0.25*detectorCC1_thickness;
  //stepLimit = new G4UserLimits(maxStep);
  //logicTracker->SetUserLimits(stepLimit);

  // Set additional contraints on the track, with G4UserSpecialCuts
  //
  // G4double maxLength = 2*fTrackerLength, maxTime = 0.1*ns, minEkin = 10*MeV;
  // logicTracker->SetUserLimits(new G4UserLimits(maxStep,maxLength,maxTime,
  //                                               minEkin));

  //AC->Set(); ToF->Set();
  PrintAllPosAndDims();
  PrintThickness();
  return world.physic;
}

void DetectorConstruction::SetMaterial_world(G4String materialName)
{
  // search the material by its name
  G4Material* material = G4Material::GetMaterial(materialName);
  if (material) {
    world.SetMater(material);
    G4cout << "\n---->  World is now filled by "
           << material->GetName() << G4endl;
  }
}

void DetectorConstruction::SetMaxStep(G4double maxStep)
{
  if ((stepLimit)&&(maxStep>0.)) stepLimit->SetMaxAllowedStep(maxStep);
}


#include <fstream>
void DetectorConstruction::PrintAllPosAndDims()
{
  ofstream file(outfile, std::ofstream::out|std::ofstream::app);
  register size_t ilayer;
  G4double thickness;
  enum Eunit {kmm, kcm} unit;
  ios::fmtflags coutflags = G4cout.flags();
  G4cout.setf(ios::fixed);

  G4cout << "   __________________________________________________________\n";
  G4cout << "  /                                                          \\ _\n";
  G4cout << " |                  Positions and dimentions                  | \\\n";
  G4cout <<"  \\__________________________________________________________/  |\n";
  G4cout <<"    \\__________________________________________________________/\n\n";
  G4cout << " ,_____________________________________________________________________________,\n";
  G4cout << " |      Detector       |           X-pos           |         Thickness         |\n";
  G4cout << " |_____________________|___________________________|___________________________|" << G4endl;

  #define PRINT_BOX(box,label)                \
    thickness = 2*(box).dims.x(); unit = kcm;            \
    if(thickness < cm) {thickness*=10; unit=kmm;};       \
    G4cout << " |" label "|   "                           \
           << setw(15) << setprecision(10) << (box).pos.x()/cm  \
           << " cm      |   "                               \
           << setw(15) << setprecision(10) << thickness/cm     \
           << ((unit==kcm)?" cm":" mm") << "      |" << G4endl;

  #define PRINT_CONTAINER(label)                 \
    G4cout << " |" label "|   " << setw(7) << ""  \
           << "                 |   " << setw(7) << ""     \
           << "                 |" << G4endl;

  #define PRINT_GEO_TO_FILE(box)                          \
    file << (box).pos.x()/mm << " " << 2*(box).dims.x()/mm << G4endl;

  #define PRINT_NLAYERS(boxes)                                        \
    file << " " << boxes.nlayers << " " << (boxes.nlayers ? 2*boxes.front().dims.y()/mm : 0.) \
         << " " << (boxes.nlayers ? 2*boxes.front().dims.y() : 0.)/                            \
                            (boxes.nlayers ? boxes.front().attr.strip.nstrips/mm : 0.) << G4endl;

  #define PRINT_BOXES(boxes,label,label2)                           \
    thickness = 2*boxes[ilayer].dims.x(); unit = kcm;                \
    if(thickness < cm) {thickness*=10; unit=kmm;};                    \
    G4cout << " |" label "[" << setw(2) << ilayer << "]" label2 "|   " \
           << setw(15) << setprecision(10) << boxes[ilayer].pos.x()/cm    \
           << " cm      |   "                                            \
           << setw(15) << setprecision(10) << thickness/cm                  \
           << ((unit==kcm)?" cm":" mm") << "      |" << G4endl;

  #define PRINT_BOXES_POS_DIM(pos,dim,ilayer,label,label2)          \
    thickness = dim; unit = kcm;                                     \
    if(thickness < cm) {thickness*=10; unit=kmm;};                    \
    G4cout << " |" label "[" << setw(4) << ilayer << "]" label2 "|   " \
           << setw(15) << setprecision(10) << (pos)/cm                    \
           << " cm      |   "                                            \
           << setw(15) << setprecision(10) << thickness/cm                  \
           << ((unit==kcm)?" cm":" mm") << "      |" << G4endl;

  #define PRINT_GEOS_TO_FILE(boxes)                                 \
    file << boxes[ilayer].pos.x()/mm << " " << 2*boxes[ilayer].dims.x()/mm << G4endl;

  #define PRINT_GEOS_TO_FILE_POS_DIM(posdim)              \
    file << chop(CC2.pos.x() + posdim->pos.x()/mm) << " "  \
         << chop(CC2.pos.y() + posdim->pos.y()/mm) << " "   \
         << chop(CC2.pos.z() + posdim->pos.z()/mm) << " "    \
         << 2*posdim->dims.x()/mm << " "                      \
         << 2*posdim->dims.y()/mm << " "                       \
         << 2*posdim->dims.z()/mm << " ";

  #define _PSSTF_SENS(box) ((SensitiveScintStrip*)((box).sensitive))

  #define PRINT_SCINT_STRIP_TO_FILE(box)              \
    file << (box).pos.x()/mm << " " << 2*(box).dims.x()/mm  \
         << " " << 2*(box).dims.y() / mm << " "             \
         <<        2*(box).dims.z() / mm << " "             \
         << _PSSTF_SENS(box)->GetStripAxisName() << " "   \
         << _PSSTF_SENS(box)->GetAlongAxisName() << " "   \
         << _PSSTF_SENS(box)->data.t_min / ns << " "       \
         << _PSSTF_SENS(box)->data.t_max / ns << " "        \
         << _PSSTF_SENS(box)->data.t_nbins << " "            \
         << _PSSTF_SENS(box)->data.p_nbins << " "             \
         << _PSSTF_SENS(box)->data.s_nbins << " "              \
         << _PSSTF_SENS(box)->GetStripOffset() / mm << G4endl;

  file << "AC-top " << topAC.size() << "\n";
  vFOR(BoxVolume, topAC, vol) {
    PRINT_BOX(*vol,"  topAC              ");
    PRINT_SCINT_STRIP_TO_FILE(*vol);
  }
  file << "AC-left " << sideAC[kLeft].size() << "\n";
  vFOR(BoxVolume, sideAC[kLeft], vol)
    PRINT_SCINT_STRIP_TO_FILE(*vol);
  file << "AC-front " << sideAC[kFront].size() << "\n";
  vFOR(BoxVolume, sideAC[kFront], vol)
    PRINT_SCINT_STRIP_TO_FILE(*vol);
  file << "AC-right " << sideAC[kRight].size() << "\n";
  vFOR(BoxVolume, sideAC[kRight], vol)
    PRINT_SCINT_STRIP_TO_FILE(*vol);
  file << "AC-back " << sideAC[kBack].size() << "\n";
  vFOR(BoxVolume, sideAC[kBack], vol)
    PRINT_SCINT_STRIP_TO_FILE(*vol);

  PRINT_BOX(sideAC.front().front(),"  sideAC             ");

  file << "C ";
  PRINT_NLAYERS(CLayerSiY);
  PRINT_CONTAINER("  C  :               ");

  dFORs(ilayer, 0, CLayerSiY.nlayers) {
    PRINT_BOXES(CLayerW,"    CLayerW   ", "   ");
    PRINT_BOXES(CLayerSiY,"    CLayerSiY ", "   ");
    PRINT_GEOS_TO_FILE(CLayerSiY);
    PRINT_BOXES(CLayerSiZ,"    CLayerSiZ ", "   ");
    PRINT_GEOS_TO_FILE(CLayerSiZ);
  };

  file << "S1 " << S1.size() << "\n";
  vFOR(BoxVolume, S1, vol) {
    PRINT_BOX(*vol,"  S1                 ");
    PRINT_SCINT_STRIP_TO_FILE(*vol);
  }

  file << "S2 " << S2.size() << "\n";
  vFOR(BoxVolume, S2, vol) {
    PRINT_BOX(*vol,"  S2                 ");
    PRINT_SCINT_STRIP_TO_FILE(*vol);
  }

  file << "CC1 ";
  PRINT_NLAYERS(CC1LayerSiY);
  PRINT_CONTAINER("  CC1 :              ");

  dFORs(ilayer, 0, CC1LayerSiY.nlayers) {
    PRINT_BOXES(CC1LayerCsI,"    CC1LayerCsI ", " ");
    PRINT_BOXES(CC1LayerSiY,"    CC1LayerSiY ", " ");
    PRINT_GEOS_TO_FILE(CC1LayerSiY);
    PRINT_BOXES(CC1LayerSiZ,"    CC1LayerSiZ ", " ");
    PRINT_GEOS_TO_FILE(CC1LayerSiZ);
  };

  file << "S3 " << S3.size() << "\n";
  vFOR(BoxVolume, S3, vol) {
    PRINT_BOX(*vol,"  S3                 ");
    PRINT_SCINT_STRIP_TO_FILE(*vol);
  }

  IF (CC2_is_segmented)
    PRINT_CONTAINER("  CC2 :              ");
    file << "CC2 + ";
    const vector<BoxUnionVolume::PosDims>& posdims = CC2.posdims;
    PRINT_BOXES_POS_DIM(CC2.pos.x() + posdims.front().pos.x(), posdims.front().dims.x(), 0, "    CC2 first ", " ");
    PRINT_BOXES_POS_DIM(CC2.pos.x() + posdims.back().pos.x(),  posdims.back().dims.x(), posdims.size(),  "    CC2 last  ", " ");
    file << CC2_single.attr.calo3d.nx << " ";
    vFORc(G4int, CC2_single.attr.calo3d.ny, ny_i)
      file << *ny_i << " ";
    vFORc(G4int, CC2_single.attr.calo3d.nz, nz_i)
      file << *nz_i << " ";
    vFORc(BoxUnionVolume::PosDims, posdims, posdim) {
      PRINT_GEOS_TO_FILE_POS_DIM(posdim);
    }
    file << G4endl;
  ELSE
    PRINT_BOX(CC2_single,"  CC2                ");
    file << "CC2 - ";
    PRINT_GEO_TO_FILE(CC2_single);
  END_IF

  file << "S4 " << S4.size() << "\n";
  vFOR(BoxVolume, S4, vol) {
    PRINT_BOX(*vol,"  S4                 ");
    PRINT_SCINT_STRIP_TO_FILE(*vol);
  }


  G4cout << " |_____________________________________________________________________________|\n" << G4endl;
  G4cout.flags(coutflags);
  file << "----------------------------------------" << G4endl;

  file.close();
}




void DetectorConstruction::PrintThickness()
{
  G4double density;    // [g/cm3]
  G4double radlength;  // [cm]
  G4double thickness, s_thickness = 0, S_thickness = 0, SS_thickness = 0; // [cm]
  G4double depth,     s_depth = 0,     S_depth = 0,     SS_depth = 0;     // [g/cm2]
  G4double length,    s_length = 0,    S_length = 0,    SS_length = 0;    // [Xo]
  G4Material* mater;

  ios::fmtflags coutflags = G4cout.flags();
  G4cout.setf(ios::fixed);
  G4cout << ",-------------------------------------------------------------------------------------------------------------------------,\n";
  G4cout << "|                     |                 t  h  i  c  k  n  e  s  s                 |      density      |         Xo        |\n";
  G4cout << "|      Material       |       [cm]        |      [g/cm2]      |       [Xo]        |      [g/cm3]      |        [cm]       |\n";
  G4cout << "|---------------------|-------------------|-------------------|-------------------|-------------------|-------------------|\n";

  #define EMPTY_LINE_OF_TABLE() \
    "                  |                   |                   |                   |                   |"

  #define PRINT_EMPTY_LINE_OF_TABLE() \
    G4cout << "|                     |                   |                   |                   |                   |                   |" << G4endl;

  #define CLEAR_s_SUMMATION()                \
    s_thickness = 0; s_depth = 0; s_length = 0;

  #define INCREASE_SS()                                                     \
    SS_thickness += S_thickness; SS_depth += S_depth; SS_length += S_length; \
     S_thickness = 0;               S_depth = 0;          S_length = 0;

  #define SET_MATER_AND_THICKNESS(box)                                              \
    mater = (box).mater;   density = mater->GetDensity();               \
    radlength = mater->GetRadlen();                                                   \
    thickness = 2*(box).dims.x();    s_thickness += thickness; S_thickness += thickness; \
    depth = thickness*density;     s_depth += depth;         S_depth += depth;          \
    length = thickness/radlength;  s_length += length;       S_length += length;

  #define PRINT_ONE_DIMENSION(quantity)                         \
    G4cout << setw(14) << setprecision(6) << quantity << "    | ";

  #define PRINT_DIMENSIONS()            \
    PRINT_ONE_DIMENSION(thickness/(cm)); \
    PRINT_ONE_DIMENSION(depth/(g/cm2));   \
    PRINT_ONE_DIMENSION(length);           \
    PRINT_ONE_DIMENSION(density/(g/cm3));   \
    PRINT_ONE_DIMENSION(radlength/(cm));     \
    G4cout << G4endl;

  #define PRINT_s_DIMENSIONS()            \
    PRINT_ONE_DIMENSION(s_thickness/(cm)); \
    PRINT_ONE_DIMENSION(s_depth/(g/cm2));   \
    PRINT_ONE_DIMENSION(s_length);           \
    PRINT_ONE_DIMENSION(density/(g/cm3));     \
    PRINT_ONE_DIMENSION(radlength/(cm));       \
    G4cout << G4endl;

  #define PRINT_S_DIMENSIONS()            \
    PRINT_ONE_DIMENSION(S_thickness/(cm)); \
    PRINT_ONE_DIMENSION(S_depth/(g/cm2));   \
    PRINT_ONE_DIMENSION(S_length);           \
    G4cout << "                  | ";         \
    G4cout << "                  |" << G4endl;

  #define SET_MATER_FOR_LAYER(box) \
    mater = (box).mater;             \
    density = mater->GetDensity();   \
    radlength = mater->GetRadlen();

  #define SET_THICKNESS_FOR_LAYER(box)                   \
    thickness = 2*(box).dims.x();   s_thickness += thickness; \
    depth = thickness*density;    s_depth += depth;        \
    length = thickness/radlength; s_length += length;

  #define INCREASE_S()                                                 \
    S_thickness += s_thickness; S_depth += s_depth; S_length += s_length;

  G4cout << "|  topAC              | " << EMPTY_LINE_OF_TABLE() << G4endl;

  G4cout << "|           pSty      | ";
  vFOR(BoxVolume, topAC, vol) {
    SET_MATER_AND_THICKNESS(*vol);
  }
  PRINT_DIMENSIONS();

  G4cout << "|           Al        | ";
  SET_MATER_AND_THICKNESS(topACSupportAl);
  PRINT_DIMENSIONS();

  G4cout << "|           CFRP      | ";
  CLEAR_s_SUMMATION();
  SET_MATER_AND_THICKNESS(topACSupportTopCFRP);
  SET_MATER_AND_THICKNESS(topACSupportBotCFRP);
  PRINT_s_DIMENSIONS();

  G4cout << "|        T O T A L    | ";
  PRINT_S_DIMENSIONS();
  PRINT_EMPTY_LINE_OF_TABLE();
  INCREASE_SS();


  G4cout << "|  C                  | " << EMPTY_LINE_OF_TABLE() << G4endl;

  G4cout << "|           Si        | ";
  CLEAR_s_SUMMATION();
  uFORs(ilayer, 0, CLayerSiY.nlayers) {
    SET_MATER_FOR_LAYER(CLayerSiY[ilayer]);
    SET_THICKNESS_FOR_LAYER(CLayerSiY[ilayer]);
    SET_MATER_FOR_LAYER(CLayerSiZ[ilayer]);
    SET_THICKNESS_FOR_LAYER(CLayerSiZ[ilayer]);
  }
  PRINT_s_DIMENSIONS();
  INCREASE_S();

  G4cout << "|           W         | ";
  CLEAR_s_SUMMATION();
  uFORs(ilayer, 0, CLayerW.nlayers) {
    SET_MATER_FOR_LAYER(CLayerW[ilayer]);
    SET_THICKNESS_FOR_LAYER(CLayerW[ilayer]);
  }
  PRINT_s_DIMENSIONS();
  INCREASE_S();

  G4cout << "|           Al        | ";
  CLEAR_s_SUMMATION();
  uFORs(ilayer, 0, CSupportAl.nlayers) {
    SET_MATER_FOR_LAYER(CSupportAl[ilayer]);
    SET_THICKNESS_FOR_LAYER(CSupportAl[ilayer]);
  }
  PRINT_s_DIMENSIONS();
  INCREASE_S();

  G4cout << "|           CFRP      | ";
  CLEAR_s_SUMMATION();
  uFORs(ilayer, 0, CSupportTopCFRP.nlayers) {
    SET_MATER_FOR_LAYER(CSupportTopCFRP[ilayer]);
    SET_THICKNESS_FOR_LAYER(CSupportTopCFRP[ilayer]);
    SET_MATER_FOR_LAYER(CSupportBotCFRP[ilayer]);
    SET_THICKNESS_FOR_LAYER(CSupportBotCFRP[ilayer]);
  }
  PRINT_s_DIMENSIONS();
  INCREASE_S();

  G4cout << "|        T O T A L    | ";
  PRINT_S_DIMENSIONS();
  PRINT_EMPTY_LINE_OF_TABLE();
  INCREASE_SS();


  G4cout << "|  S1                 | " << EMPTY_LINE_OF_TABLE() << G4endl;

  G4cout << "|           pSty      | ";
  vFOR(BoxVolume, S1, vol) {
    SET_MATER_AND_THICKNESS(*vol);
  }
  PRINT_DIMENSIONS();

  G4cout << "|           Al        | ";
  SET_MATER_AND_THICKNESS(S1SupportAl);
  PRINT_DIMENSIONS();

  G4cout << "|           CFRP      | ";
  CLEAR_s_SUMMATION();
  SET_MATER_AND_THICKNESS(S1SupportTopCFRP);
  SET_MATER_AND_THICKNESS(S1SupportBotCFRP);
  PRINT_s_DIMENSIONS();

  G4cout << "|        T O T A L    | ";
  PRINT_S_DIMENSIONS();
  PRINT_EMPTY_LINE_OF_TABLE();
  INCREASE_SS();


  G4cout << "|  S2                 | " << EMPTY_LINE_OF_TABLE() << G4endl;

  G4cout << "|           pSty      | ";
  vFOR(BoxVolume, S2, vol) {
    SET_MATER_AND_THICKNESS(*vol);
  }
  PRINT_DIMENSIONS();

  G4cout << "|           Al        | ";
  SET_MATER_AND_THICKNESS(S2SupportAl);
  PRINT_DIMENSIONS();

  G4cout << "|           CFRP      | ";
  CLEAR_s_SUMMATION();
  SET_MATER_AND_THICKNESS(S2SupportTopCFRP);
  SET_MATER_AND_THICKNESS(S2SupportBotCFRP);
  PRINT_s_DIMENSIONS();

  G4cout << "|        T O T A L    | ";
  PRINT_S_DIMENSIONS();
  PRINT_EMPTY_LINE_OF_TABLE();
  INCREASE_SS();


  G4cout << "|  CC1                | " << EMPTY_LINE_OF_TABLE() << G4endl;

  G4cout << "|           Si        | ";
  CLEAR_s_SUMMATION();
  uFORs(ilayer, 0, CC1LayerSiY.nlayers) {
    SET_MATER_FOR_LAYER(CC1LayerSiY[ilayer]);
    SET_THICKNESS_FOR_LAYER(CC1LayerSiY[ilayer]);
    SET_MATER_FOR_LAYER(CC1LayerSiZ[ilayer]);
    SET_THICKNESS_FOR_LAYER(CC1LayerSiZ[ilayer]);
  }
  PRINT_s_DIMENSIONS();
  INCREASE_S();

  G4cout << "|           CsI(Tl)   | ";
  CLEAR_s_SUMMATION();
  uFORs(ilayer, 0, CC1LayerCsI.nlayers) {
    SET_MATER_FOR_LAYER(CC1LayerCsI[ilayer]);
    SET_THICKNESS_FOR_LAYER(CC1LayerCsI[ilayer]);
  }
  PRINT_s_DIMENSIONS();
  INCREASE_S();

  G4cout << "|           Al        | ";
  CLEAR_s_SUMMATION();
  uFORs(ilayer, 0, CC1SupportAl_CsI.nlayers) {
    SET_MATER_FOR_LAYER(CC1SupportAl_CsI[ilayer]);
    SET_THICKNESS_FOR_LAYER(CC1SupportAl_CsI[ilayer]);
  }
  uFORs(ilayer, 0, CC1SupportAl_Si.nlayers) {
    SET_MATER_FOR_LAYER(CC1SupportAl_Si[ilayer]);
    SET_THICKNESS_FOR_LAYER(CC1SupportAl_Si[ilayer]);
  }
  PRINT_s_DIMENSIONS();
  INCREASE_S();

  G4cout << "|           CFRP      | ";
  CLEAR_s_SUMMATION();
  uFORs(ilayer, 0, CC1SupportTopCFRP_CsI.nlayers) {
    SET_MATER_FOR_LAYER(CC1SupportTopCFRP_CsI[ilayer]);
    SET_THICKNESS_FOR_LAYER(CC1SupportTopCFRP_CsI[ilayer]);
    SET_MATER_FOR_LAYER(CC1SupportBotCFRP_CsI[ilayer]);
    SET_THICKNESS_FOR_LAYER(CC1SupportBotCFRP_CsI[ilayer]);
  }
  uFORs(ilayer, 0, CC1SupportTopCFRP_Si.nlayers) {
    SET_MATER_FOR_LAYER(CC1SupportTopCFRP_Si[ilayer]);
    SET_THICKNESS_FOR_LAYER(CC1SupportTopCFRP_Si[ilayer]);
    SET_MATER_FOR_LAYER(CC1SupportBotCFRP_Si[ilayer]);
    SET_THICKNESS_FOR_LAYER(CC1SupportBotCFRP_Si[ilayer]);
  }
  PRINT_s_DIMENSIONS();
  INCREASE_S();

  G4cout << "|        T O T A L    | ";
  PRINT_S_DIMENSIONS();
  PRINT_EMPTY_LINE_OF_TABLE();
  INCREASE_SS();


  G4cout << "|  S3                 | " << EMPTY_LINE_OF_TABLE() << G4endl;

  G4cout << "|           pSty      | ";
  vFOR(BoxVolume, S3, vol) {
    SET_MATER_AND_THICKNESS(*vol);
  }
  PRINT_DIMENSIONS();

  G4cout << "|           Al        | ";
  SET_MATER_AND_THICKNESS(S3SupportAl);
  PRINT_DIMENSIONS();

  G4cout << "|           CFRP      | ";
  CLEAR_s_SUMMATION();
  SET_MATER_AND_THICKNESS(S3SupportTopCFRP);
  SET_MATER_AND_THICKNESS(S3SupportBotCFRP);
  PRINT_s_DIMENSIONS();

  G4cout << "|        T O T A L    | ";
  PRINT_S_DIMENSIONS();
  PRINT_EMPTY_LINE_OF_TABLE();
  INCREASE_SS();


  G4cout << "|  CC2                | " << EMPTY_LINE_OF_TABLE() << G4endl;

  IF(CC2_is_segmented)
    G4cout << "|      CsI(Tl) (each) | ";
    mater = CC2.mater;   density = mater->GetDensity();
    radlength = mater->GetRadlen();
    thickness = 2*CC2.posdims.front().dims.x();
    s_thickness += thickness; S_thickness += thickness;
    depth = thickness*density;     s_depth += depth;         S_depth += depth;
    length = thickness/radlength;  s_length += length;       S_length += length;
    PRINT_DIMENSIONS();
  ELSE
    G4cout << "|          BGO/CsI    | ";
    SET_MATER_AND_THICKNESS(CC2_single);
    PRINT_DIMENSIONS();
  END_IF

  UNLESS(CC2_is_segmented)
    G4cout << "|          CFRP       | ";
    CLEAR_s_SUMMATION();
    SET_MATER_AND_THICKNESS(CC2SupportTopCFRP);
    SET_MATER_AND_THICKNESS(CC2SupportBotCFRP);
     PRINT_s_DIMENSIONS();
  END_UNLESS

  G4cout << "|        T O T A L    | ";
  PRINT_S_DIMENSIONS();
  PRINT_EMPTY_LINE_OF_TABLE();
  INCREASE_SS();


  G4cout << "| - - - - - - - - - - | - - - - - - - - - | - - - - - - - - - | - - - - - - - - - | - - - - - - - - - | - - - - - - - - - |\n";
  G4cout << "|    T  O  T  A  L    | ";
  PRINT_ONE_DIMENSION(SS_thickness/(cm));
  PRINT_ONE_DIMENSION(SS_depth/(g/cm2));
  PRINT_ONE_DIMENSION(SS_length);
  G4cout << "                  | ";
  G4cout << "                  |" << G4endl;


  G4cout << "'-------------------------------------------------------------------------------------------------------------------------'" << G4endl;
  G4cout.flags(coutflags);
}

void DetectorConstruction::SetCompatibilityMode() { if(geo) geo->AllowDefault(); }
