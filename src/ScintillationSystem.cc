#include "G4UnitsTable.hh"

#include "ScintillationSystem.hh"
#include "SensitiveScintStrip.hh"
#include "EventAction.hh"
#include "EventData.hh"
#include "Alternative.hh"
#include "FOR.hh"


ScintStripIndex ScintStripLayer::FindIndex(const G4ThreeVector& hitpos, G4double time) const
{
  G4double s_pos, p_pos;
  s_pos = hitpos[IndexOfStripProj(acrossproj)];
  p_pos = hitpos[IndexOfStripProj(alongproj)];
  G4int ss = findbin(s_min, s_bin(), s_pos);
  G4int pp = findbin(p_min, p_bin(), p_pos);
  G4int tt = findbin(t_min, t_bin(), time);
  return ScintStripIndex(ss, pp, tt);
}

void ScintStripLayer::Initialize()
{
  //t_nbins = p_nbins = s_nbins = 3;
  vector<ScintStripPoint> t_default           ( t_nbins, 0         );
  vector< vector<ScintStripPoint> > p_default ( p_nbins, t_default );
  assign  /*   *   *   *   *   *   *   *   */ ( s_nbins, p_default );
}


ScintStripSignals ScintStripLayer::GetSignal()
{
  ScintStripSignals sig;
  //sig.one.layer = sig.two.layer = this;
  sig.one.t_bin = sig.two.t_bin = t_bin();
  sig.one.t_min = sig.two.t_min = t_min;
  sig.one.s_min = sig.two.s_min = s_min;
  sig.one.s_max = sig.two.s_max = s_max;
  sig.one.s_nbins = sig.two.s_nbins = s_nbins;
  G4double dt = (p_max - p_min) / speedoflight;
  G4int tbins = t_nbins + (G4int)ceil( dt / t_bin() );
  vector<ScintStripPoint> t_default           ( tbins, 0 );
  sig.one.assign( s_nbins, t_default );
  sig.two.assign( s_nbins, t_default );
  
  G4double one_time, two_time; // when light arrives to the edge of crystal
  FORs(i_s, 0, s_nbins) {
  //vFORc(vector< vector<ScintStripPoint> >, *this, strip) {
    // (*this)[i_s][i_p][i_t] --> sig.###[i_s][j_t]
    FORs(i_p, 0, p_nbins) FORs(i_t, 0, t_nbins) {
      two_time = bincenter(t_min, t_bin(), i_t);
      one_time = two_time + bincenter(0, p_bin(), i_p)/speedoflight;
      two_time += (p_max - bincenter(p_min, p_bin(), i_p))/speedoflight;
      sig.one[i_s][ findbin(t_min, t_bin(), one_time) ] += operator()(i_s, i_p, i_t);
      sig.two[i_s][ findbin(t_min, t_bin(), two_time) ] += operator()(i_s, i_p, i_t);
    }
  }
  
  return sig;
}


G4double ScintStripSignal::GetActuationTime() const
{
  G4int b = -1;
  FORs(i_s, 0, s_nbins) {
    FORs(i_t, 0, (G4int)at(i_s).size()) {
      if( at(i_s).at(i_t).edep > 0.00 * MeV ) { // threshold
        if( b == -1  ||  i_t < b ) b = i_t;
        break;
      }
    }
  }
  if (b == -1) return SensitiveScintStrip::kNoSignal;
  return bincenter(t_min  -  0.5 * t_bin, t_bin, b);
}

G4double ScintStripSignal::GetActuationTime(G4int strip) const
{
  G4int b = -1;
  FORs(i_t, 0, (G4int)at(strip).size()) {
    if( at(strip).at(i_t).edep > 0.00 * MeV ) { // threshold
      if( b == -1  ||  i_t < b ) b = i_t;
      break;
    }
  }
  if (b == -1) return SensitiveScintStrip::kNoSignal;
  return bincenter(t_min  -  0.5 * t_bin, t_bin, b);
}


void ScintillationSystem::Clear()
{
  /*size_t s; Disp("__________________");
  Disp(s = layers.size(),"size: ");
  Disp(s? s=layers.front()->size():s,".size: ");
  Disp(s? layers.front()->front().size():s,"..size: ");
  Disp(s? layers.front()->front().front().size():s,"...size: ");*/
  vFOR(ScintStripLayer*, layers, l) {
    ScintStripLayerFOR(**l, pt)
      pt->edep = 0.0;
    (*l)->sensitive->earliest = SensitiveScintStrip::kNoSignal;
    (*l)->sensitive->latest   = SensitiveScintStrip::kNoSignal;
    (*l)->sensitive->Etot = 0.;
    std::vector<G4double>*& stripEtot =
      (*l)->sensitive->stripEtot;
    stripEtot->assign(stripEtot->size(), 0.);
    (*l)->sensitive->hassignal = false;
  }
}



G4bool ScintillationSystem::Write() const
{
  
  if(!data->ok()) return false;
  
  return true;
}


G4bool ToFSystem::Write()
{
  ensure( data->ok() );
  ensure( ScintillationSystem::Write() );
  data->timeData.S1.data = &S1;
  data->timeData.S2.data = &S2;
  data->timeData.S3.data = &S3;
  data->timeData.S4.data = &S4;
  return true;
}

G4bool ACSystem::Write()
{
  ensure( data->ok() );
  ensure( ScintillationSystem::Write() );
  data->timeData.topAC.data        = &topAC;
  data->timeData.sideAC_left.data  = &sideAC_left;
  data->timeData.sideAC_front.data = &sideAC_front;
  data->timeData.sideAC_right.data = &sideAC_right;
  data->timeData.sideAC_back.data  = &sideAC_back;
  return true;
}



G4bool starts_with(const string& str, const string& prefix) {
  size_t len = prefix.size();
  if (str.size() < len) return false;
  return str.substr(0, len) == prefix;
}


G4bool ToFSystem::Set()
{
  vFOR(ScintStripLayer*, layers, l) {
    string name = (*l)->sensitive->GetName();
    WHICH
      INCASE starts_with(name, "S1") THEN
        S1.push_back(*l); //, (data ? data->timeData.S1.data = *l : NULL);
      INCASE starts_with(name, "S2") THEN
        S2.push_back(*l); //, (data ? data->timeData.S2.data = S2 : NULL);
      INCASE starts_with(name, "S3") THEN
        S3.push_back(*l); //, (data ? data->timeData.S3.data = S3 : NULL);
      INCASE starts_with(name, "S4") THEN
        S4.push_back(*l); //, (data ? data->timeData.S4.data = S4 : NULL);
      ELSE
        G4Exception(
          "ToFSystem::Set",
          ("Unknown ToF detector name: " + name).c_str(),
          FatalException,
         "");
    END_WHICH
  };
  if (data) {
    data->timeData.S1.data = &S1;
    data->timeData.S2.data = &S2;
    data->timeData.S3.data = &S3;
    data->timeData.S4.data = &S4;
  }
  if(S1.size() && S2.size() && S3.size() && S4.size()) return true;
  return false;
}


G4bool ACSystem::Set()
{
  vFOR(ScintStripLayer*, layers, l) {
    string name = (*l)->sensitive->GetName();
    WHICH
      INCASE starts_with(name, "topAC") THEN
        topAC.push_back(*l);
      INCASE starts_with(name, "sideAC-left") THEN
        sideAC_left.push_back(*l);
      INCASE starts_with(name, "sideAC-front") THEN
        sideAC_front.push_back(*l);
      INCASE starts_with(name, "sideAC-right") THEN
        sideAC_right.push_back(*l);
      INCASE starts_with(name, "sideAC-back") THEN
        sideAC_back.push_back(*l);
      ELSE
        G4Exception(
          "ACSystem::Set",
          ("Unknown AC detector name: " + name).c_str(),
          FatalException,
         "");
    END_WHICH
  }
  if (data) {
    data->timeData.topAC.data        = &topAC;
    data->timeData.sideAC_left.data  = &sideAC_left;
    data->timeData.sideAC_front.data = &sideAC_front;
    data->timeData.sideAC_right.data = &sideAC_right;
    data->timeData.sideAC_back.data  = &sideAC_back;
  }
  if(topAC.size() && sideAC_left.size() && sideAC_front.size() && sideAC_right.size() && sideAC_back.size()) return true;
  return false;
}




G4bool ACSystem::Trigger() const
{
  return true;
}


G4bool ToFSystem::Trigger(G4bool timeSelection) const
{
 IF( 0 ) // histogram-based
  G4double S1_earliest = 1e99*s,S2_earliest = 1e99*s;
  G4int    S1_earliestbin,      S2_earliestbin;
  G4bool   S1_actuated = false, S2_actuated = false;
  G4double e;

  vFORc(ScintStripLayer*, S1, layer) {
    FORs(ss, 0, (*layer)->s_nbins)
    FORs(pp, 0, (*layer)->p_nbins)
    FORs(tt, 0, (*layer)->t_nbins) {
      e = (**layer)(ss,pp,tt).edep;
      IF_ e > 0 THEN
        IF_ S1_actuated THEN
          { if(tt < S1_earliestbin) S1_earliestbin = tt; }
        ELSE
          S1_actuated = true, S1_earliestbin = tt;
        FI
      FI
    }
    G4double earliest_in_layer = binmax((*layer)->t_min, (*layer)->t_bin(), S1_earliestbin);
    if (earliest_in_layer < S1_earliest)
      S1_earliest = earliest_in_layer;
  }
  ensure(S1_actuated);
  
  vFORc(ScintStripLayer*, S2, layer) {
    FORs(ss, 0, (*layer)->s_nbins)
    FORs(pp, 0, (*layer)->p_nbins)
    FORs(tt, 0, (*layer)->t_nbins) {
      e = (**layer)(ss,pp,tt).edep;
      IF_ e > 0 THEN
        IF_ S2_actuated THEN
          { if(tt < S2_earliestbin) S2_earliestbin = tt; }
        ELSE
          S2_actuated = true, S2_earliestbin = tt;
        FI
      FI
    }
    G4double earliest_in_layer = binmax((*layer)->t_min, (*layer)->t_bin(), S2_earliestbin);
    if (earliest_in_layer < S2_earliest)
      S2_earliest = earliest_in_layer;
  }
  ensure(S2_actuated);
  
  return timeSelection ? S1_earliest <= S2_earliest + 0.0001*ns : true;
 FI
 IF( 1 ) // absolute time
  G4bool hassignal = false;
  G4double S1_earliest = 1e99*s, S2_earliest = 1e99*s, layer_earliest;
  vFORc(ScintStripLayer*, S1, layer) {
    if ( (*layer)->sensitive->HasSignal() ) {
      hassignal = true;
      layer_earliest = (*layer)->sensitive->GetEarliest();
      if (layer_earliest < S1_earliest)
        S1_earliest = layer_earliest;
    }
  }
  ensure( hassignal );
  
  hassignal = false;
  vFORc(ScintStripLayer*, S2, layer) {
    if ( (*layer)->sensitive->HasSignal() ) {
      hassignal = true;
      layer_earliest = (*layer)->sensitive->GetEarliest();
      if (layer_earliest < S2_earliest)
        S2_earliest = layer_earliest;
    }
  }
  ensure( hassignal );
  return timeSelection ? S1_earliest < S2_earliest : true ;
 FI
 IF( 0 ) // signal-based
  return true;
 FI
 return true;
}
