#include "Trigger.hh"

#include "DetectorConstruction.hh"
#include "PositionSystem.hh"
#include "ScintillationSystem.hh"
#include "FOR.hh"
#include "types.hh"

G4bool Trigger::IsGoodEvent() const
{
//  if( !Disp(posSystem -> Trigger(),"posSystem trigger: ","",true) ) return false;
//  if( !Disp(ToF       -> Trigger(),"time-of-flight trigger: ","",true) ) return false;
  if((use_trigger & kTrigger_pos) && posSystem)
    ensure( posSystem -> Trigger() );
  if((use_trigger & kTrigger_tof) && ToF)
    ensure( ToF       -> Trigger(ToFTimeSelection) );
  if((use_trigger & kTrigger_ac) && AC)
    ensure( AC        -> Trigger() );
  return true;
}
