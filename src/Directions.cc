#include "Directions.hh"

using namespace std;
using namespace CLHEP;

Direction3D::Direction3D(G4ThreeVector vec)
{
  vec.setMag(1);
  vy = vec.y(); vz = vec.z();
}

Direction3D::Direction3D(const Angle3D& ang)
{
  G4ThreeVector vec = ang;
  vec /= vec.x();
  vy = vec.y(); vz = vec.z();
}


Angle3D::Angle3D(G4ThreeVector vec)
{
  G4double x = vec.x(), y = vec.y(), z = vec.z();
  vec.set(y, z, x);
  theta = vec.theta();
  phi = vec.phi();
}

Angle3D::Angle3D(const Direction3D& dir)
{
  G4ThreeVector vec = dir;
  G4double x = vec.x(), y = vec.y(), z = vec.z();
  vec.set(y, z, x);
  theta = vec.theta(); phi = vec.phi();
}

Angle3D::operator G4ThreeVector() const
{
  return G4ThreeVector(cos(theta), sin(theta)*cos(phi), sin(theta)*sin(phi));
}
